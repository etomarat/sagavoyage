# coding: utf-8

from django.core.urlresolvers import reverse
from django.core.validators import MaxLengthValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _

from ckeditor.fields import RichTextField
# from ckeditor.widgets import CKEditorWidget

from kernel.db import PublishModel, TitleMixin, SortableMixin


class Article(PublishModel, TitleMixin, SortableMixin):
    teaser = models.TextField(
        verbose_name=_(u'Анонс'),
        help_text=_(u'Рекомендуется не вводить более 300 символов'),
        blank=True,
        validators=[MaxLengthValidator(350)],
        )

    text = RichTextField(verbose_name=_(u'Текст'), blank=True)
    img = models.ImageField(
        verbose_name=_(u'Изображение'),
        help_text=u'400x560px',
        upload_to='blog',
        )

    class Meta:
        verbose_name = _(u'Статья')
        verbose_name_plural = _(u'Блог')
        ordering = ['weight']

    class TMeta:
        exclude = ['img']

    @property
    @models.permalink
    def absolute_url(self):
        return ('blog:detail', [self.pk])

# EOF