# coding: utf-8

from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, DetailView

from kernel.views import PublishedQuerysetMixin

from .models import *


class ArticleList(PublishedQuerysetMixin, ListView):
    model = Article


class ArticleDetail(PublishedQuerysetMixin, DetailView):
    model = Article
    template_name = 'tour/tour_detail.html'

# EOF