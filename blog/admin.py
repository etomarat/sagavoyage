# coding: utf-8

from django.contrib import admin

from kernel.admin import BaseTranslationAdmin, PublishedSortableTranslationAdmin

from .models import *


admin.site.register(Article, PublishedSortableTranslationAdmin)

# EOF