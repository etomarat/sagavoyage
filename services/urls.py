# coding: utf-8

from django.conf.urls import patterns, url

from .views import services, ServicesDetail


urlpatterns = patterns(
    '',
    url(r'^$', services, name='services'),
    url(r'^(?P<pk>\d+)$', ServicesDetail.as_view(), name='detail'),
    )

# EOF