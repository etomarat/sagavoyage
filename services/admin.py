from django.contrib import admin

from modeltranslation.admin import TranslationTabularInline
from kernel.admin import  SingletonTranslationAdmin, BaseTranslationAdmin
from .models import ServicesPage, Plaha, Article

class PlahaInline(TranslationTabularInline):
    model = Plaha
    extra = 0

class ServicesPageAdmin(SingletonTranslationAdmin):
    inlines = [PlahaInline, ]


admin.site.register(Article, BaseTranslationAdmin)

admin.site.register(ServicesPage, ServicesPageAdmin)