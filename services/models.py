# coding: utf-8

from django.db import models
from solo.models import SingletonModel
from django.utils.translation import ugettext_lazy as _
from ckeditor.fields import RichTextField
from kernel.db import PublishModel, TitleMixin, SortableMixin
from django.core.validators import MaxLengthValidator


class ServicesPage(SingletonModel, TitleMixin):
    
    def __unicode__(self):
        return u'Страница "Наши услуги"'

    class Meta:
        verbose_name=_(u'Страница "Наши услуги"')
    
    class TMeta:
        fields = [
          'title',
          ]

class Article(PublishModel, TitleMixin):
    text = RichTextField(verbose_name=_(u'Текст'), blank=True)

    class Meta:
        verbose_name = _(u'Статья')
        verbose_name_plural = _(u'Статьи')

    @property
    @models.permalink
    def absolute_url(self):
        return ('services:detail', [self.pk])
    
    class TMeta:
        fields = [
          'text',
          'title',
          ]


class Plaha(PublishModel, TitleMixin, SortableMixin):
    img = models.ImageField(
          verbose_name=_(u'Изображение'),
          help_text=u'2560x356px',
          upload_to='partners',
          blank=True,
          )
    #link = models.CharField(verbose_name=_(u'Ссылка'), help_text=_(u'Кнопка "подробнее"'), max_length=0xff, blank=True)
    parent = models.ForeignKey(ServicesPage, verbose_name=_(u'О нас'), blank=True)
    article = models.ForeignKey(Article, verbose_name=_(u'Статья'),)
    
    class TMeta:
        fields = [
          'is_published',
          'title',
          'img',
          ]
    
    class Meta:
        verbose_name = _(u'Плашка')
        verbose_name_plural = _(u'Плашки')
        ordering = ['weight']
          
        