from django.shortcuts import render
from django.views.generic import ListView, DetailView

from kernel.views import PublishedQuerysetMixin

from .models import ServicesPage, Article

def services(request):
    template_name='services.html'

    return render(request, template_name, {'object': ServicesPage.objects.get()})

class ServicesDetail(PublishedQuerysetMixin, DetailView):
    model = Article
