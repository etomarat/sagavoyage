# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Article.is_published_en'
        db.delete_column(u'services_article', 'is_published_en')

        # Deleting field 'Article.is_published_de'
        db.delete_column(u'services_article', 'is_published_de')

        # Deleting field 'Article.is_published_es'
        db.delete_column(u'services_article', 'is_published_es')

        # Deleting field 'Article.is_published_ru'
        db.delete_column(u'services_article', 'is_published_ru')

        # Deleting field 'Article.is_published_fr'
        db.delete_column(u'services_article', 'is_published_fr')

        # Deleting field 'Article.is_published_cn'
        db.delete_column(u'services_article', 'is_published_cn')

        # Adding field 'Article.title_ru'
        db.add_column(u'services_article', 'title_ru',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Article.title_en'
        db.add_column(u'services_article', 'title_en',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Article.title_fr'
        db.add_column(u'services_article', 'title_fr',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Article.title_de'
        db.add_column(u'services_article', 'title_de',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Article.title_es'
        db.add_column(u'services_article', 'title_es',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Article.title_cn'
        db.add_column(u'services_article', 'title_cn',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Article.text_ru'
        db.add_column(u'services_article', 'text_ru',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Article.text_en'
        db.add_column(u'services_article', 'text_en',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Article.text_fr'
        db.add_column(u'services_article', 'text_fr',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Article.text_de'
        db.add_column(u'services_article', 'text_de',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Article.text_es'
        db.add_column(u'services_article', 'text_es',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Article.text_cn'
        db.add_column(u'services_article', 'text_cn',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Article.is_published_en'
        db.add_column(u'services_article', 'is_published_en',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Article.is_published_de'
        db.add_column(u'services_article', 'is_published_de',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Article.is_published_es'
        db.add_column(u'services_article', 'is_published_es',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Article.is_published_ru'
        db.add_column(u'services_article', 'is_published_ru',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Article.is_published_fr'
        db.add_column(u'services_article', 'is_published_fr',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Article.is_published_cn'
        db.add_column(u'services_article', 'is_published_cn',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Deleting field 'Article.title_ru'
        db.delete_column(u'services_article', 'title_ru')

        # Deleting field 'Article.title_en'
        db.delete_column(u'services_article', 'title_en')

        # Deleting field 'Article.title_fr'
        db.delete_column(u'services_article', 'title_fr')

        # Deleting field 'Article.title_de'
        db.delete_column(u'services_article', 'title_de')

        # Deleting field 'Article.title_es'
        db.delete_column(u'services_article', 'title_es')

        # Deleting field 'Article.title_cn'
        db.delete_column(u'services_article', 'title_cn')

        # Deleting field 'Article.text_ru'
        db.delete_column(u'services_article', 'text_ru')

        # Deleting field 'Article.text_en'
        db.delete_column(u'services_article', 'text_en')

        # Deleting field 'Article.text_fr'
        db.delete_column(u'services_article', 'text_fr')

        # Deleting field 'Article.text_de'
        db.delete_column(u'services_article', 'text_de')

        # Deleting field 'Article.text_es'
        db.delete_column(u'services_article', 'text_es')

        # Deleting field 'Article.text_cn'
        db.delete_column(u'services_article', 'text_cn')


    models = {
        u'services.article': {
            'Meta': {'object_name': 'Article'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'text': ('ckeditor.fields.RichTextField', [], {'blank': 'True'}),
            'text_cn': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text_de': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text_es': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text_ru': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_cn': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_de': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_es': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_fr': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'services.plaha': {
            'Meta': {'ordering': "['weight']", 'object_name': 'Plaha'},
            'article': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['services.Article']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'img_cn': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_de': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_en': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_es': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_fr': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_ru': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_cn': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_de': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_en': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_es': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_fr': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_ru': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['services.ServicesPage']", 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_cn': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_de': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_es': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_fr': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.SmallIntegerField', [], {'default': '10000'})
        },
        u'services.servicespage': {
            'Meta': {'object_name': 'ServicesPage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_cn': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_de': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_es': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_fr': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['services']