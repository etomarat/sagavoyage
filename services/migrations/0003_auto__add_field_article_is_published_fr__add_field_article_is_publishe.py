# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Article.is_published_fr'
        db.add_column(u'services_article', 'is_published_fr',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Article.is_published_de'
        db.add_column(u'services_article', 'is_published_de',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Article.is_published_es'
        db.add_column(u'services_article', 'is_published_es',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Article.is_published_cn'
        db.add_column(u'services_article', 'is_published_cn',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Plaha.is_published_fr'
        db.add_column(u'services_plaha', 'is_published_fr',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Plaha.is_published_de'
        db.add_column(u'services_plaha', 'is_published_de',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Plaha.is_published_es'
        db.add_column(u'services_plaha', 'is_published_es',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Plaha.is_published_cn'
        db.add_column(u'services_plaha', 'is_published_cn',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Plaha.title_fr'
        db.add_column(u'services_plaha', 'title_fr',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Plaha.title_de'
        db.add_column(u'services_plaha', 'title_de',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Plaha.title_es'
        db.add_column(u'services_plaha', 'title_es',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Plaha.title_cn'
        db.add_column(u'services_plaha', 'title_cn',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Plaha.img_fr'
        db.add_column(u'services_plaha', 'img_fr',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Plaha.img_de'
        db.add_column(u'services_plaha', 'img_de',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Plaha.img_es'
        db.add_column(u'services_plaha', 'img_es',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Plaha.img_cn'
        db.add_column(u'services_plaha', 'img_cn',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ServicesPage.title_fr'
        db.add_column(u'services_servicespage', 'title_fr',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ServicesPage.title_de'
        db.add_column(u'services_servicespage', 'title_de',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ServicesPage.title_es'
        db.add_column(u'services_servicespage', 'title_es',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ServicesPage.title_cn'
        db.add_column(u'services_servicespage', 'title_cn',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Article.is_published_fr'
        db.delete_column(u'services_article', 'is_published_fr')

        # Deleting field 'Article.is_published_de'
        db.delete_column(u'services_article', 'is_published_de')

        # Deleting field 'Article.is_published_es'
        db.delete_column(u'services_article', 'is_published_es')

        # Deleting field 'Article.is_published_cn'
        db.delete_column(u'services_article', 'is_published_cn')

        # Deleting field 'Plaha.is_published_fr'
        db.delete_column(u'services_plaha', 'is_published_fr')

        # Deleting field 'Plaha.is_published_de'
        db.delete_column(u'services_plaha', 'is_published_de')

        # Deleting field 'Plaha.is_published_es'
        db.delete_column(u'services_plaha', 'is_published_es')

        # Deleting field 'Plaha.is_published_cn'
        db.delete_column(u'services_plaha', 'is_published_cn')

        # Deleting field 'Plaha.title_fr'
        db.delete_column(u'services_plaha', 'title_fr')

        # Deleting field 'Plaha.title_de'
        db.delete_column(u'services_plaha', 'title_de')

        # Deleting field 'Plaha.title_es'
        db.delete_column(u'services_plaha', 'title_es')

        # Deleting field 'Plaha.title_cn'
        db.delete_column(u'services_plaha', 'title_cn')

        # Deleting field 'Plaha.img_fr'
        db.delete_column(u'services_plaha', 'img_fr')

        # Deleting field 'Plaha.img_de'
        db.delete_column(u'services_plaha', 'img_de')

        # Deleting field 'Plaha.img_es'
        db.delete_column(u'services_plaha', 'img_es')

        # Deleting field 'Plaha.img_cn'
        db.delete_column(u'services_plaha', 'img_cn')

        # Deleting field 'ServicesPage.title_fr'
        db.delete_column(u'services_servicespage', 'title_fr')

        # Deleting field 'ServicesPage.title_de'
        db.delete_column(u'services_servicespage', 'title_de')

        # Deleting field 'ServicesPage.title_es'
        db.delete_column(u'services_servicespage', 'title_es')

        # Deleting field 'ServicesPage.title_cn'
        db.delete_column(u'services_servicespage', 'title_cn')


    models = {
        u'services.article': {
            'Meta': {'object_name': 'Article'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_cn': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_de': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_en': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_es': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_fr': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_ru': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'text': ('ckeditor.fields.RichTextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'services.plaha': {
            'Meta': {'ordering': "['weight']", 'object_name': 'Plaha'},
            'article': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['services.Article']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'img_cn': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_de': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_en': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_es': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_fr': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_ru': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_cn': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_de': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_en': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_es': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_fr': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_ru': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['services.ServicesPage']", 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_cn': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_de': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_es': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_fr': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.SmallIntegerField', [], {'default': '10000'})
        },
        u'services.servicespage': {
            'Meta': {'object_name': 'ServicesPage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_cn': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_de': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_es': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_fr': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['services']