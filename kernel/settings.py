# coding: utf-8

"""
Django settings for kernel project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from django.conf import global_settings

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ipc*be8c=2n_(c+!%i#!bpy+*6-=64m!)$hux!(5x^nj9n63gg'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['localhost', 'sagavoyages.com', 'www.sagavoyages.com', '127.0.0.1:8000', '127.0.0.1']

if DEBUG:
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'modeltranslation',
    'ckeditor',
    'south',
    'imagefit',
    'solo',
    'googlesearch',


    'main',
    'tour',
    'about',
    'partners',
    'services',
    'blog',
    )

MIDDLEWARE_CLASSES = (
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    )

ROOT_URLCONF = 'kernel.urls'

APPEND_SLASH = True

WSGI_APPLICATION = 'kernel.wsgi.application'

FORCE_SCRIPT_NAME = ''

# TEMPLATE_DIRS = (
#     os.path.join(BASE_DIR,  'templates'),
# )


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
    }


# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/
# http://django-modeltranslation.readthedocs.org/en/latest/

gettext = lambda s: s

LANGUAGE_CODE = 'ru'

LANGUAGES = (
    ('ru', gettext(u'Русский')),
    ('en', gettext(u'English')),
    ('fr', gettext(u'Français')),
    ('de', gettext(u'Deutsch')),
    ('es', gettext(u'Español')),
    ('cn', gettext(u'中国')),
    )

MODELTRANSLATION_DEFAULT_LANGUAGE = LANGUAGE_CODE
MODELTRANSLATION_TRANSLATION_FILES = (
    'kernel.translation',
    )

MODELTRANSLATION_FALLBACK_LANGUAGES = {
    'default': (
        'ru',
        'en',
        'fr',
        'de',
        'es',
        'cn',
        )
    }

MODELTRANSLATION_NO_TRANSLATE_FIELDS = ['weight']

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
    )

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_ROOT = os.path.join(BASE_DIR, 'www', 'static')
STATIC_URL = '/static/'

# STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.CachedStaticFilesStorage'

# STATICFILES_DIRS = (
#     os.path.join(BASE_DIR, 'static'),
#     )

# STATICFILES_FINDERS = (
#     'django.contrib.staticfiles.finders.FileSystemFinder',
#     'django.contrib.staticfiles.finders.AppDirectoriesFinder',
# )


# Media

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

IMAGEFIT_ROOT = "media"

# CKEditor

CKEDITOR_UPLOAD_PATH = "uploads/"

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'Custom',
        'toolbar_Custom': [
            ['Bold', 'Italic', 'Underline'],
            ['NumberedList', 'BulletedList', 'Blockquote', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['Link', 'Unlink'],
            ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'],
            ['Styles', 'Format', 'Font', 'FontSize'],
            ['TextColor', 'BGColor'],
            ['RemoveFormat', 'Maximize', 'Source'],
        ]
    }
}

TEMPLATE_CONTEXT_PROCESSORS = global_settings.TEMPLATE_CONTEXT_PROCESSORS + (
    "django.core.context_processors.request",
    "main.context_processors.subscribe_form"
)

# EOF
