# coding: utf-8

from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.shortcuts import redirect


admin.autodiscover()


urlpatterns = patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),

    url(r'^$', lambda *args, **kwargs: redirect('index'), name='root'),
    url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^imagefit/', include('imagefit.urls')),
    url(r'^search/', include('googlesearch.urls')),
    )


urlpatterns += i18n_patterns(
    '',
    url(r'^', include('main.urls')),
    url(r'^tours/', include('tour.urls', namespace='tour')),
    url(r'^blog/', include('blog.urls', namespace='blog')),
    url(r'^about/', include('about.urls', namespace='about')),
    url(r'^for_partners/', include('partners.urls', namespace='partners')),
    url(r'^services/', include('services.urls', namespace='services')),
    )


if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# EOF