# coding: utf-8

class PublishedQuerysetMixin(object):
    def get_queryset(self):
        return super(PublishedQuerysetMixin, self).get_queryset().published()

# EOF