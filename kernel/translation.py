# coding: utf-8

from importlib import import_module

from django.conf import settings
from django.db.models.base import ModelBase

from modeltranslation.translator import translator, TranslationOptions


no_translate_fields = set(getattr(settings, 'MODELTRANSLATION_NO_TRANSLATE_FIELDS', [])) | set(['id'])


_msg = []

for app in settings.INSTALLED_APPS:
    try:
        models = import_module(app + '.models')
    except ImportError:
        continue
    for name in dir(models):
        if name.startswith('_'):
            continue
        model = getattr(models, name)
        if not isinstance(model, ModelBase) or model._meta.abstract or not hasattr(model, 'TMeta'):
            continue
        TMeta = model.TMeta
        fields = getattr(TMeta, 'fields', None)
        exclude = set(getattr(TMeta, 'exclude', []))
        if fields is None:
            fields_from_model = set(i[0].name for i in model._meta.get_fields_with_model())
            fields = list(fields_from_model - no_translate_fields - exclude)
        options = type(
            name + 'Translation',
            (TranslationOptions, ),
            {'fields': fields}
            )
        translator.register(model, options)
        if settings.DEBUG:
            _msg += [(model.__name__, ', '.join(sorted(fields)))]


if settings.DEBUG:
    if not _msg:
        print ' -- models for translation not found --\n'
    else:
        _title = ' -- models for translation --'
        _header = ['model', 'fields']
        _maxlen0 = max([len(i) for i, j in _msg] + [len(_header[0])])
        _maxlen1 = max([len(j) for i, j in _msg] + [len(_header[1])])
        _table = [i.ljust(_maxlen0) + ' | ' + j.ljust(_maxlen1) for i, j in _msg]
        _maxlen = max([len(i) for i in _table] + [len(_title)])
        print _title.center(_maxlen + 4)
        print '-' * (_maxlen + 4)
        print '|', _header[0].center(_maxlen0), '|', _header[1].center(_maxlen1), '|'
        print '-' * (_maxlen + 4)
        print '\n'.join('| ' + i.ljust(_maxlen) + ' |' for i in _table)
        print '-' * (_maxlen + 4)
        print
        del _title, _header, _maxlen0, _maxlen1, _table, _maxlen

del _msg

# EOF