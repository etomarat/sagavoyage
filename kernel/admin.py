# coding: utf-8

from modeltranslation.admin import TranslationAdmin

from solo.admin import SingletonModelAdmin

class BaseTranslationAdmin(TranslationAdmin):
    class Media:
        js = (
            'http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js',
            'modeltranslation/js/tabbed_translation_fields.js',
            )
        css = {
            'screen': ('modeltranslation/css/tabbed_translation_fields.css',),
            }
        
class SingletonTranslationAdmin(TranslationAdmin, SingletonModelAdmin):
    class Media:
        js = (
            'http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js',
            'modeltranslation/js/tabbed_translation_fields.js',
            )
        css = {
            'screen': ('modeltranslation/css/tabbed_translation_fields.css',),
            }


class PublishedTranslationAdmin(BaseTranslationAdmin):
    list_display = ['__unicode__', 'is_published']
    list_editable = ['is_published']


class SortableTranslationAdmin(BaseTranslationAdmin):
    list_display = ['__unicode__', 'weight']
    list_editable = ['weight']


class PublishedSortableTranslationAdmin(BaseTranslationAdmin):
    list_display = ['__unicode__', 'is_published', 'weight']
    list_editable = ['is_published', 'weight']

# EOF