# coding: utf-8

from django.db import models
from django.db.models.query import QuerySet
from django.utils.translation import ugettext_lazy as _


class PublishQuerySet(QuerySet):
    def published(self, **kwargs):
        return self.filter(is_published=True, **kwargs)

class PublishManager(models.Manager):
    use_for_related_fields = True

    def get_queryset(self):
        return PublishQuerySet(self.model)

    def published(self, *args, **kwargs):
        return self.get_queryset().published(*args, **kwargs)

class PublishModel(models.Model):
    is_published = models.BooleanField(
        verbose_name=_(u'Публикация'),
        default=True,
        db_index=True,
        )

    objects = PublishManager()

    class Meta:
        abstract = True
    
    class TMeta:
        fields = ['is_published']


class TitleMixin(models.Model):
    title = models.CharField(
        verbose_name=_(u'Заголовок'),
        max_length=0xff,
        )

    class Meta:
        abstract = True
        ordering = ['title']

    class TMeta:
        fields = ['title']

    def __unicode__(self):
        return self.title or '#%s' % self.pk


class SortableMixin(models.Model):
    weight = models.SmallIntegerField(
        verbose_name=_(u'Порядок'),
        default=10000,
        )

    class Meta:
        abstract = True
        ordering = ['weight']

# EOF