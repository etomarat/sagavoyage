# Деплойой на прод: #
Приконнектиться по ssh:

```
#!
ssh u0070500@sagavoyages.com
```

Перейти в папку проекта

```
#!
cd www/sagavoyages.com
```

Спуллить нужную ветку из репозитория

```
#!
git pull origin master
```

Если изменились css, js или картинки, нужно (находясь в папке проекта):

- переключится в виртуальное окружение `source ./bin/activate`

- собрать статику `python ./manage.py collectstatic`

Вуаля!

# Локальное развертывание #

Понадобиться:

* [Python 2.7.x](https://www.python.org/downloads/)

* [pip](https://pip.pypa.io/en/stable/installing/)

Приступаем:

1. устанавливает пакет для создания виртуальных окружений `pip install virtualenv`

2. переходим в папку с проектом, создаём виртуальное окружение `python -m virtualenv .`

3. активируем виртуальное окружение `source ./bin/activate`

4. устанавливаем зависимости `pip install -r reqs.txt`

5. Берем БД с прода -- файл `scp  u0070500@sagavoyages.com:/var/www/u0070500/data/www/sagavoyages.com/db.sqlite3 ./` и кладём в корень (я не помню как сделать чтобы новая создалась, если надо -- сообщи)

6. Ставим пакет `pip install Pillow`

7. пробуем запуститься `python ./manage.py runserver`

Вуаля

## Запуск ##

1. переходим в папку с проектом

2. активируем виртаульное окружение `./bin/activate`

3. запуск `./manage.py runserver`

# Редактирование полей в бд и админке (создание и проведение миграций) #

Поля в базе (и в админке) являются отражением моделей приложений. см. models.py внутри каждого приложения (модуля).
Для того чтобы изменить базу нужно:

* Отредактировать модель (см. models.py) (например добавить поле или убрать)

* Создать миграцию `./manage.py schemamigration {{app_name}} --auto`, (app_name, это например main или partners или about. См. папки в проекте)

* Чтобы провести миграцию (изменить базу), нужно выполнить `./manage.py migrate` (и на проде тоже)