# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'TourDuration.is_published_fr'
        db.add_column(u'tour_tourduration', 'is_published_fr',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'TourDuration.is_published_de'
        db.add_column(u'tour_tourduration', 'is_published_de',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'TourDuration.is_published_es'
        db.add_column(u'tour_tourduration', 'is_published_es',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'TourDuration.is_published_cn'
        db.add_column(u'tour_tourduration', 'is_published_cn',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'TourDuration.title_fr'
        db.add_column(u'tour_tourduration', 'title_fr',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'TourDuration.title_de'
        db.add_column(u'tour_tourduration', 'title_de',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'TourDuration.title_es'
        db.add_column(u'tour_tourduration', 'title_es',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'TourDuration.title_cn'
        db.add_column(u'tour_tourduration', 'title_cn',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'TourRegion.title_fr'
        db.add_column(u'tour_tourregion', 'title_fr',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'TourRegion.title_de'
        db.add_column(u'tour_tourregion', 'title_de',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'TourRegion.title_es'
        db.add_column(u'tour_tourregion', 'title_es',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'TourRegion.title_cn'
        db.add_column(u'tour_tourregion', 'title_cn',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'TourRegion.map_title_fr'
        db.add_column(u'tour_tourregion', 'map_title_fr',
                      self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True),
                      keep_default=False)

        # Adding field 'TourRegion.map_title_de'
        db.add_column(u'tour_tourregion', 'map_title_de',
                      self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True),
                      keep_default=False)

        # Adding field 'TourRegion.map_title_es'
        db.add_column(u'tour_tourregion', 'map_title_es',
                      self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True),
                      keep_default=False)

        # Adding field 'TourRegion.map_title_cn'
        db.add_column(u'tour_tourregion', 'map_title_cn',
                      self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.is_published_fr'
        db.add_column(u'tour_tour', 'is_published_fr',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Tour.is_published_de'
        db.add_column(u'tour_tour', 'is_published_de',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Tour.is_published_es'
        db.add_column(u'tour_tour', 'is_published_es',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Tour.is_published_cn'
        db.add_column(u'tour_tour', 'is_published_cn',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Tour.title_fr'
        db.add_column(u'tour_tour', 'title_fr',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.title_de'
        db.add_column(u'tour_tour', 'title_de',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.title_es'
        db.add_column(u'tour_tour', 'title_es',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.title_cn'
        db.add_column(u'tour_tour', 'title_cn',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.accommodation_type_fr'
        db.add_column(u'tour_tour', 'accommodation_type_fr',
                      self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.accommodation_type_de'
        db.add_column(u'tour_tour', 'accommodation_type_de',
                      self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.accommodation_type_es'
        db.add_column(u'tour_tour', 'accommodation_type_es',
                      self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.accommodation_type_cn'
        db.add_column(u'tour_tour', 'accommodation_type_cn',
                      self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.teaser_fr'
        db.add_column(u'tour_tour', 'teaser_fr',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.teaser_de'
        db.add_column(u'tour_tour', 'teaser_de',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.teaser_es'
        db.add_column(u'tour_tour', 'teaser_es',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.teaser_cn'
        db.add_column(u'tour_tour', 'teaser_cn',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.text_fr'
        db.add_column(u'tour_tour', 'text_fr',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.text_de'
        db.add_column(u'tour_tour', 'text_de',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.text_es'
        db.add_column(u'tour_tour', 'text_es',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.text_cn'
        db.add_column(u'tour_tour', 'text_cn',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.teaser_popular_fr'
        db.add_column(u'tour_tour', 'teaser_popular_fr',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.teaser_popular_de'
        db.add_column(u'tour_tour', 'teaser_popular_de',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.teaser_popular_es'
        db.add_column(u'tour_tour', 'teaser_popular_es',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.teaser_popular_cn'
        db.add_column(u'tour_tour', 'teaser_popular_cn',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.img_popular_fr'
        db.add_column(u'tour_tour', 'img_popular_fr',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.img_popular_de'
        db.add_column(u'tour_tour', 'img_popular_de',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.img_popular_es'
        db.add_column(u'tour_tour', 'img_popular_es',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.img_popular_cn'
        db.add_column(u'tour_tour', 'img_popular_cn',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'TourSeason.is_published_fr'
        db.add_column(u'tour_tourseason', 'is_published_fr',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'TourSeason.is_published_de'
        db.add_column(u'tour_tourseason', 'is_published_de',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'TourSeason.is_published_es'
        db.add_column(u'tour_tourseason', 'is_published_es',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'TourSeason.is_published_cn'
        db.add_column(u'tour_tourseason', 'is_published_cn',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'TourSeason.title_fr'
        db.add_column(u'tour_tourseason', 'title_fr',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'TourSeason.title_de'
        db.add_column(u'tour_tourseason', 'title_de',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'TourSeason.title_es'
        db.add_column(u'tour_tourseason', 'title_es',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'TourSeason.title_cn'
        db.add_column(u'tour_tourseason', 'title_cn',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'TourType.is_published_fr'
        db.add_column(u'tour_tourtype', 'is_published_fr',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'TourType.is_published_de'
        db.add_column(u'tour_tourtype', 'is_published_de',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'TourType.is_published_es'
        db.add_column(u'tour_tourtype', 'is_published_es',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'TourType.is_published_cn'
        db.add_column(u'tour_tourtype', 'is_published_cn',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'TourType.title_fr'
        db.add_column(u'tour_tourtype', 'title_fr',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'TourType.title_de'
        db.add_column(u'tour_tourtype', 'title_de',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'TourType.title_es'
        db.add_column(u'tour_tourtype', 'title_es',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'TourType.title_cn'
        db.add_column(u'tour_tourtype', 'title_cn',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'TourDuration.is_published_fr'
        db.delete_column(u'tour_tourduration', 'is_published_fr')

        # Deleting field 'TourDuration.is_published_de'
        db.delete_column(u'tour_tourduration', 'is_published_de')

        # Deleting field 'TourDuration.is_published_es'
        db.delete_column(u'tour_tourduration', 'is_published_es')

        # Deleting field 'TourDuration.is_published_cn'
        db.delete_column(u'tour_tourduration', 'is_published_cn')

        # Deleting field 'TourDuration.title_fr'
        db.delete_column(u'tour_tourduration', 'title_fr')

        # Deleting field 'TourDuration.title_de'
        db.delete_column(u'tour_tourduration', 'title_de')

        # Deleting field 'TourDuration.title_es'
        db.delete_column(u'tour_tourduration', 'title_es')

        # Deleting field 'TourDuration.title_cn'
        db.delete_column(u'tour_tourduration', 'title_cn')

        # Deleting field 'TourRegion.title_fr'
        db.delete_column(u'tour_tourregion', 'title_fr')

        # Deleting field 'TourRegion.title_de'
        db.delete_column(u'tour_tourregion', 'title_de')

        # Deleting field 'TourRegion.title_es'
        db.delete_column(u'tour_tourregion', 'title_es')

        # Deleting field 'TourRegion.title_cn'
        db.delete_column(u'tour_tourregion', 'title_cn')

        # Deleting field 'TourRegion.map_title_fr'
        db.delete_column(u'tour_tourregion', 'map_title_fr')

        # Deleting field 'TourRegion.map_title_de'
        db.delete_column(u'tour_tourregion', 'map_title_de')

        # Deleting field 'TourRegion.map_title_es'
        db.delete_column(u'tour_tourregion', 'map_title_es')

        # Deleting field 'TourRegion.map_title_cn'
        db.delete_column(u'tour_tourregion', 'map_title_cn')

        # Deleting field 'Tour.is_published_fr'
        db.delete_column(u'tour_tour', 'is_published_fr')

        # Deleting field 'Tour.is_published_de'
        db.delete_column(u'tour_tour', 'is_published_de')

        # Deleting field 'Tour.is_published_es'
        db.delete_column(u'tour_tour', 'is_published_es')

        # Deleting field 'Tour.is_published_cn'
        db.delete_column(u'tour_tour', 'is_published_cn')

        # Deleting field 'Tour.title_fr'
        db.delete_column(u'tour_tour', 'title_fr')

        # Deleting field 'Tour.title_de'
        db.delete_column(u'tour_tour', 'title_de')

        # Deleting field 'Tour.title_es'
        db.delete_column(u'tour_tour', 'title_es')

        # Deleting field 'Tour.title_cn'
        db.delete_column(u'tour_tour', 'title_cn')

        # Deleting field 'Tour.accommodation_type_fr'
        db.delete_column(u'tour_tour', 'accommodation_type_fr')

        # Deleting field 'Tour.accommodation_type_de'
        db.delete_column(u'tour_tour', 'accommodation_type_de')

        # Deleting field 'Tour.accommodation_type_es'
        db.delete_column(u'tour_tour', 'accommodation_type_es')

        # Deleting field 'Tour.accommodation_type_cn'
        db.delete_column(u'tour_tour', 'accommodation_type_cn')

        # Deleting field 'Tour.teaser_fr'
        db.delete_column(u'tour_tour', 'teaser_fr')

        # Deleting field 'Tour.teaser_de'
        db.delete_column(u'tour_tour', 'teaser_de')

        # Deleting field 'Tour.teaser_es'
        db.delete_column(u'tour_tour', 'teaser_es')

        # Deleting field 'Tour.teaser_cn'
        db.delete_column(u'tour_tour', 'teaser_cn')

        # Deleting field 'Tour.text_fr'
        db.delete_column(u'tour_tour', 'text_fr')

        # Deleting field 'Tour.text_de'
        db.delete_column(u'tour_tour', 'text_de')

        # Deleting field 'Tour.text_es'
        db.delete_column(u'tour_tour', 'text_es')

        # Deleting field 'Tour.text_cn'
        db.delete_column(u'tour_tour', 'text_cn')

        # Deleting field 'Tour.teaser_popular_fr'
        db.delete_column(u'tour_tour', 'teaser_popular_fr')

        # Deleting field 'Tour.teaser_popular_de'
        db.delete_column(u'tour_tour', 'teaser_popular_de')

        # Deleting field 'Tour.teaser_popular_es'
        db.delete_column(u'tour_tour', 'teaser_popular_es')

        # Deleting field 'Tour.teaser_popular_cn'
        db.delete_column(u'tour_tour', 'teaser_popular_cn')

        # Deleting field 'Tour.img_popular_fr'
        db.delete_column(u'tour_tour', 'img_popular_fr')

        # Deleting field 'Tour.img_popular_de'
        db.delete_column(u'tour_tour', 'img_popular_de')

        # Deleting field 'Tour.img_popular_es'
        db.delete_column(u'tour_tour', 'img_popular_es')

        # Deleting field 'Tour.img_popular_cn'
        db.delete_column(u'tour_tour', 'img_popular_cn')

        # Deleting field 'TourSeason.is_published_fr'
        db.delete_column(u'tour_tourseason', 'is_published_fr')

        # Deleting field 'TourSeason.is_published_de'
        db.delete_column(u'tour_tourseason', 'is_published_de')

        # Deleting field 'TourSeason.is_published_es'
        db.delete_column(u'tour_tourseason', 'is_published_es')

        # Deleting field 'TourSeason.is_published_cn'
        db.delete_column(u'tour_tourseason', 'is_published_cn')

        # Deleting field 'TourSeason.title_fr'
        db.delete_column(u'tour_tourseason', 'title_fr')

        # Deleting field 'TourSeason.title_de'
        db.delete_column(u'tour_tourseason', 'title_de')

        # Deleting field 'TourSeason.title_es'
        db.delete_column(u'tour_tourseason', 'title_es')

        # Deleting field 'TourSeason.title_cn'
        db.delete_column(u'tour_tourseason', 'title_cn')

        # Deleting field 'TourType.is_published_fr'
        db.delete_column(u'tour_tourtype', 'is_published_fr')

        # Deleting field 'TourType.is_published_de'
        db.delete_column(u'tour_tourtype', 'is_published_de')

        # Deleting field 'TourType.is_published_es'
        db.delete_column(u'tour_tourtype', 'is_published_es')

        # Deleting field 'TourType.is_published_cn'
        db.delete_column(u'tour_tourtype', 'is_published_cn')

        # Deleting field 'TourType.title_fr'
        db.delete_column(u'tour_tourtype', 'title_fr')

        # Deleting field 'TourType.title_de'
        db.delete_column(u'tour_tourtype', 'title_de')

        # Deleting field 'TourType.title_es'
        db.delete_column(u'tour_tourtype', 'title_es')

        # Deleting field 'TourType.title_cn'
        db.delete_column(u'tour_tourtype', 'title_cn')


    models = {
        u'tour.tour': {
            'Meta': {'ordering': "['weight']", 'object_name': 'Tour'},
            'accommodation_type': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'accommodation_type_cn': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'accommodation_type_de': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'accommodation_type_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'accommodation_type_es': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'accommodation_type_fr': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'accommodation_type_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'geography': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['tour.TourRegion']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'img_popular': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'img_popular_cn': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_popular_de': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_popular_en': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_popular_es': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_popular_fr': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_popular_ru': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'is_popular': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_cn': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_de': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_en': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_es': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_fr': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_ru': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'season': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['tour.TourSeason']", 'symmetrical': 'False', 'blank': 'True'}),
            't_duration': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['tour.TourDuration']", 'symmetrical': 'False'}),
            'teaser': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'teaser_cn': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'teaser_de': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'teaser_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'teaser_es': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'teaser_fr': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'teaser_popular': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'teaser_popular_cn': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'teaser_popular_de': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'teaser_popular_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'teaser_popular_es': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'teaser_popular_fr': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'teaser_popular_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'teaser_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'text': ('ckeditor.fields.RichTextField', [], {'blank': 'True'}),
            'text_cn': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text_de': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text_es': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text_ru': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_cn': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_de': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_es': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_fr': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['tour.TourType']", 'symmetrical': 'False', 'blank': 'True'}),
            'weight': ('django.db.models.fields.SmallIntegerField', [], {'default': '10000'})
        },
        u'tour.tourduration': {
            'Meta': {'ordering': "['weight']", 'object_name': 'TourDuration'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_cn': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_de': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_en': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_es': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_fr': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_ru': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_cn': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_de': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_es': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_fr': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.SmallIntegerField', [], {'default': '10000'})
        },
        u'tour.tourregion': {
            'Meta': {'ordering': "['weight']", 'object_name': 'TourRegion'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'map_title': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'map_title_cn': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'map_title_de': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'map_title_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'map_title_es': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'map_title_fr': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'map_title_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_cn': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_de': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_es': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_fr': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.SmallIntegerField', [], {'default': '10000'})
        },
        u'tour.tourseason': {
            'Meta': {'ordering': "['weight']", 'object_name': 'TourSeason'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img_on_main': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_cn': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_de': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_en': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_es': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_fr': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_ru': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_cn': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_de': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_es': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_fr': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.SmallIntegerField', [], {'default': '10000'})
        },
        u'tour.tourtype': {
            'Meta': {'ordering': "['weight']", 'object_name': 'TourType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_cn': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_de': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_en': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_es': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_fr': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_ru': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'on_main': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_cn': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_de': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_es': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_fr': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.SmallIntegerField', [], {'default': '10000'})
        }
    }

    complete_apps = ['tour']