# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Tour.accommodation_type'
        db.add_column(u'tour_tour', 'accommodation_type',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=254),
                      keep_default=False)

        # Adding field 'Tour.accommodation_type_ru'
        db.add_column(u'tour_tour', 'accommodation_type_ru',
                      self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tour.accommodation_type_en'
        db.add_column(u'tour_tour', 'accommodation_type_en',
                      self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True),
                      keep_default=False)


        # Changing field 'Tour.teaser_en'
        db.alter_column(u'tour_tour', 'teaser_en', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Tour.teaser_ru'
        db.alter_column(u'tour_tour', 'teaser_ru', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Tour.teaser'
        db.alter_column(u'tour_tour', 'teaser', self.gf('django.db.models.fields.TextField')())

    def backwards(self, orm):
        # Deleting field 'Tour.accommodation_type'
        db.delete_column(u'tour_tour', 'accommodation_type')

        # Deleting field 'Tour.accommodation_type_ru'
        db.delete_column(u'tour_tour', 'accommodation_type_ru')

        # Deleting field 'Tour.accommodation_type_en'
        db.delete_column(u'tour_tour', 'accommodation_type_en')


        # Changing field 'Tour.teaser_en'
        db.alter_column(u'tour_tour', 'teaser_en', self.gf('ckeditor.fields.RichTextField')(null=True))

        # Changing field 'Tour.teaser_ru'
        db.alter_column(u'tour_tour', 'teaser_ru', self.gf('ckeditor.fields.RichTextField')(null=True))

        # Changing field 'Tour.teaser'
        db.alter_column(u'tour_tour', 'teaser', self.gf('ckeditor.fields.RichTextField')())

    models = {
        u'tour.tour': {
            'Meta': {'object_name': 'Tour'},
            'accommodation_type': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'accommodation_type_en': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'accommodation_type_ru': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'geography': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['tour.TourRegion']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'season': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['tour.TourSeason']", 'symmetrical': 'False', 'blank': 'True'}),
            't_duration': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['tour.TourDuration']", 'symmetrical': 'False'}),
            'teaser': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'teaser_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'teaser_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'text': ('ckeditor.fields.RichTextField', [], {'blank': 'True'}),
            'text_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text_ru': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['tour.TourType']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'tour.tourduration': {
            'Meta': {'ordering': "['weight']", 'object_name': 'TourDuration'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'})
        },
        u'tour.tourregion': {
            'Meta': {'ordering': "['weight']", 'object_name': 'TourRegion'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'})
        },
        u'tour.tourseason': {
            'Meta': {'ordering': "['weight']", 'object_name': 'TourSeason'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'})
        },
        u'tour.tourtype': {
            'Meta': {'ordering': "['weight']", 'object_name': 'TourType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['tour']