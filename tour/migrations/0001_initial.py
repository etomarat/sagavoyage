# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'TourType'
        db.create_table(u'tour_tourtype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_published', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('title_ru', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('title_en', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('weight', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'tour', ['TourType'])

        # Adding model 'TourRegion'
        db.create_table(u'tour_tourregion', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_published', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('title_ru', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('title_en', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('weight', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'tour', ['TourRegion'])

        # Adding model 'TourSeason'
        db.create_table(u'tour_tourseason', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_published', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('title_ru', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('title_en', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('weight', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'tour', ['TourSeason'])

        # Adding model 'TourDuration'
        db.create_table(u'tour_tourduration', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_published', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('title_ru', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('title_en', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('weight', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'tour', ['TourDuration'])

        # Adding model 'Tour'
        db.create_table(u'tour_tour', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_published', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('title_ru', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('title_en', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('teaser', self.gf('ckeditor.fields.RichTextField')(blank=True)),
            ('teaser_ru', self.gf('ckeditor.fields.RichTextField')(null=True, blank=True)),
            ('teaser_en', self.gf('ckeditor.fields.RichTextField')(null=True, blank=True)),
            ('text', self.gf('ckeditor.fields.RichTextField')(blank=True)),
            ('text_ru', self.gf('ckeditor.fields.RichTextField')(null=True, blank=True)),
            ('text_en', self.gf('ckeditor.fields.RichTextField')(null=True, blank=True)),
            ('img', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
        ))
        db.send_create_signal(u'tour', ['Tour'])

        # Adding M2M table for field geography on 'Tour'
        m2m_table_name = db.shorten_name(u'tour_tour_geography')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('tour', models.ForeignKey(orm[u'tour.tour'], null=False)),
            ('tourregion', models.ForeignKey(orm[u'tour.tourregion'], null=False))
        ))
        db.create_unique(m2m_table_name, ['tour_id', 'tourregion_id'])

        # Adding M2M table for field season on 'Tour'
        m2m_table_name = db.shorten_name(u'tour_tour_season')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('tour', models.ForeignKey(orm[u'tour.tour'], null=False)),
            ('tourseason', models.ForeignKey(orm[u'tour.tourseason'], null=False))
        ))
        db.create_unique(m2m_table_name, ['tour_id', 'tourseason_id'])

        # Adding M2M table for field type on 'Tour'
        m2m_table_name = db.shorten_name(u'tour_tour_type')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('tour', models.ForeignKey(orm[u'tour.tour'], null=False)),
            ('tourtype', models.ForeignKey(orm[u'tour.tourtype'], null=False))
        ))
        db.create_unique(m2m_table_name, ['tour_id', 'tourtype_id'])

        # Adding M2M table for field t_duration on 'Tour'
        m2m_table_name = db.shorten_name(u'tour_tour_t_duration')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('tour', models.ForeignKey(orm[u'tour.tour'], null=False)),
            ('tourduration', models.ForeignKey(orm[u'tour.tourduration'], null=False))
        ))
        db.create_unique(m2m_table_name, ['tour_id', 'tourduration_id'])


    def backwards(self, orm):
        # Deleting model 'TourType'
        db.delete_table(u'tour_tourtype')

        # Deleting model 'TourRegion'
        db.delete_table(u'tour_tourregion')

        # Deleting model 'TourSeason'
        db.delete_table(u'tour_tourseason')

        # Deleting model 'TourDuration'
        db.delete_table(u'tour_tourduration')

        # Deleting model 'Tour'
        db.delete_table(u'tour_tour')

        # Removing M2M table for field geography on 'Tour'
        db.delete_table(db.shorten_name(u'tour_tour_geography'))

        # Removing M2M table for field season on 'Tour'
        db.delete_table(db.shorten_name(u'tour_tour_season'))

        # Removing M2M table for field type on 'Tour'
        db.delete_table(db.shorten_name(u'tour_tour_type'))

        # Removing M2M table for field t_duration on 'Tour'
        db.delete_table(db.shorten_name(u'tour_tour_t_duration'))


    models = {
        u'tour.tour': {
            'Meta': {'object_name': 'Tour'},
            'geography': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['tour.TourRegion']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'season': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['tour.TourSeason']", 'symmetrical': 'False', 'blank': 'True'}),
            't_duration': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['tour.TourDuration']", 'symmetrical': 'False', 'blank': 'True'}),
            'teaser': ('ckeditor.fields.RichTextField', [], {'blank': 'True'}),
            'teaser_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'teaser_ru': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text': ('ckeditor.fields.RichTextField', [], {'blank': 'True'}),
            'text_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text_ru': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['tour.TourType']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'tour.tourduration': {
            'Meta': {'ordering': "['weight']", 'object_name': 'TourDuration'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'})
        },
        u'tour.tourregion': {
            'Meta': {'ordering': "['weight']", 'object_name': 'TourRegion'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'})
        },
        u'tour.tourseason': {
            'Meta': {'ordering': "['weight']", 'object_name': 'TourSeason'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'})
        },
        u'tour.tourtype': {
            'Meta': {'ordering': "['weight']", 'object_name': 'TourType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['tour']