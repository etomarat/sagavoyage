# coding: utf-8

from django.conf.urls import patterns, url

from .views import *


urlpatterns = patterns(
    '',
    url(r'^$', TourList.as_view(), name='list'),
    url(r'^(?P<pk>\d+)$', TourDetail.as_view(), name='detail'),
    )

# EOF