# coding: utf-8

from django.contrib import admin

from kernel.admin import BaseTranslationAdmin, PublishedSortableTranslationAdmin

from .models import *


class TourAdmin(BaseTranslationAdmin):
    list_display = ['__unicode__', 'is_popular', 'is_published']
    list_editable = ['is_popular', 'is_published']

admin.site.register(Tour, TourAdmin)


admin.site.register(TourRegion, PublishedSortableTranslationAdmin)
admin.site.register(TourType, PublishedSortableTranslationAdmin)
admin.site.register(TourDuration, PublishedSortableTranslationAdmin)
admin.site.register(TourSeason, PublishedSortableTranslationAdmin)

# EOF