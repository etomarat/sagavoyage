# coding: utf-8
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, DetailView

from kernel.views import PublishedQuerysetMixin

from .models import *


class TourList(PublishedQuerysetMixin, ListView):
    model = Tour

    def dispatch(self, request, *args, **kwargs):
        self.filters = dict()
        filter_fields = [
            'geography',
            'season',
            'type',
            't_duration',
            ]
        for field in filter_fields:
            args = set()
            for i in request.REQUEST.getlist(field + '[]') or [request.REQUEST.get(field, '')]:
                try:
                    args.add(int(i))
                except ValueError:
                    pass
            if args:
                self.filters.update({field: args})
        return super(TourList, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return super(TourList, self).get_queryset().filter(
            **dict((i + '__in', j) for i, j in self.filters.items())
            ).distinct()

    def get_context_data(self, **kwargs):
        context = super(TourList, self).get_context_data(**kwargs)
        if self.request.method == 'GET':
            context.update(
                tour_regions=TourRegion.get_exists_tours_qset(),
                tour_seasons=TourSeason.get_exists_tours_qset(),
                tour_types=TourType.get_exists_tours_qset(),
                tour_durations=TourDuration.get_exists_tours_qset(),
                checked = self.filters,
                )
        return context

    def get_template_names(self):
        if self.request.method == 'POST':
            return ['includes/tour_grid.html']
        return super(TourList, self).get_template_names()

    def post(self, request, *args, **kwargs):
        return self.get(request, *args, **kwargs)


class TourDetail(PublishedQuerysetMixin, DetailView):
    model = Tour

# EOF