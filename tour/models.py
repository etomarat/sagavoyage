# coding: utf-8

from django.core.urlresolvers import reverse
from django.core.validators import MaxLengthValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _

from ckeditor.fields import RichTextField
# from ckeditor.widgets import CKEditorWidget

from kernel.db import PublishModel, TitleMixin, SortableMixin


class NotEmptyToursMixin(object):
    @classmethod
    def get_exists_tours_qset(self):
        return self.objects.published().annotate(models.Count('tour')).filter(tour__count__gt = 0)


class TourType(PublishModel, TitleMixin, SortableMixin, NotEmptyToursMixin):
    on_main = models.BooleanField(verbose_name=_(u'Выводить на главную'), default=False)
    
    class Meta(SortableMixin.Meta):
        verbose_name = _(u'Вид туризмы')
        verbose_name_plural = _(u'Виды туризма')
    
    @property
    def absolute_url(self):
        return '%(path)s?type=%(pk)d' % dict(
            path=reverse('tour:list'),
            pk=self.pk,
            )
    
    class TMeta:
        fields = ['is_published', 'title']
        ordering = ['weight']
        

class TourRegion(PublishModel, TitleMixin, SortableMixin, NotEmptyToursMixin):
    map_title = models.CharField(
        verbose_name=_(u'Название для карты (напр. Центральный)'),
        help_text=_(u'Федеральный округ'),
        max_length = 254,
        )

    class Meta(SortableMixin.Meta):
        verbose_name = _(u'География тура')
        verbose_name_plural = _(u'Географии туров')
        ordering = ['weight']

    class TMeta:
        fields = ['title', 'map_title']

    @property
    def absolute_url(self):
        return '%(path)s?geography=%(pk)d' % dict(
            path=reverse('tour:list'),
            pk=self.pk,
            )

    @property
    def title_for_map(self):
        return self.map_title or self.title


class TourSeason(PublishModel, TitleMixin, SortableMixin, NotEmptyToursMixin):
    img_on_main = models.ImageField(
        verbose_name=_(u'Изображение для главной'),
        help_text=u'610x270px',
        upload_to='tour',
        blank=True,
        )

    class Meta(SortableMixin.Meta):
        verbose_name = _(u'Время года')
        verbose_name_plural = _(u'Времена года')
        ordering = ['weight']

    @property
    def absolute_url(self):
        return '%(path)s?season=%(pk)d' % dict(
            path=reverse('tour:list'),
            pk=self.pk,
            )
    
    class TMeta:
        fields = ['is_published', 'title']

class TourDuration(PublishModel, TitleMixin, SortableMixin, NotEmptyToursMixin):
    class Meta(SortableMixin.Meta):
        verbose_name = _(u'Продолжительность тура')
        verbose_name_plural = _(u'Продолжительности туров')
        ordering = ['weight']
    
    class TMeta:
        fields = ['is_published', 'title']


class Tour(PublishModel, TitleMixin, SortableMixin):
    geography = models.ManyToManyField(to=TourRegion, verbose_name=_(u'География тура'), blank=True)
    season = models.ManyToManyField(to=TourSeason, verbose_name=_(u'Время года'), blank=True)
    type = models.ManyToManyField(to=TourType, verbose_name=_(u'Виды туризма'), blank=True)
    t_duration = models.ManyToManyField(to=TourDuration, verbose_name=_(u'Продолжительность'),)
    accommodation_type = models.CharField(verbose_name=_(u'Проживание'), max_length = 254,)
    teaser = models.TextField(
        verbose_name=_(u'Анонс'),
        help_text=_(u'Рекомендуется не вводить более 300 символов'),
        blank=True,
        validators=[MaxLengthValidator(350)],
        )

    text = RichTextField(verbose_name=_(u'Текст'), blank=True)
    img = models.ImageField(
        verbose_name=_(u'Изображение'),
        help_text=u'400x560px',
        upload_to='tour',
        blank=True,
        )

    is_popular = models.BooleanField(verbose_name=_(u'Популярный тур'), default=False)
    teaser_popular = models.TextField(
        verbose_name=_(u'Анонс для популярных туров'),
        help_text=_(u'Рекомендуется не вводить более 150 символов'),
        blank=True,
        validators=[MaxLengthValidator(150)],
        )
    img_popular = models.ImageField(
        verbose_name=_(u'Изображение для популярных туров'),
        help_text=u'400x270px',
        upload_to='tour_small',
        blank=True,
        )

    class Meta:
        verbose_name = _(u'Тур')
        verbose_name_plural = _(u'Туры')
        ordering = ['weight']

    class TMeta:
        exclude = ['geography', 'season', 'type', 't_duration', 'img',]

    @property
    @models.permalink
    def absolute_url(self):
        return ('tour:detail', [self.pk])

# EOF