from django.contrib import admin

from modeltranslation.admin import TranslationTabularInline
from kernel.admin import  SingletonTranslationAdmin
from .models import AboutPage, Slide, Certificate, Reviews



class SlideInline(TranslationTabularInline):
    model = Slide
    extra = 0
    
class CertificateInline(TranslationTabularInline):
    model = Certificate
    extra = 0
    
class ReviewsInline(TranslationTabularInline):
    model = Reviews
    extra = 0

class AboutPageAdmin(SingletonTranslationAdmin):
    inlines = [SlideInline, CertificateInline, ReviewsInline]

admin.site.register(AboutPage, AboutPageAdmin)
