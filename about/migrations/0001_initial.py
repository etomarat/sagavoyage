# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'AboutPage'
        db.create_table(u'about_aboutpage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('text', self.gf('ckeditor.fields.RichTextField')(blank=True)),
            ('teaser', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('header1', self.gf('django.db.models.fields.CharField')(default=u'\u041d\u0430\u0448\u0438 \u043f\u0440\u0438\u043d\u0446\u0438\u043f\u044b \u0440\u0430\u0431\u043e\u0442\u044b', max_length=255, blank=True)),
            ('princip1_img', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('princip1_title', self.gf('django.db.models.fields.CharField')(default=u'\u041e\u0440\u0438\u0435\u043d\u0442\u0430\u0446\u0438\u044f \u043d\u0430 \u043a\u043b\u0438\u0435\u043d\u0442\u0430', max_length=255, blank=True)),
            ('princip1_description', self.gf('django.db.models.fields.TextField')(default=u'\u0412\u043d\u0438\u043c\u0430\u0442\u0435\u043b\u044c\u043d\u043e\u0435 \u043e\u0442\u043d\u043e\u0448\u0435\u043d\u0438\u0435 \u0438 \u043f\u043e\u043d\u0438\u043c\u0430\u043d\u0438\u0435 \u043f\u043e\u0442\u0440\u0435\u0431\u043d\u043e\u0441\u0442\u0435\u0439 \u043a\u043b\u0438\u0435\u043d\u0442\u0430', blank=True)),
            ('princip2_img', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('princip2_title', self.gf('django.db.models.fields.CharField')(default=u'\u0412\u044b\u0441\u043e\u043a\u043e\u0435 \u043a\u0430\u0447\u0435\u0441\u0442\u0432\u043e \u043e\u043a\u0430\u0437\u0430\u043d\u0438\u044f \u0443\u0441\u043b\u0443\u0433 \u0438 \u043e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0446\u0438\u0438 \u0442\u0443\u0440\u043e\u0432', max_length=255, blank=True)),
            ('princip2_description', self.gf('django.db.models.fields.TextField')(default=u'\u041d\u0435\u0437\u0430\u0432\u0438\u0441\u0438\u043c\u043e \u043e\u0442 \u0431\u044e\u0434\u0436\u0435\u0442\u0430 \u0442\u0443\u0440\u0430, \u043c\u044b \u043f\u0440\u043e\u0434\u0443\u043c\u044b\u0432\u0430\u0435\u043c \u043a\u0430\u0436\u0434\u0443\u044e \u043c\u0438\u043d\u0443\u0442\u0443 \u043f\u0443\u0442\u0435\u0448\u0435\u0441\u0442\u0432\u0438\u044f \u043a\u043b\u0438\u0435\u043d\u0442\u0430', blank=True)),
            ('princip3_img', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('princip3_title', self.gf('django.db.models.fields.CharField')(default=u'\u0420\u0435\u043f\u0443\u0442\u0430\u0446\u0438\u044f \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438', max_length=255, blank=True)),
            ('princip3_description', self.gf('django.db.models.fields.TextField')(default=u'\u041c\u044b \u0446\u0435\u043d\u0438\u043c \u0434\u043e\u0432\u0435\u0440\u0438\u0435 \u043d\u0430\u0448\u0438\u0445 \u043a\u043b\u0438\u0435\u043d\u0442\u043e\u0432 \u0438 \u043f\u0430\u0440\u0442\u043d\u0435\u0440\u043e\u0432', blank=True)),
            ('header2', self.gf('django.db.models.fields.CharField')(default=u'\u041d\u0430\u0448\u0438 \u043d\u0430\u0433\u0440\u0430\u0434\u044b \u0438 \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u044b', max_length=255, blank=True)),
            ('header3', self.gf('django.db.models.fields.CharField')(default=u'\u041e\u0442\u0437\u044b\u0432\u044b \u043e \u043d\u0430\u0441', max_length=255, blank=True)),
        ))
        db.send_create_signal(u'about', ['AboutPage'])

        # Adding model 'Slide'
        db.create_table(u'about_slide', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_published', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('is_published_ru', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('is_published_en', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('weight', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
            ('teaser', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('img', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('link', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['about.AboutPage'])),
        ))
        db.send_create_signal(u'about', ['Slide'])

        # Adding model 'Reviews'
        db.create_table(u'about_reviews', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_published', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('is_published_ru', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('is_published_en', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('weight', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('message', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('img', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['about.AboutPage'])),
        ))
        db.send_create_signal(u'about', ['Reviews'])

        # Adding model 'Certificate'
        db.create_table(u'about_certificate', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_published', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('is_published_ru', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('is_published_en', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('weight', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
            ('thumb_img', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('img', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['about.AboutPage'])),
        ))
        db.send_create_signal(u'about', ['Certificate'])


    def backwards(self, orm):
        # Deleting model 'AboutPage'
        db.delete_table(u'about_aboutpage')

        # Deleting model 'Slide'
        db.delete_table(u'about_slide')

        # Deleting model 'Reviews'
        db.delete_table(u'about_reviews')

        # Deleting model 'Certificate'
        db.delete_table(u'about_certificate')


    models = {
        u'about.aboutpage': {
            'Meta': {'object_name': 'AboutPage'},
            'header1': ('django.db.models.fields.CharField', [], {'default': "u'\\u041d\\u0430\\u0448\\u0438 \\u043f\\u0440\\u0438\\u043d\\u0446\\u0438\\u043f\\u044b \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b'", 'max_length': '255', 'blank': 'True'}),
            'header2': ('django.db.models.fields.CharField', [], {'default': "u'\\u041d\\u0430\\u0448\\u0438 \\u043d\\u0430\\u0433\\u0440\\u0430\\u0434\\u044b \\u0438 \\u0441\\u0435\\u0440\\u0442\\u0438\\u0444\\u0438\\u043a\\u0430\\u0442\\u044b'", 'max_length': '255', 'blank': 'True'}),
            'header3': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0442\\u0437\\u044b\\u0432\\u044b \\u043e \\u043d\\u0430\\u0441'", 'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'princip1_description': ('django.db.models.fields.TextField', [], {'default': "u'\\u0412\\u043d\\u0438\\u043c\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u043e\\u0435 \\u043e\\u0442\\u043d\\u043e\\u0448\\u0435\\u043d\\u0438\\u0435 \\u0438 \\u043f\\u043e\\u043d\\u0438\\u043c\\u0430\\u043d\\u0438\\u0435 \\u043f\\u043e\\u0442\\u0440\\u0435\\u0431\\u043d\\u043e\\u0441\\u0442\\u0435\\u0439 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430'", 'blank': 'True'}),
            'princip1_img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'princip1_title': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0440\\u0438\\u0435\\u043d\\u0442\\u0430\\u0446\\u0438\\u044f \\u043d\\u0430 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430'", 'max_length': '255', 'blank': 'True'}),
            'princip2_description': ('django.db.models.fields.TextField', [], {'default': "u'\\u041d\\u0435\\u0437\\u0430\\u0432\\u0438\\u0441\\u0438\\u043c\\u043e \\u043e\\u0442 \\u0431\\u044e\\u0434\\u0436\\u0435\\u0442\\u0430 \\u0442\\u0443\\u0440\\u0430, \\u043c\\u044b \\u043f\\u0440\\u043e\\u0434\\u0443\\u043c\\u044b\\u0432\\u0430\\u0435\\u043c \\u043a\\u0430\\u0436\\u0434\\u0443\\u044e \\u043c\\u0438\\u043d\\u0443\\u0442\\u0443 \\u043f\\u0443\\u0442\\u0435\\u0448\\u0435\\u0441\\u0442\\u0432\\u0438\\u044f \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430'", 'blank': 'True'}),
            'princip2_img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'princip2_title': ('django.db.models.fields.CharField', [], {'default': "u'\\u0412\\u044b\\u0441\\u043e\\u043a\\u043e\\u0435 \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u043e\\u043a\\u0430\\u0437\\u0430\\u043d\\u0438\\u044f \\u0443\\u0441\\u043b\\u0443\\u0433 \\u0438 \\u043e\\u0440\\u0433\\u0430\\u043d\\u0438\\u0437\\u0430\\u0446\\u0438\\u0438 \\u0442\\u0443\\u0440\\u043e\\u0432'", 'max_length': '255', 'blank': 'True'}),
            'princip3_description': ('django.db.models.fields.TextField', [], {'default': "u'\\u041c\\u044b \\u0446\\u0435\\u043d\\u0438\\u043c \\u0434\\u043e\\u0432\\u0435\\u0440\\u0438\\u0435 \\u043d\\u0430\\u0448\\u0438\\u0445 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u043e\\u0432 \\u0438 \\u043f\\u0430\\u0440\\u0442\\u043d\\u0435\\u0440\\u043e\\u0432'", 'blank': 'True'}),
            'princip3_img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'princip3_title': ('django.db.models.fields.CharField', [], {'default': "u'\\u0420\\u0435\\u043f\\u0443\\u0442\\u0430\\u0446\\u0438\\u044f \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438'", 'max_length': '255', 'blank': 'True'}),
            'teaser': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'text': ('ckeditor.fields.RichTextField', [], {'blank': 'True'})
        },
        u'about.certificate': {
            'Meta': {'object_name': 'Certificate'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_en': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_ru': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['about.AboutPage']"}),
            'thumb_img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'weight': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'})
        },
        u'about.reviews': {
            'Meta': {'object_name': 'Reviews'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_en': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_ru': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['about.AboutPage']"}),
            'weight': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'})
        },
        u'about.slide': {
            'Meta': {'object_name': 'Slide'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_en': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_ru': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['about.AboutPage']"}),
            'teaser': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'weight': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['about']