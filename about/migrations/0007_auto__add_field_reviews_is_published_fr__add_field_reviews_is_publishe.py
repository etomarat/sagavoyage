# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Reviews.is_published_fr'
        db.add_column(u'about_reviews', 'is_published_fr',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Reviews.is_published_de'
        db.add_column(u'about_reviews', 'is_published_de',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Reviews.is_published_es'
        db.add_column(u'about_reviews', 'is_published_es',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Reviews.is_published_cn'
        db.add_column(u'about_reviews', 'is_published_cn',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Reviews.name_fr'
        db.add_column(u'about_reviews', 'name_fr',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Reviews.name_de'
        db.add_column(u'about_reviews', 'name_de',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Reviews.name_es'
        db.add_column(u'about_reviews', 'name_es',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Reviews.name_cn'
        db.add_column(u'about_reviews', 'name_cn',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Reviews.message_fr'
        db.add_column(u'about_reviews', 'message_fr',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Reviews.message_de'
        db.add_column(u'about_reviews', 'message_de',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Reviews.message_es'
        db.add_column(u'about_reviews', 'message_es',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Reviews.message_cn'
        db.add_column(u'about_reviews', 'message_cn',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Reviews.img_fr'
        db.add_column(u'about_reviews', 'img_fr',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Reviews.img_de'
        db.add_column(u'about_reviews', 'img_de',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Reviews.img_es'
        db.add_column(u'about_reviews', 'img_es',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Reviews.img_cn'
        db.add_column(u'about_reviews', 'img_cn',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.text_fr'
        db.add_column(u'about_aboutpage', 'text_fr',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.text_de'
        db.add_column(u'about_aboutpage', 'text_de',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.text_es'
        db.add_column(u'about_aboutpage', 'text_es',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.text_cn'
        db.add_column(u'about_aboutpage', 'text_cn',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.header1_fr'
        db.add_column(u'about_aboutpage', 'header1_fr',
                      self.gf('django.db.models.fields.CharField')(default=u'\u041d\u0430\u0448\u0438 \u043f\u0440\u0438\u043d\u0446\u0438\u043f\u044b \u0440\u0430\u0431\u043e\u0442\u044b', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.header1_de'
        db.add_column(u'about_aboutpage', 'header1_de',
                      self.gf('django.db.models.fields.CharField')(default=u'\u041d\u0430\u0448\u0438 \u043f\u0440\u0438\u043d\u0446\u0438\u043f\u044b \u0440\u0430\u0431\u043e\u0442\u044b', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.header1_es'
        db.add_column(u'about_aboutpage', 'header1_es',
                      self.gf('django.db.models.fields.CharField')(default=u'\u041d\u0430\u0448\u0438 \u043f\u0440\u0438\u043d\u0446\u0438\u043f\u044b \u0440\u0430\u0431\u043e\u0442\u044b', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.header1_cn'
        db.add_column(u'about_aboutpage', 'header1_cn',
                      self.gf('django.db.models.fields.CharField')(default=u'\u041d\u0430\u0448\u0438 \u043f\u0440\u0438\u043d\u0446\u0438\u043f\u044b \u0440\u0430\u0431\u043e\u0442\u044b', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip1_img_fr'
        db.add_column(u'about_aboutpage', 'princip1_img_fr',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip1_img_de'
        db.add_column(u'about_aboutpage', 'princip1_img_de',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip1_img_es'
        db.add_column(u'about_aboutpage', 'princip1_img_es',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip1_img_cn'
        db.add_column(u'about_aboutpage', 'princip1_img_cn',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip1_title_fr'
        db.add_column(u'about_aboutpage', 'princip1_title_fr',
                      self.gf('django.db.models.fields.CharField')(default=u'\u041e\u0440\u0438\u0435\u043d\u0442\u0430\u0446\u0438\u044f \u043d\u0430 \u043a\u043b\u0438\u0435\u043d\u0442\u0430', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip1_title_de'
        db.add_column(u'about_aboutpage', 'princip1_title_de',
                      self.gf('django.db.models.fields.CharField')(default=u'\u041e\u0440\u0438\u0435\u043d\u0442\u0430\u0446\u0438\u044f \u043d\u0430 \u043a\u043b\u0438\u0435\u043d\u0442\u0430', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip1_title_es'
        db.add_column(u'about_aboutpage', 'princip1_title_es',
                      self.gf('django.db.models.fields.CharField')(default=u'\u041e\u0440\u0438\u0435\u043d\u0442\u0430\u0446\u0438\u044f \u043d\u0430 \u043a\u043b\u0438\u0435\u043d\u0442\u0430', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip1_title_cn'
        db.add_column(u'about_aboutpage', 'princip1_title_cn',
                      self.gf('django.db.models.fields.CharField')(default=u'\u041e\u0440\u0438\u0435\u043d\u0442\u0430\u0446\u0438\u044f \u043d\u0430 \u043a\u043b\u0438\u0435\u043d\u0442\u0430', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip1_description_fr'
        db.add_column(u'about_aboutpage', 'princip1_description_fr',
                      self.gf('django.db.models.fields.TextField')(default=u'\u0412\u043d\u0438\u043c\u0430\u0442\u0435\u043b\u044c\u043d\u043e\u0435 \u043e\u0442\u043d\u043e\u0448\u0435\u043d\u0438\u0435 \u0438 \u043f\u043e\u043d\u0438\u043c\u0430\u043d\u0438\u0435 \u043f\u043e\u0442\u0440\u0435\u0431\u043d\u043e\u0441\u0442\u0435\u0439 \u043a\u043b\u0438\u0435\u043d\u0442\u0430', null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip1_description_de'
        db.add_column(u'about_aboutpage', 'princip1_description_de',
                      self.gf('django.db.models.fields.TextField')(default=u'\u0412\u043d\u0438\u043c\u0430\u0442\u0435\u043b\u044c\u043d\u043e\u0435 \u043e\u0442\u043d\u043e\u0448\u0435\u043d\u0438\u0435 \u0438 \u043f\u043e\u043d\u0438\u043c\u0430\u043d\u0438\u0435 \u043f\u043e\u0442\u0440\u0435\u0431\u043d\u043e\u0441\u0442\u0435\u0439 \u043a\u043b\u0438\u0435\u043d\u0442\u0430', null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip1_description_es'
        db.add_column(u'about_aboutpage', 'princip1_description_es',
                      self.gf('django.db.models.fields.TextField')(default=u'\u0412\u043d\u0438\u043c\u0430\u0442\u0435\u043b\u044c\u043d\u043e\u0435 \u043e\u0442\u043d\u043e\u0448\u0435\u043d\u0438\u0435 \u0438 \u043f\u043e\u043d\u0438\u043c\u0430\u043d\u0438\u0435 \u043f\u043e\u0442\u0440\u0435\u0431\u043d\u043e\u0441\u0442\u0435\u0439 \u043a\u043b\u0438\u0435\u043d\u0442\u0430', null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip1_description_cn'
        db.add_column(u'about_aboutpage', 'princip1_description_cn',
                      self.gf('django.db.models.fields.TextField')(default=u'\u0412\u043d\u0438\u043c\u0430\u0442\u0435\u043b\u044c\u043d\u043e\u0435 \u043e\u0442\u043d\u043e\u0448\u0435\u043d\u0438\u0435 \u0438 \u043f\u043e\u043d\u0438\u043c\u0430\u043d\u0438\u0435 \u043f\u043e\u0442\u0440\u0435\u0431\u043d\u043e\u0441\u0442\u0435\u0439 \u043a\u043b\u0438\u0435\u043d\u0442\u0430', null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip2_img_fr'
        db.add_column(u'about_aboutpage', 'princip2_img_fr',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip2_img_de'
        db.add_column(u'about_aboutpage', 'princip2_img_de',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip2_img_es'
        db.add_column(u'about_aboutpage', 'princip2_img_es',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip2_img_cn'
        db.add_column(u'about_aboutpage', 'princip2_img_cn',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip2_title_fr'
        db.add_column(u'about_aboutpage', 'princip2_title_fr',
                      self.gf('django.db.models.fields.CharField')(default=u'\u0412\u044b\u0441\u043e\u043a\u043e\u0435 \u043a\u0430\u0447\u0435\u0441\u0442\u0432\u043e \u043e\u043a\u0430\u0437\u0430\u043d\u0438\u044f \u0443\u0441\u043b\u0443\u0433 \u0438 \u043e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0446\u0438\u0438 \u0442\u0443\u0440\u043e\u0432', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip2_title_de'
        db.add_column(u'about_aboutpage', 'princip2_title_de',
                      self.gf('django.db.models.fields.CharField')(default=u'\u0412\u044b\u0441\u043e\u043a\u043e\u0435 \u043a\u0430\u0447\u0435\u0441\u0442\u0432\u043e \u043e\u043a\u0430\u0437\u0430\u043d\u0438\u044f \u0443\u0441\u043b\u0443\u0433 \u0438 \u043e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0446\u0438\u0438 \u0442\u0443\u0440\u043e\u0432', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip2_title_es'
        db.add_column(u'about_aboutpage', 'princip2_title_es',
                      self.gf('django.db.models.fields.CharField')(default=u'\u0412\u044b\u0441\u043e\u043a\u043e\u0435 \u043a\u0430\u0447\u0435\u0441\u0442\u0432\u043e \u043e\u043a\u0430\u0437\u0430\u043d\u0438\u044f \u0443\u0441\u043b\u0443\u0433 \u0438 \u043e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0446\u0438\u0438 \u0442\u0443\u0440\u043e\u0432', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip2_title_cn'
        db.add_column(u'about_aboutpage', 'princip2_title_cn',
                      self.gf('django.db.models.fields.CharField')(default=u'\u0412\u044b\u0441\u043e\u043a\u043e\u0435 \u043a\u0430\u0447\u0435\u0441\u0442\u0432\u043e \u043e\u043a\u0430\u0437\u0430\u043d\u0438\u044f \u0443\u0441\u043b\u0443\u0433 \u0438 \u043e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0446\u0438\u0438 \u0442\u0443\u0440\u043e\u0432', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip2_description_fr'
        db.add_column(u'about_aboutpage', 'princip2_description_fr',
                      self.gf('django.db.models.fields.TextField')(default=u'\u041d\u0435\u0437\u0430\u0432\u0438\u0441\u0438\u043c\u043e \u043e\u0442 \u0431\u044e\u0434\u0436\u0435\u0442\u0430 \u0442\u0443\u0440\u0430, \u043c\u044b \u043f\u0440\u043e\u0434\u0443\u043c\u044b\u0432\u0430\u0435\u043c \u043a\u0430\u0436\u0434\u0443\u044e \u043c\u0438\u043d\u0443\u0442\u0443 \u043f\u0443\u0442\u0435\u0448\u0435\u0441\u0442\u0432\u0438\u044f \u043a\u043b\u0438\u0435\u043d\u0442\u0430', null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip2_description_de'
        db.add_column(u'about_aboutpage', 'princip2_description_de',
                      self.gf('django.db.models.fields.TextField')(default=u'\u041d\u0435\u0437\u0430\u0432\u0438\u0441\u0438\u043c\u043e \u043e\u0442 \u0431\u044e\u0434\u0436\u0435\u0442\u0430 \u0442\u0443\u0440\u0430, \u043c\u044b \u043f\u0440\u043e\u0434\u0443\u043c\u044b\u0432\u0430\u0435\u043c \u043a\u0430\u0436\u0434\u0443\u044e \u043c\u0438\u043d\u0443\u0442\u0443 \u043f\u0443\u0442\u0435\u0448\u0435\u0441\u0442\u0432\u0438\u044f \u043a\u043b\u0438\u0435\u043d\u0442\u0430', null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip2_description_es'
        db.add_column(u'about_aboutpage', 'princip2_description_es',
                      self.gf('django.db.models.fields.TextField')(default=u'\u041d\u0435\u0437\u0430\u0432\u0438\u0441\u0438\u043c\u043e \u043e\u0442 \u0431\u044e\u0434\u0436\u0435\u0442\u0430 \u0442\u0443\u0440\u0430, \u043c\u044b \u043f\u0440\u043e\u0434\u0443\u043c\u044b\u0432\u0430\u0435\u043c \u043a\u0430\u0436\u0434\u0443\u044e \u043c\u0438\u043d\u0443\u0442\u0443 \u043f\u0443\u0442\u0435\u0448\u0435\u0441\u0442\u0432\u0438\u044f \u043a\u043b\u0438\u0435\u043d\u0442\u0430', null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip2_description_cn'
        db.add_column(u'about_aboutpage', 'princip2_description_cn',
                      self.gf('django.db.models.fields.TextField')(default=u'\u041d\u0435\u0437\u0430\u0432\u0438\u0441\u0438\u043c\u043e \u043e\u0442 \u0431\u044e\u0434\u0436\u0435\u0442\u0430 \u0442\u0443\u0440\u0430, \u043c\u044b \u043f\u0440\u043e\u0434\u0443\u043c\u044b\u0432\u0430\u0435\u043c \u043a\u0430\u0436\u0434\u0443\u044e \u043c\u0438\u043d\u0443\u0442\u0443 \u043f\u0443\u0442\u0435\u0448\u0435\u0441\u0442\u0432\u0438\u044f \u043a\u043b\u0438\u0435\u043d\u0442\u0430', null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip3_img_fr'
        db.add_column(u'about_aboutpage', 'princip3_img_fr',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip3_img_de'
        db.add_column(u'about_aboutpage', 'princip3_img_de',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip3_img_es'
        db.add_column(u'about_aboutpage', 'princip3_img_es',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip3_img_cn'
        db.add_column(u'about_aboutpage', 'princip3_img_cn',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip3_title_fr'
        db.add_column(u'about_aboutpage', 'princip3_title_fr',
                      self.gf('django.db.models.fields.CharField')(default=u'\u0420\u0435\u043f\u0443\u0442\u0430\u0446\u0438\u044f \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip3_title_de'
        db.add_column(u'about_aboutpage', 'princip3_title_de',
                      self.gf('django.db.models.fields.CharField')(default=u'\u0420\u0435\u043f\u0443\u0442\u0430\u0446\u0438\u044f \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip3_title_es'
        db.add_column(u'about_aboutpage', 'princip3_title_es',
                      self.gf('django.db.models.fields.CharField')(default=u'\u0420\u0435\u043f\u0443\u0442\u0430\u0446\u0438\u044f \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip3_title_cn'
        db.add_column(u'about_aboutpage', 'princip3_title_cn',
                      self.gf('django.db.models.fields.CharField')(default=u'\u0420\u0435\u043f\u0443\u0442\u0430\u0446\u0438\u044f \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip3_description_fr'
        db.add_column(u'about_aboutpage', 'princip3_description_fr',
                      self.gf('django.db.models.fields.TextField')(default=u'\u041c\u044b \u0446\u0435\u043d\u0438\u043c \u0434\u043e\u0432\u0435\u0440\u0438\u0435 \u043d\u0430\u0448\u0438\u0445 \u043a\u043b\u0438\u0435\u043d\u0442\u043e\u0432 \u0438 \u043f\u0430\u0440\u0442\u043d\u0435\u0440\u043e\u0432', null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip3_description_de'
        db.add_column(u'about_aboutpage', 'princip3_description_de',
                      self.gf('django.db.models.fields.TextField')(default=u'\u041c\u044b \u0446\u0435\u043d\u0438\u043c \u0434\u043e\u0432\u0435\u0440\u0438\u0435 \u043d\u0430\u0448\u0438\u0445 \u043a\u043b\u0438\u0435\u043d\u0442\u043e\u0432 \u0438 \u043f\u0430\u0440\u0442\u043d\u0435\u0440\u043e\u0432', null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip3_description_es'
        db.add_column(u'about_aboutpage', 'princip3_description_es',
                      self.gf('django.db.models.fields.TextField')(default=u'\u041c\u044b \u0446\u0435\u043d\u0438\u043c \u0434\u043e\u0432\u0435\u0440\u0438\u0435 \u043d\u0430\u0448\u0438\u0445 \u043a\u043b\u0438\u0435\u043d\u0442\u043e\u0432 \u0438 \u043f\u0430\u0440\u0442\u043d\u0435\u0440\u043e\u0432', null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.princip3_description_cn'
        db.add_column(u'about_aboutpage', 'princip3_description_cn',
                      self.gf('django.db.models.fields.TextField')(default=u'\u041c\u044b \u0446\u0435\u043d\u0438\u043c \u0434\u043e\u0432\u0435\u0440\u0438\u0435 \u043d\u0430\u0448\u0438\u0445 \u043a\u043b\u0438\u0435\u043d\u0442\u043e\u0432 \u0438 \u043f\u0430\u0440\u0442\u043d\u0435\u0440\u043e\u0432', null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.header2_fr'
        db.add_column(u'about_aboutpage', 'header2_fr',
                      self.gf('django.db.models.fields.CharField')(default=u'\u041d\u0430\u0448\u0438 \u043d\u0430\u0433\u0440\u0430\u0434\u044b \u0438 \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u044b', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.header2_de'
        db.add_column(u'about_aboutpage', 'header2_de',
                      self.gf('django.db.models.fields.CharField')(default=u'\u041d\u0430\u0448\u0438 \u043d\u0430\u0433\u0440\u0430\u0434\u044b \u0438 \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u044b', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.header2_es'
        db.add_column(u'about_aboutpage', 'header2_es',
                      self.gf('django.db.models.fields.CharField')(default=u'\u041d\u0430\u0448\u0438 \u043d\u0430\u0433\u0440\u0430\u0434\u044b \u0438 \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u044b', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.header2_cn'
        db.add_column(u'about_aboutpage', 'header2_cn',
                      self.gf('django.db.models.fields.CharField')(default=u'\u041d\u0430\u0448\u0438 \u043d\u0430\u0433\u0440\u0430\u0434\u044b \u0438 \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u044b', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.header3_fr'
        db.add_column(u'about_aboutpage', 'header3_fr',
                      self.gf('django.db.models.fields.CharField')(default=u'\u041e\u0442\u0437\u044b\u0432\u044b \u043e \u043d\u0430\u0441', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.header3_de'
        db.add_column(u'about_aboutpage', 'header3_de',
                      self.gf('django.db.models.fields.CharField')(default=u'\u041e\u0442\u0437\u044b\u0432\u044b \u043e \u043d\u0430\u0441', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.header3_es'
        db.add_column(u'about_aboutpage', 'header3_es',
                      self.gf('django.db.models.fields.CharField')(default=u'\u041e\u0442\u0437\u044b\u0432\u044b \u043e \u043d\u0430\u0441', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'AboutPage.header3_cn'
        db.add_column(u'about_aboutpage', 'header3_cn',
                      self.gf('django.db.models.fields.CharField')(default=u'\u041e\u0442\u0437\u044b\u0432\u044b \u043e \u043d\u0430\u0441', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Certificate.is_published_fr'
        db.add_column(u'about_certificate', 'is_published_fr',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Certificate.is_published_de'
        db.add_column(u'about_certificate', 'is_published_de',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Certificate.is_published_es'
        db.add_column(u'about_certificate', 'is_published_es',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Certificate.is_published_cn'
        db.add_column(u'about_certificate', 'is_published_cn',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Certificate.thumb_img_fr'
        db.add_column(u'about_certificate', 'thumb_img_fr',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Certificate.thumb_img_de'
        db.add_column(u'about_certificate', 'thumb_img_de',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Certificate.thumb_img_es'
        db.add_column(u'about_certificate', 'thumb_img_es',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Certificate.thumb_img_cn'
        db.add_column(u'about_certificate', 'thumb_img_cn',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Certificate.img_fr'
        db.add_column(u'about_certificate', 'img_fr',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Certificate.img_de'
        db.add_column(u'about_certificate', 'img_de',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Certificate.img_es'
        db.add_column(u'about_certificate', 'img_es',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Certificate.img_cn'
        db.add_column(u'about_certificate', 'img_cn',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Slide.is_published_fr'
        db.add_column(u'about_slide', 'is_published_fr',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Slide.is_published_de'
        db.add_column(u'about_slide', 'is_published_de',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Slide.is_published_es'
        db.add_column(u'about_slide', 'is_published_es',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Slide.is_published_cn'
        db.add_column(u'about_slide', 'is_published_cn',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Slide.title_fr'
        db.add_column(u'about_slide', 'title_fr',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Slide.title_de'
        db.add_column(u'about_slide', 'title_de',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Slide.title_es'
        db.add_column(u'about_slide', 'title_es',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Slide.title_cn'
        db.add_column(u'about_slide', 'title_cn',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Slide.teaser_fr'
        db.add_column(u'about_slide', 'teaser_fr',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Slide.teaser_de'
        db.add_column(u'about_slide', 'teaser_de',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Slide.teaser_es'
        db.add_column(u'about_slide', 'teaser_es',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Slide.teaser_cn'
        db.add_column(u'about_slide', 'teaser_cn',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Slide.img_fr'
        db.add_column(u'about_slide', 'img_fr',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Slide.img_de'
        db.add_column(u'about_slide', 'img_de',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Slide.img_es'
        db.add_column(u'about_slide', 'img_es',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Slide.img_cn'
        db.add_column(u'about_slide', 'img_cn',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Slide.link_fr'
        db.add_column(u'about_slide', 'link_fr',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Slide.link_de'
        db.add_column(u'about_slide', 'link_de',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Slide.link_es'
        db.add_column(u'about_slide', 'link_es',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Slide.link_cn'
        db.add_column(u'about_slide', 'link_cn',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Reviews.is_published_fr'
        db.delete_column(u'about_reviews', 'is_published_fr')

        # Deleting field 'Reviews.is_published_de'
        db.delete_column(u'about_reviews', 'is_published_de')

        # Deleting field 'Reviews.is_published_es'
        db.delete_column(u'about_reviews', 'is_published_es')

        # Deleting field 'Reviews.is_published_cn'
        db.delete_column(u'about_reviews', 'is_published_cn')

        # Deleting field 'Reviews.name_fr'
        db.delete_column(u'about_reviews', 'name_fr')

        # Deleting field 'Reviews.name_de'
        db.delete_column(u'about_reviews', 'name_de')

        # Deleting field 'Reviews.name_es'
        db.delete_column(u'about_reviews', 'name_es')

        # Deleting field 'Reviews.name_cn'
        db.delete_column(u'about_reviews', 'name_cn')

        # Deleting field 'Reviews.message_fr'
        db.delete_column(u'about_reviews', 'message_fr')

        # Deleting field 'Reviews.message_de'
        db.delete_column(u'about_reviews', 'message_de')

        # Deleting field 'Reviews.message_es'
        db.delete_column(u'about_reviews', 'message_es')

        # Deleting field 'Reviews.message_cn'
        db.delete_column(u'about_reviews', 'message_cn')

        # Deleting field 'Reviews.img_fr'
        db.delete_column(u'about_reviews', 'img_fr')

        # Deleting field 'Reviews.img_de'
        db.delete_column(u'about_reviews', 'img_de')

        # Deleting field 'Reviews.img_es'
        db.delete_column(u'about_reviews', 'img_es')

        # Deleting field 'Reviews.img_cn'
        db.delete_column(u'about_reviews', 'img_cn')

        # Deleting field 'AboutPage.text_fr'
        db.delete_column(u'about_aboutpage', 'text_fr')

        # Deleting field 'AboutPage.text_de'
        db.delete_column(u'about_aboutpage', 'text_de')

        # Deleting field 'AboutPage.text_es'
        db.delete_column(u'about_aboutpage', 'text_es')

        # Deleting field 'AboutPage.text_cn'
        db.delete_column(u'about_aboutpage', 'text_cn')

        # Deleting field 'AboutPage.header1_fr'
        db.delete_column(u'about_aboutpage', 'header1_fr')

        # Deleting field 'AboutPage.header1_de'
        db.delete_column(u'about_aboutpage', 'header1_de')

        # Deleting field 'AboutPage.header1_es'
        db.delete_column(u'about_aboutpage', 'header1_es')

        # Deleting field 'AboutPage.header1_cn'
        db.delete_column(u'about_aboutpage', 'header1_cn')

        # Deleting field 'AboutPage.princip1_img_fr'
        db.delete_column(u'about_aboutpage', 'princip1_img_fr')

        # Deleting field 'AboutPage.princip1_img_de'
        db.delete_column(u'about_aboutpage', 'princip1_img_de')

        # Deleting field 'AboutPage.princip1_img_es'
        db.delete_column(u'about_aboutpage', 'princip1_img_es')

        # Deleting field 'AboutPage.princip1_img_cn'
        db.delete_column(u'about_aboutpage', 'princip1_img_cn')

        # Deleting field 'AboutPage.princip1_title_fr'
        db.delete_column(u'about_aboutpage', 'princip1_title_fr')

        # Deleting field 'AboutPage.princip1_title_de'
        db.delete_column(u'about_aboutpage', 'princip1_title_de')

        # Deleting field 'AboutPage.princip1_title_es'
        db.delete_column(u'about_aboutpage', 'princip1_title_es')

        # Deleting field 'AboutPage.princip1_title_cn'
        db.delete_column(u'about_aboutpage', 'princip1_title_cn')

        # Deleting field 'AboutPage.princip1_description_fr'
        db.delete_column(u'about_aboutpage', 'princip1_description_fr')

        # Deleting field 'AboutPage.princip1_description_de'
        db.delete_column(u'about_aboutpage', 'princip1_description_de')

        # Deleting field 'AboutPage.princip1_description_es'
        db.delete_column(u'about_aboutpage', 'princip1_description_es')

        # Deleting field 'AboutPage.princip1_description_cn'
        db.delete_column(u'about_aboutpage', 'princip1_description_cn')

        # Deleting field 'AboutPage.princip2_img_fr'
        db.delete_column(u'about_aboutpage', 'princip2_img_fr')

        # Deleting field 'AboutPage.princip2_img_de'
        db.delete_column(u'about_aboutpage', 'princip2_img_de')

        # Deleting field 'AboutPage.princip2_img_es'
        db.delete_column(u'about_aboutpage', 'princip2_img_es')

        # Deleting field 'AboutPage.princip2_img_cn'
        db.delete_column(u'about_aboutpage', 'princip2_img_cn')

        # Deleting field 'AboutPage.princip2_title_fr'
        db.delete_column(u'about_aboutpage', 'princip2_title_fr')

        # Deleting field 'AboutPage.princip2_title_de'
        db.delete_column(u'about_aboutpage', 'princip2_title_de')

        # Deleting field 'AboutPage.princip2_title_es'
        db.delete_column(u'about_aboutpage', 'princip2_title_es')

        # Deleting field 'AboutPage.princip2_title_cn'
        db.delete_column(u'about_aboutpage', 'princip2_title_cn')

        # Deleting field 'AboutPage.princip2_description_fr'
        db.delete_column(u'about_aboutpage', 'princip2_description_fr')

        # Deleting field 'AboutPage.princip2_description_de'
        db.delete_column(u'about_aboutpage', 'princip2_description_de')

        # Deleting field 'AboutPage.princip2_description_es'
        db.delete_column(u'about_aboutpage', 'princip2_description_es')

        # Deleting field 'AboutPage.princip2_description_cn'
        db.delete_column(u'about_aboutpage', 'princip2_description_cn')

        # Deleting field 'AboutPage.princip3_img_fr'
        db.delete_column(u'about_aboutpage', 'princip3_img_fr')

        # Deleting field 'AboutPage.princip3_img_de'
        db.delete_column(u'about_aboutpage', 'princip3_img_de')

        # Deleting field 'AboutPage.princip3_img_es'
        db.delete_column(u'about_aboutpage', 'princip3_img_es')

        # Deleting field 'AboutPage.princip3_img_cn'
        db.delete_column(u'about_aboutpage', 'princip3_img_cn')

        # Deleting field 'AboutPage.princip3_title_fr'
        db.delete_column(u'about_aboutpage', 'princip3_title_fr')

        # Deleting field 'AboutPage.princip3_title_de'
        db.delete_column(u'about_aboutpage', 'princip3_title_de')

        # Deleting field 'AboutPage.princip3_title_es'
        db.delete_column(u'about_aboutpage', 'princip3_title_es')

        # Deleting field 'AboutPage.princip3_title_cn'
        db.delete_column(u'about_aboutpage', 'princip3_title_cn')

        # Deleting field 'AboutPage.princip3_description_fr'
        db.delete_column(u'about_aboutpage', 'princip3_description_fr')

        # Deleting field 'AboutPage.princip3_description_de'
        db.delete_column(u'about_aboutpage', 'princip3_description_de')

        # Deleting field 'AboutPage.princip3_description_es'
        db.delete_column(u'about_aboutpage', 'princip3_description_es')

        # Deleting field 'AboutPage.princip3_description_cn'
        db.delete_column(u'about_aboutpage', 'princip3_description_cn')

        # Deleting field 'AboutPage.header2_fr'
        db.delete_column(u'about_aboutpage', 'header2_fr')

        # Deleting field 'AboutPage.header2_de'
        db.delete_column(u'about_aboutpage', 'header2_de')

        # Deleting field 'AboutPage.header2_es'
        db.delete_column(u'about_aboutpage', 'header2_es')

        # Deleting field 'AboutPage.header2_cn'
        db.delete_column(u'about_aboutpage', 'header2_cn')

        # Deleting field 'AboutPage.header3_fr'
        db.delete_column(u'about_aboutpage', 'header3_fr')

        # Deleting field 'AboutPage.header3_de'
        db.delete_column(u'about_aboutpage', 'header3_de')

        # Deleting field 'AboutPage.header3_es'
        db.delete_column(u'about_aboutpage', 'header3_es')

        # Deleting field 'AboutPage.header3_cn'
        db.delete_column(u'about_aboutpage', 'header3_cn')

        # Deleting field 'Certificate.is_published_fr'
        db.delete_column(u'about_certificate', 'is_published_fr')

        # Deleting field 'Certificate.is_published_de'
        db.delete_column(u'about_certificate', 'is_published_de')

        # Deleting field 'Certificate.is_published_es'
        db.delete_column(u'about_certificate', 'is_published_es')

        # Deleting field 'Certificate.is_published_cn'
        db.delete_column(u'about_certificate', 'is_published_cn')

        # Deleting field 'Certificate.thumb_img_fr'
        db.delete_column(u'about_certificate', 'thumb_img_fr')

        # Deleting field 'Certificate.thumb_img_de'
        db.delete_column(u'about_certificate', 'thumb_img_de')

        # Deleting field 'Certificate.thumb_img_es'
        db.delete_column(u'about_certificate', 'thumb_img_es')

        # Deleting field 'Certificate.thumb_img_cn'
        db.delete_column(u'about_certificate', 'thumb_img_cn')

        # Deleting field 'Certificate.img_fr'
        db.delete_column(u'about_certificate', 'img_fr')

        # Deleting field 'Certificate.img_de'
        db.delete_column(u'about_certificate', 'img_de')

        # Deleting field 'Certificate.img_es'
        db.delete_column(u'about_certificate', 'img_es')

        # Deleting field 'Certificate.img_cn'
        db.delete_column(u'about_certificate', 'img_cn')

        # Deleting field 'Slide.is_published_fr'
        db.delete_column(u'about_slide', 'is_published_fr')

        # Deleting field 'Slide.is_published_de'
        db.delete_column(u'about_slide', 'is_published_de')

        # Deleting field 'Slide.is_published_es'
        db.delete_column(u'about_slide', 'is_published_es')

        # Deleting field 'Slide.is_published_cn'
        db.delete_column(u'about_slide', 'is_published_cn')

        # Deleting field 'Slide.title_fr'
        db.delete_column(u'about_slide', 'title_fr')

        # Deleting field 'Slide.title_de'
        db.delete_column(u'about_slide', 'title_de')

        # Deleting field 'Slide.title_es'
        db.delete_column(u'about_slide', 'title_es')

        # Deleting field 'Slide.title_cn'
        db.delete_column(u'about_slide', 'title_cn')

        # Deleting field 'Slide.teaser_fr'
        db.delete_column(u'about_slide', 'teaser_fr')

        # Deleting field 'Slide.teaser_de'
        db.delete_column(u'about_slide', 'teaser_de')

        # Deleting field 'Slide.teaser_es'
        db.delete_column(u'about_slide', 'teaser_es')

        # Deleting field 'Slide.teaser_cn'
        db.delete_column(u'about_slide', 'teaser_cn')

        # Deleting field 'Slide.img_fr'
        db.delete_column(u'about_slide', 'img_fr')

        # Deleting field 'Slide.img_de'
        db.delete_column(u'about_slide', 'img_de')

        # Deleting field 'Slide.img_es'
        db.delete_column(u'about_slide', 'img_es')

        # Deleting field 'Slide.img_cn'
        db.delete_column(u'about_slide', 'img_cn')

        # Deleting field 'Slide.link_fr'
        db.delete_column(u'about_slide', 'link_fr')

        # Deleting field 'Slide.link_de'
        db.delete_column(u'about_slide', 'link_de')

        # Deleting field 'Slide.link_es'
        db.delete_column(u'about_slide', 'link_es')

        # Deleting field 'Slide.link_cn'
        db.delete_column(u'about_slide', 'link_cn')


    models = {
        u'about.aboutpage': {
            'Meta': {'object_name': 'AboutPage'},
            'header1': ('django.db.models.fields.CharField', [], {'default': "u'\\u041d\\u0430\\u0448\\u0438 \\u043f\\u0440\\u0438\\u043d\\u0446\\u0438\\u043f\\u044b \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b'", 'max_length': '255', 'blank': 'True'}),
            'header1_cn': ('django.db.models.fields.CharField', [], {'default': "u'\\u041d\\u0430\\u0448\\u0438 \\u043f\\u0440\\u0438\\u043d\\u0446\\u0438\\u043f\\u044b \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'header1_de': ('django.db.models.fields.CharField', [], {'default': "u'\\u041d\\u0430\\u0448\\u0438 \\u043f\\u0440\\u0438\\u043d\\u0446\\u0438\\u043f\\u044b \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'header1_en': ('django.db.models.fields.CharField', [], {'default': "u'\\u041d\\u0430\\u0448\\u0438 \\u043f\\u0440\\u0438\\u043d\\u0446\\u0438\\u043f\\u044b \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'header1_es': ('django.db.models.fields.CharField', [], {'default': "u'\\u041d\\u0430\\u0448\\u0438 \\u043f\\u0440\\u0438\\u043d\\u0446\\u0438\\u043f\\u044b \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'header1_fr': ('django.db.models.fields.CharField', [], {'default': "u'\\u041d\\u0430\\u0448\\u0438 \\u043f\\u0440\\u0438\\u043d\\u0446\\u0438\\u043f\\u044b \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'header1_ru': ('django.db.models.fields.CharField', [], {'default': "u'\\u041d\\u0430\\u0448\\u0438 \\u043f\\u0440\\u0438\\u043d\\u0446\\u0438\\u043f\\u044b \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'header2': ('django.db.models.fields.CharField', [], {'default': "u'\\u041d\\u0430\\u0448\\u0438 \\u043d\\u0430\\u0433\\u0440\\u0430\\u0434\\u044b \\u0438 \\u0441\\u0435\\u0440\\u0442\\u0438\\u0444\\u0438\\u043a\\u0430\\u0442\\u044b'", 'max_length': '255', 'blank': 'True'}),
            'header2_cn': ('django.db.models.fields.CharField', [], {'default': "u'\\u041d\\u0430\\u0448\\u0438 \\u043d\\u0430\\u0433\\u0440\\u0430\\u0434\\u044b \\u0438 \\u0441\\u0435\\u0440\\u0442\\u0438\\u0444\\u0438\\u043a\\u0430\\u0442\\u044b'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'header2_de': ('django.db.models.fields.CharField', [], {'default': "u'\\u041d\\u0430\\u0448\\u0438 \\u043d\\u0430\\u0433\\u0440\\u0430\\u0434\\u044b \\u0438 \\u0441\\u0435\\u0440\\u0442\\u0438\\u0444\\u0438\\u043a\\u0430\\u0442\\u044b'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'header2_en': ('django.db.models.fields.CharField', [], {'default': "u'\\u041d\\u0430\\u0448\\u0438 \\u043d\\u0430\\u0433\\u0440\\u0430\\u0434\\u044b \\u0438 \\u0441\\u0435\\u0440\\u0442\\u0438\\u0444\\u0438\\u043a\\u0430\\u0442\\u044b'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'header2_es': ('django.db.models.fields.CharField', [], {'default': "u'\\u041d\\u0430\\u0448\\u0438 \\u043d\\u0430\\u0433\\u0440\\u0430\\u0434\\u044b \\u0438 \\u0441\\u0435\\u0440\\u0442\\u0438\\u0444\\u0438\\u043a\\u0430\\u0442\\u044b'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'header2_fr': ('django.db.models.fields.CharField', [], {'default': "u'\\u041d\\u0430\\u0448\\u0438 \\u043d\\u0430\\u0433\\u0440\\u0430\\u0434\\u044b \\u0438 \\u0441\\u0435\\u0440\\u0442\\u0438\\u0444\\u0438\\u043a\\u0430\\u0442\\u044b'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'header2_ru': ('django.db.models.fields.CharField', [], {'default': "u'\\u041d\\u0430\\u0448\\u0438 \\u043d\\u0430\\u0433\\u0440\\u0430\\u0434\\u044b \\u0438 \\u0441\\u0435\\u0440\\u0442\\u0438\\u0444\\u0438\\u043a\\u0430\\u0442\\u044b'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'header3': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0442\\u0437\\u044b\\u0432\\u044b \\u043e \\u043d\\u0430\\u0441'", 'max_length': '255', 'blank': 'True'}),
            'header3_cn': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0442\\u0437\\u044b\\u0432\\u044b \\u043e \\u043d\\u0430\\u0441'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'header3_de': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0442\\u0437\\u044b\\u0432\\u044b \\u043e \\u043d\\u0430\\u0441'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'header3_en': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0442\\u0437\\u044b\\u0432\\u044b \\u043e \\u043d\\u0430\\u0441'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'header3_es': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0442\\u0437\\u044b\\u0432\\u044b \\u043e \\u043d\\u0430\\u0441'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'header3_fr': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0442\\u0437\\u044b\\u0432\\u044b \\u043e \\u043d\\u0430\\u0441'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'header3_ru': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0442\\u0437\\u044b\\u0432\\u044b \\u043e \\u043d\\u0430\\u0441'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'princip1_description': ('django.db.models.fields.TextField', [], {'default': "u'\\u0412\\u043d\\u0438\\u043c\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u043e\\u0435 \\u043e\\u0442\\u043d\\u043e\\u0448\\u0435\\u043d\\u0438\\u0435 \\u0438 \\u043f\\u043e\\u043d\\u0438\\u043c\\u0430\\u043d\\u0438\\u0435 \\u043f\\u043e\\u0442\\u0440\\u0435\\u0431\\u043d\\u043e\\u0441\\u0442\\u0435\\u0439 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430'", 'blank': 'True'}),
            'princip1_description_cn': ('django.db.models.fields.TextField', [], {'default': "u'\\u0412\\u043d\\u0438\\u043c\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u043e\\u0435 \\u043e\\u0442\\u043d\\u043e\\u0448\\u0435\\u043d\\u0438\\u0435 \\u0438 \\u043f\\u043e\\u043d\\u0438\\u043c\\u0430\\u043d\\u0438\\u0435 \\u043f\\u043e\\u0442\\u0440\\u0435\\u0431\\u043d\\u043e\\u0441\\u0442\\u0435\\u0439 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430'", 'null': 'True', 'blank': 'True'}),
            'princip1_description_de': ('django.db.models.fields.TextField', [], {'default': "u'\\u0412\\u043d\\u0438\\u043c\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u043e\\u0435 \\u043e\\u0442\\u043d\\u043e\\u0448\\u0435\\u043d\\u0438\\u0435 \\u0438 \\u043f\\u043e\\u043d\\u0438\\u043c\\u0430\\u043d\\u0438\\u0435 \\u043f\\u043e\\u0442\\u0440\\u0435\\u0431\\u043d\\u043e\\u0441\\u0442\\u0435\\u0439 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430'", 'null': 'True', 'blank': 'True'}),
            'princip1_description_en': ('django.db.models.fields.TextField', [], {'default': "u'\\u0412\\u043d\\u0438\\u043c\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u043e\\u0435 \\u043e\\u0442\\u043d\\u043e\\u0448\\u0435\\u043d\\u0438\\u0435 \\u0438 \\u043f\\u043e\\u043d\\u0438\\u043c\\u0430\\u043d\\u0438\\u0435 \\u043f\\u043e\\u0442\\u0440\\u0435\\u0431\\u043d\\u043e\\u0441\\u0442\\u0435\\u0439 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430'", 'null': 'True', 'blank': 'True'}),
            'princip1_description_es': ('django.db.models.fields.TextField', [], {'default': "u'\\u0412\\u043d\\u0438\\u043c\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u043e\\u0435 \\u043e\\u0442\\u043d\\u043e\\u0448\\u0435\\u043d\\u0438\\u0435 \\u0438 \\u043f\\u043e\\u043d\\u0438\\u043c\\u0430\\u043d\\u0438\\u0435 \\u043f\\u043e\\u0442\\u0440\\u0435\\u0431\\u043d\\u043e\\u0441\\u0442\\u0435\\u0439 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430'", 'null': 'True', 'blank': 'True'}),
            'princip1_description_fr': ('django.db.models.fields.TextField', [], {'default': "u'\\u0412\\u043d\\u0438\\u043c\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u043e\\u0435 \\u043e\\u0442\\u043d\\u043e\\u0448\\u0435\\u043d\\u0438\\u0435 \\u0438 \\u043f\\u043e\\u043d\\u0438\\u043c\\u0430\\u043d\\u0438\\u0435 \\u043f\\u043e\\u0442\\u0440\\u0435\\u0431\\u043d\\u043e\\u0441\\u0442\\u0435\\u0439 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430'", 'null': 'True', 'blank': 'True'}),
            'princip1_description_ru': ('django.db.models.fields.TextField', [], {'default': "u'\\u0412\\u043d\\u0438\\u043c\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u043e\\u0435 \\u043e\\u0442\\u043d\\u043e\\u0448\\u0435\\u043d\\u0438\\u0435 \\u0438 \\u043f\\u043e\\u043d\\u0438\\u043c\\u0430\\u043d\\u0438\\u0435 \\u043f\\u043e\\u0442\\u0440\\u0435\\u0431\\u043d\\u043e\\u0441\\u0442\\u0435\\u0439 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430'", 'null': 'True', 'blank': 'True'}),
            'princip1_img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'princip1_img_cn': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'princip1_img_de': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'princip1_img_en': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'princip1_img_es': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'princip1_img_fr': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'princip1_img_ru': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'princip1_title': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0440\\u0438\\u0435\\u043d\\u0442\\u0430\\u0446\\u0438\\u044f \\u043d\\u0430 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430'", 'max_length': '255', 'blank': 'True'}),
            'princip1_title_cn': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0440\\u0438\\u0435\\u043d\\u0442\\u0430\\u0446\\u0438\\u044f \\u043d\\u0430 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'princip1_title_de': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0440\\u0438\\u0435\\u043d\\u0442\\u0430\\u0446\\u0438\\u044f \\u043d\\u0430 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'princip1_title_en': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0440\\u0438\\u0435\\u043d\\u0442\\u0430\\u0446\\u0438\\u044f \\u043d\\u0430 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'princip1_title_es': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0440\\u0438\\u0435\\u043d\\u0442\\u0430\\u0446\\u0438\\u044f \\u043d\\u0430 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'princip1_title_fr': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0440\\u0438\\u0435\\u043d\\u0442\\u0430\\u0446\\u0438\\u044f \\u043d\\u0430 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'princip1_title_ru': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0440\\u0438\\u0435\\u043d\\u0442\\u0430\\u0446\\u0438\\u044f \\u043d\\u0430 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'princip2_description': ('django.db.models.fields.TextField', [], {'default': "u'\\u041d\\u0435\\u0437\\u0430\\u0432\\u0438\\u0441\\u0438\\u043c\\u043e \\u043e\\u0442 \\u0431\\u044e\\u0434\\u0436\\u0435\\u0442\\u0430 \\u0442\\u0443\\u0440\\u0430, \\u043c\\u044b \\u043f\\u0440\\u043e\\u0434\\u0443\\u043c\\u044b\\u0432\\u0430\\u0435\\u043c \\u043a\\u0430\\u0436\\u0434\\u0443\\u044e \\u043c\\u0438\\u043d\\u0443\\u0442\\u0443 \\u043f\\u0443\\u0442\\u0435\\u0448\\u0435\\u0441\\u0442\\u0432\\u0438\\u044f \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430'", 'blank': 'True'}),
            'princip2_description_cn': ('django.db.models.fields.TextField', [], {'default': "u'\\u041d\\u0435\\u0437\\u0430\\u0432\\u0438\\u0441\\u0438\\u043c\\u043e \\u043e\\u0442 \\u0431\\u044e\\u0434\\u0436\\u0435\\u0442\\u0430 \\u0442\\u0443\\u0440\\u0430, \\u043c\\u044b \\u043f\\u0440\\u043e\\u0434\\u0443\\u043c\\u044b\\u0432\\u0430\\u0435\\u043c \\u043a\\u0430\\u0436\\u0434\\u0443\\u044e \\u043c\\u0438\\u043d\\u0443\\u0442\\u0443 \\u043f\\u0443\\u0442\\u0435\\u0448\\u0435\\u0441\\u0442\\u0432\\u0438\\u044f \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430'", 'null': 'True', 'blank': 'True'}),
            'princip2_description_de': ('django.db.models.fields.TextField', [], {'default': "u'\\u041d\\u0435\\u0437\\u0430\\u0432\\u0438\\u0441\\u0438\\u043c\\u043e \\u043e\\u0442 \\u0431\\u044e\\u0434\\u0436\\u0435\\u0442\\u0430 \\u0442\\u0443\\u0440\\u0430, \\u043c\\u044b \\u043f\\u0440\\u043e\\u0434\\u0443\\u043c\\u044b\\u0432\\u0430\\u0435\\u043c \\u043a\\u0430\\u0436\\u0434\\u0443\\u044e \\u043c\\u0438\\u043d\\u0443\\u0442\\u0443 \\u043f\\u0443\\u0442\\u0435\\u0448\\u0435\\u0441\\u0442\\u0432\\u0438\\u044f \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430'", 'null': 'True', 'blank': 'True'}),
            'princip2_description_en': ('django.db.models.fields.TextField', [], {'default': "u'\\u041d\\u0435\\u0437\\u0430\\u0432\\u0438\\u0441\\u0438\\u043c\\u043e \\u043e\\u0442 \\u0431\\u044e\\u0434\\u0436\\u0435\\u0442\\u0430 \\u0442\\u0443\\u0440\\u0430, \\u043c\\u044b \\u043f\\u0440\\u043e\\u0434\\u0443\\u043c\\u044b\\u0432\\u0430\\u0435\\u043c \\u043a\\u0430\\u0436\\u0434\\u0443\\u044e \\u043c\\u0438\\u043d\\u0443\\u0442\\u0443 \\u043f\\u0443\\u0442\\u0435\\u0448\\u0435\\u0441\\u0442\\u0432\\u0438\\u044f \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430'", 'null': 'True', 'blank': 'True'}),
            'princip2_description_es': ('django.db.models.fields.TextField', [], {'default': "u'\\u041d\\u0435\\u0437\\u0430\\u0432\\u0438\\u0441\\u0438\\u043c\\u043e \\u043e\\u0442 \\u0431\\u044e\\u0434\\u0436\\u0435\\u0442\\u0430 \\u0442\\u0443\\u0440\\u0430, \\u043c\\u044b \\u043f\\u0440\\u043e\\u0434\\u0443\\u043c\\u044b\\u0432\\u0430\\u0435\\u043c \\u043a\\u0430\\u0436\\u0434\\u0443\\u044e \\u043c\\u0438\\u043d\\u0443\\u0442\\u0443 \\u043f\\u0443\\u0442\\u0435\\u0448\\u0435\\u0441\\u0442\\u0432\\u0438\\u044f \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430'", 'null': 'True', 'blank': 'True'}),
            'princip2_description_fr': ('django.db.models.fields.TextField', [], {'default': "u'\\u041d\\u0435\\u0437\\u0430\\u0432\\u0438\\u0441\\u0438\\u043c\\u043e \\u043e\\u0442 \\u0431\\u044e\\u0434\\u0436\\u0435\\u0442\\u0430 \\u0442\\u0443\\u0440\\u0430, \\u043c\\u044b \\u043f\\u0440\\u043e\\u0434\\u0443\\u043c\\u044b\\u0432\\u0430\\u0435\\u043c \\u043a\\u0430\\u0436\\u0434\\u0443\\u044e \\u043c\\u0438\\u043d\\u0443\\u0442\\u0443 \\u043f\\u0443\\u0442\\u0435\\u0448\\u0435\\u0441\\u0442\\u0432\\u0438\\u044f \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430'", 'null': 'True', 'blank': 'True'}),
            'princip2_description_ru': ('django.db.models.fields.TextField', [], {'default': "u'\\u041d\\u0435\\u0437\\u0430\\u0432\\u0438\\u0441\\u0438\\u043c\\u043e \\u043e\\u0442 \\u0431\\u044e\\u0434\\u0436\\u0435\\u0442\\u0430 \\u0442\\u0443\\u0440\\u0430, \\u043c\\u044b \\u043f\\u0440\\u043e\\u0434\\u0443\\u043c\\u044b\\u0432\\u0430\\u0435\\u043c \\u043a\\u0430\\u0436\\u0434\\u0443\\u044e \\u043c\\u0438\\u043d\\u0443\\u0442\\u0443 \\u043f\\u0443\\u0442\\u0435\\u0448\\u0435\\u0441\\u0442\\u0432\\u0438\\u044f \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430'", 'null': 'True', 'blank': 'True'}),
            'princip2_img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'princip2_img_cn': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'princip2_img_de': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'princip2_img_en': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'princip2_img_es': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'princip2_img_fr': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'princip2_img_ru': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'princip2_title': ('django.db.models.fields.CharField', [], {'default': "u'\\u0412\\u044b\\u0441\\u043e\\u043a\\u043e\\u0435 \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u043e\\u043a\\u0430\\u0437\\u0430\\u043d\\u0438\\u044f \\u0443\\u0441\\u043b\\u0443\\u0433 \\u0438 \\u043e\\u0440\\u0433\\u0430\\u043d\\u0438\\u0437\\u0430\\u0446\\u0438\\u0438 \\u0442\\u0443\\u0440\\u043e\\u0432'", 'max_length': '255', 'blank': 'True'}),
            'princip2_title_cn': ('django.db.models.fields.CharField', [], {'default': "u'\\u0412\\u044b\\u0441\\u043e\\u043a\\u043e\\u0435 \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u043e\\u043a\\u0430\\u0437\\u0430\\u043d\\u0438\\u044f \\u0443\\u0441\\u043b\\u0443\\u0433 \\u0438 \\u043e\\u0440\\u0433\\u0430\\u043d\\u0438\\u0437\\u0430\\u0446\\u0438\\u0438 \\u0442\\u0443\\u0440\\u043e\\u0432'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'princip2_title_de': ('django.db.models.fields.CharField', [], {'default': "u'\\u0412\\u044b\\u0441\\u043e\\u043a\\u043e\\u0435 \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u043e\\u043a\\u0430\\u0437\\u0430\\u043d\\u0438\\u044f \\u0443\\u0441\\u043b\\u0443\\u0433 \\u0438 \\u043e\\u0440\\u0433\\u0430\\u043d\\u0438\\u0437\\u0430\\u0446\\u0438\\u0438 \\u0442\\u0443\\u0440\\u043e\\u0432'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'princip2_title_en': ('django.db.models.fields.CharField', [], {'default': "u'\\u0412\\u044b\\u0441\\u043e\\u043a\\u043e\\u0435 \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u043e\\u043a\\u0430\\u0437\\u0430\\u043d\\u0438\\u044f \\u0443\\u0441\\u043b\\u0443\\u0433 \\u0438 \\u043e\\u0440\\u0433\\u0430\\u043d\\u0438\\u0437\\u0430\\u0446\\u0438\\u0438 \\u0442\\u0443\\u0440\\u043e\\u0432'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'princip2_title_es': ('django.db.models.fields.CharField', [], {'default': "u'\\u0412\\u044b\\u0441\\u043e\\u043a\\u043e\\u0435 \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u043e\\u043a\\u0430\\u0437\\u0430\\u043d\\u0438\\u044f \\u0443\\u0441\\u043b\\u0443\\u0433 \\u0438 \\u043e\\u0440\\u0433\\u0430\\u043d\\u0438\\u0437\\u0430\\u0446\\u0438\\u0438 \\u0442\\u0443\\u0440\\u043e\\u0432'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'princip2_title_fr': ('django.db.models.fields.CharField', [], {'default': "u'\\u0412\\u044b\\u0441\\u043e\\u043a\\u043e\\u0435 \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u043e\\u043a\\u0430\\u0437\\u0430\\u043d\\u0438\\u044f \\u0443\\u0441\\u043b\\u0443\\u0433 \\u0438 \\u043e\\u0440\\u0433\\u0430\\u043d\\u0438\\u0437\\u0430\\u0446\\u0438\\u0438 \\u0442\\u0443\\u0440\\u043e\\u0432'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'princip2_title_ru': ('django.db.models.fields.CharField', [], {'default': "u'\\u0412\\u044b\\u0441\\u043e\\u043a\\u043e\\u0435 \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u043e\\u043a\\u0430\\u0437\\u0430\\u043d\\u0438\\u044f \\u0443\\u0441\\u043b\\u0443\\u0433 \\u0438 \\u043e\\u0440\\u0433\\u0430\\u043d\\u0438\\u0437\\u0430\\u0446\\u0438\\u0438 \\u0442\\u0443\\u0440\\u043e\\u0432'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'princip3_description': ('django.db.models.fields.TextField', [], {'default': "u'\\u041c\\u044b \\u0446\\u0435\\u043d\\u0438\\u043c \\u0434\\u043e\\u0432\\u0435\\u0440\\u0438\\u0435 \\u043d\\u0430\\u0448\\u0438\\u0445 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u043e\\u0432 \\u0438 \\u043f\\u0430\\u0440\\u0442\\u043d\\u0435\\u0440\\u043e\\u0432'", 'blank': 'True'}),
            'princip3_description_cn': ('django.db.models.fields.TextField', [], {'default': "u'\\u041c\\u044b \\u0446\\u0435\\u043d\\u0438\\u043c \\u0434\\u043e\\u0432\\u0435\\u0440\\u0438\\u0435 \\u043d\\u0430\\u0448\\u0438\\u0445 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u043e\\u0432 \\u0438 \\u043f\\u0430\\u0440\\u0442\\u043d\\u0435\\u0440\\u043e\\u0432'", 'null': 'True', 'blank': 'True'}),
            'princip3_description_de': ('django.db.models.fields.TextField', [], {'default': "u'\\u041c\\u044b \\u0446\\u0435\\u043d\\u0438\\u043c \\u0434\\u043e\\u0432\\u0435\\u0440\\u0438\\u0435 \\u043d\\u0430\\u0448\\u0438\\u0445 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u043e\\u0432 \\u0438 \\u043f\\u0430\\u0440\\u0442\\u043d\\u0435\\u0440\\u043e\\u0432'", 'null': 'True', 'blank': 'True'}),
            'princip3_description_en': ('django.db.models.fields.TextField', [], {'default': "u'\\u041c\\u044b \\u0446\\u0435\\u043d\\u0438\\u043c \\u0434\\u043e\\u0432\\u0435\\u0440\\u0438\\u0435 \\u043d\\u0430\\u0448\\u0438\\u0445 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u043e\\u0432 \\u0438 \\u043f\\u0430\\u0440\\u0442\\u043d\\u0435\\u0440\\u043e\\u0432'", 'null': 'True', 'blank': 'True'}),
            'princip3_description_es': ('django.db.models.fields.TextField', [], {'default': "u'\\u041c\\u044b \\u0446\\u0435\\u043d\\u0438\\u043c \\u0434\\u043e\\u0432\\u0435\\u0440\\u0438\\u0435 \\u043d\\u0430\\u0448\\u0438\\u0445 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u043e\\u0432 \\u0438 \\u043f\\u0430\\u0440\\u0442\\u043d\\u0435\\u0440\\u043e\\u0432'", 'null': 'True', 'blank': 'True'}),
            'princip3_description_fr': ('django.db.models.fields.TextField', [], {'default': "u'\\u041c\\u044b \\u0446\\u0435\\u043d\\u0438\\u043c \\u0434\\u043e\\u0432\\u0435\\u0440\\u0438\\u0435 \\u043d\\u0430\\u0448\\u0438\\u0445 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u043e\\u0432 \\u0438 \\u043f\\u0430\\u0440\\u0442\\u043d\\u0435\\u0440\\u043e\\u0432'", 'null': 'True', 'blank': 'True'}),
            'princip3_description_ru': ('django.db.models.fields.TextField', [], {'default': "u'\\u041c\\u044b \\u0446\\u0435\\u043d\\u0438\\u043c \\u0434\\u043e\\u0432\\u0435\\u0440\\u0438\\u0435 \\u043d\\u0430\\u0448\\u0438\\u0445 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u043e\\u0432 \\u0438 \\u043f\\u0430\\u0440\\u0442\\u043d\\u0435\\u0440\\u043e\\u0432'", 'null': 'True', 'blank': 'True'}),
            'princip3_img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'princip3_img_cn': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'princip3_img_de': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'princip3_img_en': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'princip3_img_es': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'princip3_img_fr': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'princip3_img_ru': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'princip3_title': ('django.db.models.fields.CharField', [], {'default': "u'\\u0420\\u0435\\u043f\\u0443\\u0442\\u0430\\u0446\\u0438\\u044f \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438'", 'max_length': '255', 'blank': 'True'}),
            'princip3_title_cn': ('django.db.models.fields.CharField', [], {'default': "u'\\u0420\\u0435\\u043f\\u0443\\u0442\\u0430\\u0446\\u0438\\u044f \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'princip3_title_de': ('django.db.models.fields.CharField', [], {'default': "u'\\u0420\\u0435\\u043f\\u0443\\u0442\\u0430\\u0446\\u0438\\u044f \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'princip3_title_en': ('django.db.models.fields.CharField', [], {'default': "u'\\u0420\\u0435\\u043f\\u0443\\u0442\\u0430\\u0446\\u0438\\u044f \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'princip3_title_es': ('django.db.models.fields.CharField', [], {'default': "u'\\u0420\\u0435\\u043f\\u0443\\u0442\\u0430\\u0446\\u0438\\u044f \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'princip3_title_fr': ('django.db.models.fields.CharField', [], {'default': "u'\\u0420\\u0435\\u043f\\u0443\\u0442\\u0430\\u0446\\u0438\\u044f \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'princip3_title_ru': ('django.db.models.fields.CharField', [], {'default': "u'\\u0420\\u0435\\u043f\\u0443\\u0442\\u0430\\u0446\\u0438\\u044f \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'text': ('ckeditor.fields.RichTextField', [], {'blank': 'True'}),
            'text_cn': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text_de': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text_es': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text_ru': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'})
        },
        u'about.certificate': {
            'Meta': {'ordering': "['weight']", 'object_name': 'Certificate'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'img_cn': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_de': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_en': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_es': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_fr': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_ru': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_cn': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_de': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_en': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_es': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_fr': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_ru': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['about.AboutPage']"}),
            'thumb_img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'thumb_img_cn': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'thumb_img_de': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'thumb_img_en': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'thumb_img_es': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'thumb_img_fr': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'thumb_img_ru': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.SmallIntegerField', [], {'default': '10000'})
        },
        u'about.reviews': {
            'Meta': {'ordering': "['weight']", 'object_name': 'Reviews'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'img_cn': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_de': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_en': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_es': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_fr': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_ru': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_cn': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_de': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_en': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_es': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_fr': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_ru': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'message_cn': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'message_de': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'message_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'message_es': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'message_fr': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'message_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'name_cn': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_de': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_es': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_fr': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['about.AboutPage']"}),
            'weight': ('django.db.models.fields.SmallIntegerField', [], {'default': '10000'})
        },
        u'about.slide': {
            'Meta': {'ordering': "['weight']", 'object_name': 'Slide'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'img_cn': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_de': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_en': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_es': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_fr': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_ru': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_cn': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_de': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_en': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_es': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_fr': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_ru': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'link_cn': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'link_de': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'link_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'link_es': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'link_fr': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'link_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['about.AboutPage']"}),
            'teaser': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'teaser_cn': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'teaser_de': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'teaser_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'teaser_es': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'teaser_fr': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'teaser_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_cn': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_de': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_es': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_fr': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.SmallIntegerField', [], {'default': '10000'})
        }
    }

    complete_apps = ['about']