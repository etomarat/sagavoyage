# coding: utf-8

from django.db import models
from solo.models import SingletonModel
from django.utils.translation import ugettext_lazy as _
from ckeditor.fields import RichTextField
from kernel.db import PublishModel, TitleMixin, SortableMixin
from django.core.validators import MaxLengthValidator


class AboutPage(SingletonModel):
    text = RichTextField(verbose_name=_(u'Текст'), blank=True)
    
    header1 = models.CharField(
        verbose_name=_(u'Заголовок 1'),
        help_text=_(u'Наши принципы работы'),
        default=u'Наши принципы работы',
        max_length=0xff,
        blank=True,
        )
    
    princip1_img = models.ImageField(
        verbose_name=_(u'Изображение принцип-1'),
        help_text=u'100x100px',
        upload_to='about',
        blank=True,
        )
    princip1_title = models.CharField(
        verbose_name=_(u'Заголовок принцип-1'),
        help_text=_(u'Ориентация на клиента'),
        default=u'Ориентация на клиента',
        max_length=0xff,
        blank=True
        )
    princip1_description = models.TextField(
        verbose_name=_(u'Описание принцип-1'),
        help_text=_(u'Внимательное отношение и понимание потребностей клиента'),
        default=u'Внимательное отношение и понимание потребностей клиента',
        blank=True,
        )
    
    princip2_img = models.ImageField(
        verbose_name=_(u'Изображение принцип-2'),
        help_text=u'100x100px',
        upload_to='about',
        blank=True,
        )
    princip2_title = models.CharField(
        verbose_name=_(u'Заголовок принцип-2'),
        help_text=_(u'Высокое качество оказания услуг и организации туров'),
        default=u'Высокое качество оказания услуг и организации туров',
        max_length=0xff,
        blank=True
        )
    princip2_description = models.TextField(
        verbose_name=_(u'Описание принцип-2'),
        help_text=_(u'Независимо от бюджета тура, мы продумываем каждую минуту путешествия клиента'),
        default=u'Независимо от бюджета тура, мы продумываем каждую минуту путешествия клиента',
        blank=True,
        )
    
    princip3_img = models.ImageField(
        verbose_name=_(u'Изображение принцип-3'),
        help_text=u'100x100px',
        upload_to='about',
        blank=True,
        )
    princip3_title = models.CharField(
        verbose_name=_(u'Заголовок принцип-3'),
        help_text=_(u'Репутация компании'),
        default=u'Репутация компании',
        max_length=0xff,
        blank=True
        )
    princip3_description = models.TextField(
        verbose_name=_(u'Описание принцип-3'),
        help_text=_(u'Мы ценим доверие наших клиентов и партнеров'),
        default=u'Мы ценим доверие наших клиентов и партнеров',
        blank=True,
        )
    
    header2 = models.CharField(
        verbose_name=_(u'Заголовок 2'),
        help_text=_(u'Наши награды и сертификаты'),
        default=u'Наши награды и сертификаты',
        max_length=0xff,
        blank=True,
        )
    
    header3 = models.CharField(
        verbose_name=_(u'Заголовок 3'),
        help_text=_(u'Отзывы о нас'),
        default=u'Отзывы о нас',
        max_length=0xff,
        blank=True,
        )
    
    def __unicode__(self):
        return u'Страница "О Нас"'

    class Meta:
        verbose_name=_(u'Страница "О Нас"')
    
    class TMeta:
        fields = [
          'text',
          'header1',
          'header2',
          'header3',
          'princip1_img',
          'princip2_img',
          'princip3_img',
          'princip1_title',
          'princip2_title',
          'princip3_title',
          'princip1_description',
          'princip2_description',
          'princip3_description',
          ]


class Slide(PublishModel, TitleMixin, SortableMixin):
  teaser = models.TextField(
        verbose_name=_(u'Текст'),
        help_text=_(u'Рекомендуется не вводить более 250 символов'),
        blank=True,
        validators=[MaxLengthValidator(300)],
        )
  img = models.ImageField(
        verbose_name=_(u'Изображение (1280x600px)'),
        help_text=u'1280x600px',
        upload_to='about',
        blank=True,
        )
  link = models.CharField(verbose_name=_(u'Ссылка'), help_text=_(u'Кнопка "подробнее"'), max_length=0xff, blank=True)
  parent = models.ForeignKey(AboutPage, verbose_name=_(u'О нас'),)
  
  class TMeta:
      fields = [
        'is_published',
        'title',
        'teaser',
        'img',
        'link',
        ]
  
  class Meta:
        verbose_name = _(u'Слайд')
        verbose_name_plural = _(u'Слайды')
        ordering = ['weight']


class Reviews(PublishModel, SortableMixin):
  name = models.CharField(verbose_name=_(u'Имя'), max_length=0xff, blank=True)
  message = models.TextField(
        verbose_name=_(u'Текст'),
        blank=True,
        )
  img = models.ImageField(
        verbose_name=_(u'Аватар (100x100px)'),
        help_text=u'100x100px',
        upload_to='about',
        blank=True,
        )
  parent = models.ForeignKey(AboutPage, verbose_name=_(u'О нас'),)
  
  class TMeta:
      fields = [
        'is_published',
        'name',
        'message',
        'img',
        ]
      
  class Meta:
        verbose_name = _(u'Отзыв')
        verbose_name_plural = _(u'Отзывы')
        ordering = ['weight']
  
class Certificate(PublishModel, SortableMixin):
  thumb_img = models.ImageField(
        verbose_name=_(u'Превью изображения (400x560px)'),
        help_text=u'400x560px',
        upload_to='about',
        blank=True,
        )
  img = models.ImageField(
        verbose_name=_(u'Изображение (Любой размер)'),
        help_text=u'Любой размер',
        upload_to='about',
        blank=True,
        )
  parent = models.ForeignKey(AboutPage, verbose_name=_(u'О нас'),)
  
  class TMeta:
      fields = [
        'is_published',
        'thumb_img',
        'img',
        ]
  
  class Meta:
        verbose_name = _(u'Сертификат')
        verbose_name_plural = _(u'Сертификаты')
        ordering = ['weight']
