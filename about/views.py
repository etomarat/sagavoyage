from django.shortcuts import render
from .models import AboutPage

def about(request):
    template_name='about.html'

    return render(request, template_name, {'object': AboutPage.objects.get()})