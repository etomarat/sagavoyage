# coding: utf-8

from django.db import models
from solo.models import SingletonModel
from django.utils.translation import ugettext_lazy as _
from ckeditor.fields import RichTextField
from kernel.db import PublishModel, TitleMixin, SortableMixin
from django.core.validators import MaxLengthValidator


class PartnersPage(SingletonModel):
    text1 = RichTextField(verbose_name=_(u'Текст1'), blank=True)
    
    block1_img = models.ImageField(
        verbose_name=_(u'Изображение блок "адрес"'),
        help_text=u'100x100px',
        upload_to='partners',
        blank=True,
        )
    block1_title = models.CharField(
        verbose_name=_(u'Заголовок блок "адрес"'),
        help_text=_(u'напр: Вы можете заполнить, подписать и отправить нам агентский договор на адрес:'),
        default=u'Вы можете заполнить, подписать и отправить нам агентский договор на адрес:',
        max_length=0xff,
        blank=True
        )
    block1_description = models.TextField(
        verbose_name=_(u'Описание блок "адрес"'),
        help_text=_(u'напр: 344064, г. Ростов-на-Дону, Вавилова 59в/101 , 2 этаж, офис 223'),
        default=u'344064, г. Ростов-на-Дону, Вавилова 59в/101 , 2 этаж, офис 223',
        blank=True,
        )
    
    block2_img = models.ImageField(
        verbose_name=_(u'Изображение блок "договор"'),
        help_text=u'100x100px',
        upload_to='partners',
        blank=True,
        )
    block2_title = models.CharField(
        verbose_name=_(u'Заголовок блок "договор"'),
        help_text=_(u'напр: Ознакомиться с агентским договором можно здесь:'),
        default=u'Ознакомиться с агентским договором можно здесь:',
        max_length=0xff,
        blank=True
        )
    block2_file = models.FileField(
        verbose_name=_(u'Фаил "договор"'),
        upload_to='partners',
        blank=True,
        )
    block2_file_title = models.CharField(
        verbose_name=_(u'Текст ссылки с на фаил с договором.'),
        help_text=_(u'напр: dogovor_dlya_agenta.doc'),
        default=u'dogovor_dlya_agenta.doc',
        max_length=0xff,
        blank=True
        )
    
    block3_img = models.ImageField(
        verbose_name=_(u'Изображение блок "контакты"'),
        help_text=u'100x100px',
        upload_to='partners',
        blank=True,
        )
    block3_title = models.CharField(
        verbose_name=_(u'Заголовок блок "контакты"'),
        help_text=_(u'напр: Единый бесплатный номер и электронная почта'),
        default=u'Репутация компании',
        max_length=0xff,
        blank=True
        )
    block3_description = RichTextField(
        verbose_name=_(u'Описание блок "контакты"'),
        help_text=_(u'Укажите здесь номер телефона и email.'),
        default=u'<a href="tel:8-800-775-21-01">8 (800) 775-21-01</a><br><a href="mailto:info@sagavoyages.ru">info@sagavoyages.ru</a>',
        blank=True,
        )
    
    text2 = RichTextField(verbose_name=_(u'Текст2'), blank=True)
    doc2_file = models.FileField(
        verbose_name=_(u'Фаил "договор для гостиниц"'),
        upload_to='partners',
        blank=True,
        )
    
    header_before_plashkas = models.CharField(
        verbose_name=_(u'Заголовок перед плашками'),
        help_text=_(u'напр: Для отелей России предлагаем наши услуги:'),
        default=u'Для отелей России предлагаем наши услуги:',
        max_length=0xff,
        blank=True
        )
    
    def __unicode__(self):
        return u'Страница "Партнёрам"'

    class Meta:
        verbose_name=_(u'Страница "Партнёрам"')
    
    class TMeta:
        fields = [
          'text1',
          'text2',
          'block1_img',
          'block2_img',
          'block3_img',
          'block1_title',
          'block2_title',
          'block3_title',
          'block2_file',
          'block2_file_title',
          'block1_description',
          'block3_description',
          'header_before_plashkas',
          'doc2_file',
          ]


class Plaha(PublishModel, TitleMixin, SortableMixin):
  img = models.ImageField(
        verbose_name=_(u'Изображение'),
        help_text=u'2560x356px',
        upload_to='partners',
        blank=True,
        )
  #link = models.CharField(verbose_name=_(u'Ссылка'), help_text=_(u'Кнопка "подробнее"'), max_length=0xff, blank=True)
  parent = models.ForeignKey(PartnersPage, verbose_name=_(u'О нас'),)
  
  class TMeta:
      fields = [
        'is_published',
        'title',
        'img',
        ]
  
  class Meta:
        verbose_name = _(u'Плашка')
        verbose_name_plural = _(u'Плашки')
        ordering = ['weight']
