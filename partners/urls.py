# coding: utf-8

from django.conf.urls import patterns, url

from .views import partners


urlpatterns = patterns(
    '',
    url(r'^$', partners, name='partners'),
    )

# EOF