# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'PartnersPage.text1_en'
        db.alter_column(u'partners_partnerspage', 'text1_en', self.gf('ckeditor.fields.RichTextField')(null=True))

        # Changing field 'PartnersPage.text1_ru'
        db.alter_column(u'partners_partnerspage', 'text1_ru', self.gf('ckeditor.fields.RichTextField')(null=True))

        # Changing field 'PartnersPage.text1'
        db.alter_column(u'partners_partnerspage', 'text1', self.gf('ckeditor.fields.RichTextField')())

    def backwards(self, orm):

        # Changing field 'PartnersPage.text1_en'
        db.alter_column(u'partners_partnerspage', 'text1_en', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'PartnersPage.text1_ru'
        db.alter_column(u'partners_partnerspage', 'text1_ru', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'PartnersPage.text1'
        db.alter_column(u'partners_partnerspage', 'text1', self.gf('django.db.models.fields.TextField')())

    models = {
        u'partners.partnerspage': {
            'Meta': {'object_name': 'PartnersPage'},
            'block1_description': ('django.db.models.fields.TextField', [], {'default': "u'344064, \\u0433. \\u0420\\u043e\\u0441\\u0442\\u043e\\u0432-\\u043d\\u0430-\\u0414\\u043e\\u043d\\u0443, \\u0412\\u0430\\u0432\\u0438\\u043b\\u043e\\u0432\\u0430 59\\u0432/101 , 2 \\u044d\\u0442\\u0430\\u0436, \\u043e\\u0444\\u0438\\u0441 223'", 'blank': 'True'}),
            'block1_description_en': ('django.db.models.fields.TextField', [], {'default': "u'344064, \\u0433. \\u0420\\u043e\\u0441\\u0442\\u043e\\u0432-\\u043d\\u0430-\\u0414\\u043e\\u043d\\u0443, \\u0412\\u0430\\u0432\\u0438\\u043b\\u043e\\u0432\\u0430 59\\u0432/101 , 2 \\u044d\\u0442\\u0430\\u0436, \\u043e\\u0444\\u0438\\u0441 223'", 'null': 'True', 'blank': 'True'}),
            'block1_description_ru': ('django.db.models.fields.TextField', [], {'default': "u'344064, \\u0433. \\u0420\\u043e\\u0441\\u0442\\u043e\\u0432-\\u043d\\u0430-\\u0414\\u043e\\u043d\\u0443, \\u0412\\u0430\\u0432\\u0438\\u043b\\u043e\\u0432\\u0430 59\\u0432/101 , 2 \\u044d\\u0442\\u0430\\u0436, \\u043e\\u0444\\u0438\\u0441 223'", 'null': 'True', 'blank': 'True'}),
            'block1_img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'block1_img_en': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block1_img_ru': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block1_title': ('django.db.models.fields.CharField', [], {'default': "u'\\u0412\\u044b \\u043c\\u043e\\u0436\\u0435\\u0442\\u0435 \\u0437\\u0430\\u043f\\u043e\\u043b\\u043d\\u0438\\u0442\\u044c, \\u043f\\u043e\\u0434\\u043f\\u0438\\u0441\\u0430\\u0442\\u044c \\u0438 \\u043e\\u0442\\u043f\\u0440\\u0430\\u0432\\u0438\\u0442\\u044c \\u043d\\u0430\\u043c \\u0430\\u0433\\u0435\\u043d\\u0442\\u0441\\u043a\\u0438\\u0439 \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440 \\u043d\\u0430 \\u0430\\u0434\\u0440\\u0435\\u0441:'", 'max_length': '255', 'blank': 'True'}),
            'block1_title_en': ('django.db.models.fields.CharField', [], {'default': "u'\\u0412\\u044b \\u043c\\u043e\\u0436\\u0435\\u0442\\u0435 \\u0437\\u0430\\u043f\\u043e\\u043b\\u043d\\u0438\\u0442\\u044c, \\u043f\\u043e\\u0434\\u043f\\u0438\\u0441\\u0430\\u0442\\u044c \\u0438 \\u043e\\u0442\\u043f\\u0440\\u0430\\u0432\\u0438\\u0442\\u044c \\u043d\\u0430\\u043c \\u0430\\u0433\\u0435\\u043d\\u0442\\u0441\\u043a\\u0438\\u0439 \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440 \\u043d\\u0430 \\u0430\\u0434\\u0440\\u0435\\u0441:'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block1_title_ru': ('django.db.models.fields.CharField', [], {'default': "u'\\u0412\\u044b \\u043c\\u043e\\u0436\\u0435\\u0442\\u0435 \\u0437\\u0430\\u043f\\u043e\\u043b\\u043d\\u0438\\u0442\\u044c, \\u043f\\u043e\\u0434\\u043f\\u0438\\u0441\\u0430\\u0442\\u044c \\u0438 \\u043e\\u0442\\u043f\\u0440\\u0430\\u0432\\u0438\\u0442\\u044c \\u043d\\u0430\\u043c \\u0430\\u0433\\u0435\\u043d\\u0442\\u0441\\u043a\\u0438\\u0439 \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440 \\u043d\\u0430 \\u0430\\u0434\\u0440\\u0435\\u0441:'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block2_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'blank': 'True'}),
            'block2_file_en': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block2_file_ru': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block2_file_title': ('django.db.models.fields.CharField', [], {'default': "u'dogovor_dlya_agenta.doc'", 'max_length': '255', 'blank': 'True'}),
            'block2_file_title_en': ('django.db.models.fields.CharField', [], {'default': "u'dogovor_dlya_agenta.doc'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block2_file_title_ru': ('django.db.models.fields.CharField', [], {'default': "u'dogovor_dlya_agenta.doc'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block2_img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'block2_img_en': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block2_img_ru': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block2_title': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0437\\u043d\\u0430\\u043a\\u043e\\u043c\\u0438\\u0442\\u044c\\u0441\\u044f \\u0441 \\u0430\\u0433\\u0435\\u043d\\u0442\\u0441\\u043a\\u0438\\u043c \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440\\u043e\\u043c \\u043c\\u043e\\u0436\\u043d\\u043e \\u0437\\u0434\\u0435\\u0441\\u044c:'", 'max_length': '255', 'blank': 'True'}),
            'block2_title_en': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0437\\u043d\\u0430\\u043a\\u043e\\u043c\\u0438\\u0442\\u044c\\u0441\\u044f \\u0441 \\u0430\\u0433\\u0435\\u043d\\u0442\\u0441\\u043a\\u0438\\u043c \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440\\u043e\\u043c \\u043c\\u043e\\u0436\\u043d\\u043e \\u0437\\u0434\\u0435\\u0441\\u044c:'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block2_title_ru': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0437\\u043d\\u0430\\u043a\\u043e\\u043c\\u0438\\u0442\\u044c\\u0441\\u044f \\u0441 \\u0430\\u0433\\u0435\\u043d\\u0442\\u0441\\u043a\\u0438\\u043c \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440\\u043e\\u043c \\u043c\\u043e\\u0436\\u043d\\u043e \\u0437\\u0434\\u0435\\u0441\\u044c:'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block3_description': ('ckeditor.fields.RichTextField', [], {'default': 'u\'<a href="tel:8-800-775-21-01">8 (800) 775-21-01</a><br><a href="mailto:info@sagavoyages.ru">info@sagavoyages.ru</a>\'', 'blank': 'True'}),
            'block3_description_en': ('ckeditor.fields.RichTextField', [], {'default': 'u\'<a href="tel:8-800-775-21-01">8 (800) 775-21-01</a><br><a href="mailto:info@sagavoyages.ru">info@sagavoyages.ru</a>\'', 'null': 'True', 'blank': 'True'}),
            'block3_description_ru': ('ckeditor.fields.RichTextField', [], {'default': 'u\'<a href="tel:8-800-775-21-01">8 (800) 775-21-01</a><br><a href="mailto:info@sagavoyages.ru">info@sagavoyages.ru</a>\'', 'null': 'True', 'blank': 'True'}),
            'block3_img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'block3_img_en': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block3_img_ru': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block3_title': ('django.db.models.fields.CharField', [], {'default': "u'\\u0420\\u0435\\u043f\\u0443\\u0442\\u0430\\u0446\\u0438\\u044f \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438'", 'max_length': '255', 'blank': 'True'}),
            'block3_title_en': ('django.db.models.fields.CharField', [], {'default': "u'\\u0420\\u0435\\u043f\\u0443\\u0442\\u0430\\u0446\\u0438\\u044f \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block3_title_ru': ('django.db.models.fields.CharField', [], {'default': "u'\\u0420\\u0435\\u043f\\u0443\\u0442\\u0430\\u0446\\u0438\\u044f \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'header_before_plashkas': ('django.db.models.fields.CharField', [], {'default': "u'\\u0414\\u043b\\u044f \\u043e\\u0442\\u0435\\u043b\\u0435\\u0439 \\u0420\\u043e\\u0441\\u0441\\u0438\\u0438 \\u043f\\u0440\\u0435\\u0434\\u043b\\u0430\\u0433\\u0430\\u0435\\u043c \\u043d\\u0430\\u0448\\u0438 \\u0443\\u0441\\u043b\\u0443\\u0433\\u0438:'", 'max_length': '255', 'blank': 'True'}),
            'header_before_plashkas_en': ('django.db.models.fields.CharField', [], {'default': "u'\\u0414\\u043b\\u044f \\u043e\\u0442\\u0435\\u043b\\u0435\\u0439 \\u0420\\u043e\\u0441\\u0441\\u0438\\u0438 \\u043f\\u0440\\u0435\\u0434\\u043b\\u0430\\u0433\\u0430\\u0435\\u043c \\u043d\\u0430\\u0448\\u0438 \\u0443\\u0441\\u043b\\u0443\\u0433\\u0438:'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'header_before_plashkas_ru': ('django.db.models.fields.CharField', [], {'default': "u'\\u0414\\u043b\\u044f \\u043e\\u0442\\u0435\\u043b\\u0435\\u0439 \\u0420\\u043e\\u0441\\u0441\\u0438\\u0438 \\u043f\\u0440\\u0435\\u0434\\u043b\\u0430\\u0433\\u0430\\u0435\\u043c \\u043d\\u0430\\u0448\\u0438 \\u0443\\u0441\\u043b\\u0443\\u0433\\u0438:'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text1': ('ckeditor.fields.RichTextField', [], {'blank': 'True'}),
            'text1_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text1_ru': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text2': ('ckeditor.fields.RichTextField', [], {'blank': 'True'}),
            'text2_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text2_ru': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'})
        },
        u'partners.plaha': {
            'Meta': {'object_name': 'Plaha'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'img_en': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_ru': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_en': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_ru': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['partners.PartnersPage']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['partners']