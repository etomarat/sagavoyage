# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'PartnersPage.text1_fr'
        db.add_column(u'partners_partnerspage', 'text1_fr',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.text1_de'
        db.add_column(u'partners_partnerspage', 'text1_de',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.text1_es'
        db.add_column(u'partners_partnerspage', 'text1_es',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.text1_cn'
        db.add_column(u'partners_partnerspage', 'text1_cn',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block1_img_fr'
        db.add_column(u'partners_partnerspage', 'block1_img_fr',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block1_img_de'
        db.add_column(u'partners_partnerspage', 'block1_img_de',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block1_img_es'
        db.add_column(u'partners_partnerspage', 'block1_img_es',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block1_img_cn'
        db.add_column(u'partners_partnerspage', 'block1_img_cn',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block1_title_fr'
        db.add_column(u'partners_partnerspage', 'block1_title_fr',
                      self.gf('django.db.models.fields.CharField')(default=u'\u0412\u044b \u043c\u043e\u0436\u0435\u0442\u0435 \u0437\u0430\u043f\u043e\u043b\u043d\u0438\u0442\u044c, \u043f\u043e\u0434\u043f\u0438\u0441\u0430\u0442\u044c \u0438 \u043e\u0442\u043f\u0440\u0430\u0432\u0438\u0442\u044c \u043d\u0430\u043c \u0430\u0433\u0435\u043d\u0442\u0441\u043a\u0438\u0439 \u0434\u043e\u0433\u043e\u0432\u043e\u0440 \u043d\u0430 \u0430\u0434\u0440\u0435\u0441:', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block1_title_de'
        db.add_column(u'partners_partnerspage', 'block1_title_de',
                      self.gf('django.db.models.fields.CharField')(default=u'\u0412\u044b \u043c\u043e\u0436\u0435\u0442\u0435 \u0437\u0430\u043f\u043e\u043b\u043d\u0438\u0442\u044c, \u043f\u043e\u0434\u043f\u0438\u0441\u0430\u0442\u044c \u0438 \u043e\u0442\u043f\u0440\u0430\u0432\u0438\u0442\u044c \u043d\u0430\u043c \u0430\u0433\u0435\u043d\u0442\u0441\u043a\u0438\u0439 \u0434\u043e\u0433\u043e\u0432\u043e\u0440 \u043d\u0430 \u0430\u0434\u0440\u0435\u0441:', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block1_title_es'
        db.add_column(u'partners_partnerspage', 'block1_title_es',
                      self.gf('django.db.models.fields.CharField')(default=u'\u0412\u044b \u043c\u043e\u0436\u0435\u0442\u0435 \u0437\u0430\u043f\u043e\u043b\u043d\u0438\u0442\u044c, \u043f\u043e\u0434\u043f\u0438\u0441\u0430\u0442\u044c \u0438 \u043e\u0442\u043f\u0440\u0430\u0432\u0438\u0442\u044c \u043d\u0430\u043c \u0430\u0433\u0435\u043d\u0442\u0441\u043a\u0438\u0439 \u0434\u043e\u0433\u043e\u0432\u043e\u0440 \u043d\u0430 \u0430\u0434\u0440\u0435\u0441:', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block1_title_cn'
        db.add_column(u'partners_partnerspage', 'block1_title_cn',
                      self.gf('django.db.models.fields.CharField')(default=u'\u0412\u044b \u043c\u043e\u0436\u0435\u0442\u0435 \u0437\u0430\u043f\u043e\u043b\u043d\u0438\u0442\u044c, \u043f\u043e\u0434\u043f\u0438\u0441\u0430\u0442\u044c \u0438 \u043e\u0442\u043f\u0440\u0430\u0432\u0438\u0442\u044c \u043d\u0430\u043c \u0430\u0433\u0435\u043d\u0442\u0441\u043a\u0438\u0439 \u0434\u043e\u0433\u043e\u0432\u043e\u0440 \u043d\u0430 \u0430\u0434\u0440\u0435\u0441:', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block1_description_fr'
        db.add_column(u'partners_partnerspage', 'block1_description_fr',
                      self.gf('django.db.models.fields.TextField')(default=u'344064, \u0433. \u0420\u043e\u0441\u0442\u043e\u0432-\u043d\u0430-\u0414\u043e\u043d\u0443, \u0412\u0430\u0432\u0438\u043b\u043e\u0432\u0430 59\u0432/101 , 2 \u044d\u0442\u0430\u0436, \u043e\u0444\u0438\u0441 223', null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block1_description_de'
        db.add_column(u'partners_partnerspage', 'block1_description_de',
                      self.gf('django.db.models.fields.TextField')(default=u'344064, \u0433. \u0420\u043e\u0441\u0442\u043e\u0432-\u043d\u0430-\u0414\u043e\u043d\u0443, \u0412\u0430\u0432\u0438\u043b\u043e\u0432\u0430 59\u0432/101 , 2 \u044d\u0442\u0430\u0436, \u043e\u0444\u0438\u0441 223', null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block1_description_es'
        db.add_column(u'partners_partnerspage', 'block1_description_es',
                      self.gf('django.db.models.fields.TextField')(default=u'344064, \u0433. \u0420\u043e\u0441\u0442\u043e\u0432-\u043d\u0430-\u0414\u043e\u043d\u0443, \u0412\u0430\u0432\u0438\u043b\u043e\u0432\u0430 59\u0432/101 , 2 \u044d\u0442\u0430\u0436, \u043e\u0444\u0438\u0441 223', null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block1_description_cn'
        db.add_column(u'partners_partnerspage', 'block1_description_cn',
                      self.gf('django.db.models.fields.TextField')(default=u'344064, \u0433. \u0420\u043e\u0441\u0442\u043e\u0432-\u043d\u0430-\u0414\u043e\u043d\u0443, \u0412\u0430\u0432\u0438\u043b\u043e\u0432\u0430 59\u0432/101 , 2 \u044d\u0442\u0430\u0436, \u043e\u0444\u0438\u0441 223', null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block2_img_fr'
        db.add_column(u'partners_partnerspage', 'block2_img_fr',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block2_img_de'
        db.add_column(u'partners_partnerspage', 'block2_img_de',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block2_img_es'
        db.add_column(u'partners_partnerspage', 'block2_img_es',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block2_img_cn'
        db.add_column(u'partners_partnerspage', 'block2_img_cn',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block2_title_fr'
        db.add_column(u'partners_partnerspage', 'block2_title_fr',
                      self.gf('django.db.models.fields.CharField')(default=u'\u041e\u0437\u043d\u0430\u043a\u043e\u043c\u0438\u0442\u044c\u0441\u044f \u0441 \u0430\u0433\u0435\u043d\u0442\u0441\u043a\u0438\u043c \u0434\u043e\u0433\u043e\u0432\u043e\u0440\u043e\u043c \u043c\u043e\u0436\u043d\u043e \u0437\u0434\u0435\u0441\u044c:', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block2_title_de'
        db.add_column(u'partners_partnerspage', 'block2_title_de',
                      self.gf('django.db.models.fields.CharField')(default=u'\u041e\u0437\u043d\u0430\u043a\u043e\u043c\u0438\u0442\u044c\u0441\u044f \u0441 \u0430\u0433\u0435\u043d\u0442\u0441\u043a\u0438\u043c \u0434\u043e\u0433\u043e\u0432\u043e\u0440\u043e\u043c \u043c\u043e\u0436\u043d\u043e \u0437\u0434\u0435\u0441\u044c:', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block2_title_es'
        db.add_column(u'partners_partnerspage', 'block2_title_es',
                      self.gf('django.db.models.fields.CharField')(default=u'\u041e\u0437\u043d\u0430\u043a\u043e\u043c\u0438\u0442\u044c\u0441\u044f \u0441 \u0430\u0433\u0435\u043d\u0442\u0441\u043a\u0438\u043c \u0434\u043e\u0433\u043e\u0432\u043e\u0440\u043e\u043c \u043c\u043e\u0436\u043d\u043e \u0437\u0434\u0435\u0441\u044c:', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block2_title_cn'
        db.add_column(u'partners_partnerspage', 'block2_title_cn',
                      self.gf('django.db.models.fields.CharField')(default=u'\u041e\u0437\u043d\u0430\u043a\u043e\u043c\u0438\u0442\u044c\u0441\u044f \u0441 \u0430\u0433\u0435\u043d\u0442\u0441\u043a\u0438\u043c \u0434\u043e\u0433\u043e\u0432\u043e\u0440\u043e\u043c \u043c\u043e\u0436\u043d\u043e \u0437\u0434\u0435\u0441\u044c:', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block2_file_fr'
        db.add_column(u'partners_partnerspage', 'block2_file_fr',
                      self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block2_file_de'
        db.add_column(u'partners_partnerspage', 'block2_file_de',
                      self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block2_file_es'
        db.add_column(u'partners_partnerspage', 'block2_file_es',
                      self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block2_file_cn'
        db.add_column(u'partners_partnerspage', 'block2_file_cn',
                      self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block2_file_title_fr'
        db.add_column(u'partners_partnerspage', 'block2_file_title_fr',
                      self.gf('django.db.models.fields.CharField')(default=u'dogovor_dlya_agenta.doc', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block2_file_title_de'
        db.add_column(u'partners_partnerspage', 'block2_file_title_de',
                      self.gf('django.db.models.fields.CharField')(default=u'dogovor_dlya_agenta.doc', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block2_file_title_es'
        db.add_column(u'partners_partnerspage', 'block2_file_title_es',
                      self.gf('django.db.models.fields.CharField')(default=u'dogovor_dlya_agenta.doc', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block2_file_title_cn'
        db.add_column(u'partners_partnerspage', 'block2_file_title_cn',
                      self.gf('django.db.models.fields.CharField')(default=u'dogovor_dlya_agenta.doc', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block3_img_fr'
        db.add_column(u'partners_partnerspage', 'block3_img_fr',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block3_img_de'
        db.add_column(u'partners_partnerspage', 'block3_img_de',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block3_img_es'
        db.add_column(u'partners_partnerspage', 'block3_img_es',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block3_img_cn'
        db.add_column(u'partners_partnerspage', 'block3_img_cn',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block3_title_fr'
        db.add_column(u'partners_partnerspage', 'block3_title_fr',
                      self.gf('django.db.models.fields.CharField')(default=u'\u0420\u0435\u043f\u0443\u0442\u0430\u0446\u0438\u044f \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block3_title_de'
        db.add_column(u'partners_partnerspage', 'block3_title_de',
                      self.gf('django.db.models.fields.CharField')(default=u'\u0420\u0435\u043f\u0443\u0442\u0430\u0446\u0438\u044f \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block3_title_es'
        db.add_column(u'partners_partnerspage', 'block3_title_es',
                      self.gf('django.db.models.fields.CharField')(default=u'\u0420\u0435\u043f\u0443\u0442\u0430\u0446\u0438\u044f \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block3_title_cn'
        db.add_column(u'partners_partnerspage', 'block3_title_cn',
                      self.gf('django.db.models.fields.CharField')(default=u'\u0420\u0435\u043f\u0443\u0442\u0430\u0446\u0438\u044f \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block3_description_fr'
        db.add_column(u'partners_partnerspage', 'block3_description_fr',
                      self.gf('ckeditor.fields.RichTextField')(default=u'<a href="tel:8-800-775-21-01">8 (800) 775-21-01</a><br><a href="mailto:info@sagavoyages.ru">info@sagavoyages.ru</a>', null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block3_description_de'
        db.add_column(u'partners_partnerspage', 'block3_description_de',
                      self.gf('ckeditor.fields.RichTextField')(default=u'<a href="tel:8-800-775-21-01">8 (800) 775-21-01</a><br><a href="mailto:info@sagavoyages.ru">info@sagavoyages.ru</a>', null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block3_description_es'
        db.add_column(u'partners_partnerspage', 'block3_description_es',
                      self.gf('ckeditor.fields.RichTextField')(default=u'<a href="tel:8-800-775-21-01">8 (800) 775-21-01</a><br><a href="mailto:info@sagavoyages.ru">info@sagavoyages.ru</a>', null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.block3_description_cn'
        db.add_column(u'partners_partnerspage', 'block3_description_cn',
                      self.gf('ckeditor.fields.RichTextField')(default=u'<a href="tel:8-800-775-21-01">8 (800) 775-21-01</a><br><a href="mailto:info@sagavoyages.ru">info@sagavoyages.ru</a>', null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.text2_fr'
        db.add_column(u'partners_partnerspage', 'text2_fr',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.text2_de'
        db.add_column(u'partners_partnerspage', 'text2_de',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.text2_es'
        db.add_column(u'partners_partnerspage', 'text2_es',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.text2_cn'
        db.add_column(u'partners_partnerspage', 'text2_cn',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.doc2_file_fr'
        db.add_column(u'partners_partnerspage', 'doc2_file_fr',
                      self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.doc2_file_de'
        db.add_column(u'partners_partnerspage', 'doc2_file_de',
                      self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.doc2_file_es'
        db.add_column(u'partners_partnerspage', 'doc2_file_es',
                      self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.doc2_file_cn'
        db.add_column(u'partners_partnerspage', 'doc2_file_cn',
                      self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.header_before_plashkas_fr'
        db.add_column(u'partners_partnerspage', 'header_before_plashkas_fr',
                      self.gf('django.db.models.fields.CharField')(default=u'\u0414\u043b\u044f \u043e\u0442\u0435\u043b\u0435\u0439 \u0420\u043e\u0441\u0441\u0438\u0438 \u043f\u0440\u0435\u0434\u043b\u0430\u0433\u0430\u0435\u043c \u043d\u0430\u0448\u0438 \u0443\u0441\u043b\u0443\u0433\u0438:', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.header_before_plashkas_de'
        db.add_column(u'partners_partnerspage', 'header_before_plashkas_de',
                      self.gf('django.db.models.fields.CharField')(default=u'\u0414\u043b\u044f \u043e\u0442\u0435\u043b\u0435\u0439 \u0420\u043e\u0441\u0441\u0438\u0438 \u043f\u0440\u0435\u0434\u043b\u0430\u0433\u0430\u0435\u043c \u043d\u0430\u0448\u0438 \u0443\u0441\u043b\u0443\u0433\u0438:', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.header_before_plashkas_es'
        db.add_column(u'partners_partnerspage', 'header_before_plashkas_es',
                      self.gf('django.db.models.fields.CharField')(default=u'\u0414\u043b\u044f \u043e\u0442\u0435\u043b\u0435\u0439 \u0420\u043e\u0441\u0441\u0438\u0438 \u043f\u0440\u0435\u0434\u043b\u0430\u0433\u0430\u0435\u043c \u043d\u0430\u0448\u0438 \u0443\u0441\u043b\u0443\u0433\u0438:', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'PartnersPage.header_before_plashkas_cn'
        db.add_column(u'partners_partnerspage', 'header_before_plashkas_cn',
                      self.gf('django.db.models.fields.CharField')(default=u'\u0414\u043b\u044f \u043e\u0442\u0435\u043b\u0435\u0439 \u0420\u043e\u0441\u0441\u0438\u0438 \u043f\u0440\u0435\u0434\u043b\u0430\u0433\u0430\u0435\u043c \u043d\u0430\u0448\u0438 \u0443\u0441\u043b\u0443\u0433\u0438:', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Plaha.is_published_fr'
        db.add_column(u'partners_plaha', 'is_published_fr',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Plaha.is_published_de'
        db.add_column(u'partners_plaha', 'is_published_de',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Plaha.is_published_es'
        db.add_column(u'partners_plaha', 'is_published_es',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Plaha.is_published_cn'
        db.add_column(u'partners_plaha', 'is_published_cn',
                      self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True),
                      keep_default=False)

        # Adding field 'Plaha.title_fr'
        db.add_column(u'partners_plaha', 'title_fr',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Plaha.title_de'
        db.add_column(u'partners_plaha', 'title_de',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Plaha.title_es'
        db.add_column(u'partners_plaha', 'title_es',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Plaha.title_cn'
        db.add_column(u'partners_plaha', 'title_cn',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Plaha.img_fr'
        db.add_column(u'partners_plaha', 'img_fr',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Plaha.img_de'
        db.add_column(u'partners_plaha', 'img_de',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Plaha.img_es'
        db.add_column(u'partners_plaha', 'img_es',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Plaha.img_cn'
        db.add_column(u'partners_plaha', 'img_cn',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'PartnersPage.text1_fr'
        db.delete_column(u'partners_partnerspage', 'text1_fr')

        # Deleting field 'PartnersPage.text1_de'
        db.delete_column(u'partners_partnerspage', 'text1_de')

        # Deleting field 'PartnersPage.text1_es'
        db.delete_column(u'partners_partnerspage', 'text1_es')

        # Deleting field 'PartnersPage.text1_cn'
        db.delete_column(u'partners_partnerspage', 'text1_cn')

        # Deleting field 'PartnersPage.block1_img_fr'
        db.delete_column(u'partners_partnerspage', 'block1_img_fr')

        # Deleting field 'PartnersPage.block1_img_de'
        db.delete_column(u'partners_partnerspage', 'block1_img_de')

        # Deleting field 'PartnersPage.block1_img_es'
        db.delete_column(u'partners_partnerspage', 'block1_img_es')

        # Deleting field 'PartnersPage.block1_img_cn'
        db.delete_column(u'partners_partnerspage', 'block1_img_cn')

        # Deleting field 'PartnersPage.block1_title_fr'
        db.delete_column(u'partners_partnerspage', 'block1_title_fr')

        # Deleting field 'PartnersPage.block1_title_de'
        db.delete_column(u'partners_partnerspage', 'block1_title_de')

        # Deleting field 'PartnersPage.block1_title_es'
        db.delete_column(u'partners_partnerspage', 'block1_title_es')

        # Deleting field 'PartnersPage.block1_title_cn'
        db.delete_column(u'partners_partnerspage', 'block1_title_cn')

        # Deleting field 'PartnersPage.block1_description_fr'
        db.delete_column(u'partners_partnerspage', 'block1_description_fr')

        # Deleting field 'PartnersPage.block1_description_de'
        db.delete_column(u'partners_partnerspage', 'block1_description_de')

        # Deleting field 'PartnersPage.block1_description_es'
        db.delete_column(u'partners_partnerspage', 'block1_description_es')

        # Deleting field 'PartnersPage.block1_description_cn'
        db.delete_column(u'partners_partnerspage', 'block1_description_cn')

        # Deleting field 'PartnersPage.block2_img_fr'
        db.delete_column(u'partners_partnerspage', 'block2_img_fr')

        # Deleting field 'PartnersPage.block2_img_de'
        db.delete_column(u'partners_partnerspage', 'block2_img_de')

        # Deleting field 'PartnersPage.block2_img_es'
        db.delete_column(u'partners_partnerspage', 'block2_img_es')

        # Deleting field 'PartnersPage.block2_img_cn'
        db.delete_column(u'partners_partnerspage', 'block2_img_cn')

        # Deleting field 'PartnersPage.block2_title_fr'
        db.delete_column(u'partners_partnerspage', 'block2_title_fr')

        # Deleting field 'PartnersPage.block2_title_de'
        db.delete_column(u'partners_partnerspage', 'block2_title_de')

        # Deleting field 'PartnersPage.block2_title_es'
        db.delete_column(u'partners_partnerspage', 'block2_title_es')

        # Deleting field 'PartnersPage.block2_title_cn'
        db.delete_column(u'partners_partnerspage', 'block2_title_cn')

        # Deleting field 'PartnersPage.block2_file_fr'
        db.delete_column(u'partners_partnerspage', 'block2_file_fr')

        # Deleting field 'PartnersPage.block2_file_de'
        db.delete_column(u'partners_partnerspage', 'block2_file_de')

        # Deleting field 'PartnersPage.block2_file_es'
        db.delete_column(u'partners_partnerspage', 'block2_file_es')

        # Deleting field 'PartnersPage.block2_file_cn'
        db.delete_column(u'partners_partnerspage', 'block2_file_cn')

        # Deleting field 'PartnersPage.block2_file_title_fr'
        db.delete_column(u'partners_partnerspage', 'block2_file_title_fr')

        # Deleting field 'PartnersPage.block2_file_title_de'
        db.delete_column(u'partners_partnerspage', 'block2_file_title_de')

        # Deleting field 'PartnersPage.block2_file_title_es'
        db.delete_column(u'partners_partnerspage', 'block2_file_title_es')

        # Deleting field 'PartnersPage.block2_file_title_cn'
        db.delete_column(u'partners_partnerspage', 'block2_file_title_cn')

        # Deleting field 'PartnersPage.block3_img_fr'
        db.delete_column(u'partners_partnerspage', 'block3_img_fr')

        # Deleting field 'PartnersPage.block3_img_de'
        db.delete_column(u'partners_partnerspage', 'block3_img_de')

        # Deleting field 'PartnersPage.block3_img_es'
        db.delete_column(u'partners_partnerspage', 'block3_img_es')

        # Deleting field 'PartnersPage.block3_img_cn'
        db.delete_column(u'partners_partnerspage', 'block3_img_cn')

        # Deleting field 'PartnersPage.block3_title_fr'
        db.delete_column(u'partners_partnerspage', 'block3_title_fr')

        # Deleting field 'PartnersPage.block3_title_de'
        db.delete_column(u'partners_partnerspage', 'block3_title_de')

        # Deleting field 'PartnersPage.block3_title_es'
        db.delete_column(u'partners_partnerspage', 'block3_title_es')

        # Deleting field 'PartnersPage.block3_title_cn'
        db.delete_column(u'partners_partnerspage', 'block3_title_cn')

        # Deleting field 'PartnersPage.block3_description_fr'
        db.delete_column(u'partners_partnerspage', 'block3_description_fr')

        # Deleting field 'PartnersPage.block3_description_de'
        db.delete_column(u'partners_partnerspage', 'block3_description_de')

        # Deleting field 'PartnersPage.block3_description_es'
        db.delete_column(u'partners_partnerspage', 'block3_description_es')

        # Deleting field 'PartnersPage.block3_description_cn'
        db.delete_column(u'partners_partnerspage', 'block3_description_cn')

        # Deleting field 'PartnersPage.text2_fr'
        db.delete_column(u'partners_partnerspage', 'text2_fr')

        # Deleting field 'PartnersPage.text2_de'
        db.delete_column(u'partners_partnerspage', 'text2_de')

        # Deleting field 'PartnersPage.text2_es'
        db.delete_column(u'partners_partnerspage', 'text2_es')

        # Deleting field 'PartnersPage.text2_cn'
        db.delete_column(u'partners_partnerspage', 'text2_cn')

        # Deleting field 'PartnersPage.doc2_file_fr'
        db.delete_column(u'partners_partnerspage', 'doc2_file_fr')

        # Deleting field 'PartnersPage.doc2_file_de'
        db.delete_column(u'partners_partnerspage', 'doc2_file_de')

        # Deleting field 'PartnersPage.doc2_file_es'
        db.delete_column(u'partners_partnerspage', 'doc2_file_es')

        # Deleting field 'PartnersPage.doc2_file_cn'
        db.delete_column(u'partners_partnerspage', 'doc2_file_cn')

        # Deleting field 'PartnersPage.header_before_plashkas_fr'
        db.delete_column(u'partners_partnerspage', 'header_before_plashkas_fr')

        # Deleting field 'PartnersPage.header_before_plashkas_de'
        db.delete_column(u'partners_partnerspage', 'header_before_plashkas_de')

        # Deleting field 'PartnersPage.header_before_plashkas_es'
        db.delete_column(u'partners_partnerspage', 'header_before_plashkas_es')

        # Deleting field 'PartnersPage.header_before_plashkas_cn'
        db.delete_column(u'partners_partnerspage', 'header_before_plashkas_cn')

        # Deleting field 'Plaha.is_published_fr'
        db.delete_column(u'partners_plaha', 'is_published_fr')

        # Deleting field 'Plaha.is_published_de'
        db.delete_column(u'partners_plaha', 'is_published_de')

        # Deleting field 'Plaha.is_published_es'
        db.delete_column(u'partners_plaha', 'is_published_es')

        # Deleting field 'Plaha.is_published_cn'
        db.delete_column(u'partners_plaha', 'is_published_cn')

        # Deleting field 'Plaha.title_fr'
        db.delete_column(u'partners_plaha', 'title_fr')

        # Deleting field 'Plaha.title_de'
        db.delete_column(u'partners_plaha', 'title_de')

        # Deleting field 'Plaha.title_es'
        db.delete_column(u'partners_plaha', 'title_es')

        # Deleting field 'Plaha.title_cn'
        db.delete_column(u'partners_plaha', 'title_cn')

        # Deleting field 'Plaha.img_fr'
        db.delete_column(u'partners_plaha', 'img_fr')

        # Deleting field 'Plaha.img_de'
        db.delete_column(u'partners_plaha', 'img_de')

        # Deleting field 'Plaha.img_es'
        db.delete_column(u'partners_plaha', 'img_es')

        # Deleting field 'Plaha.img_cn'
        db.delete_column(u'partners_plaha', 'img_cn')


    models = {
        u'partners.partnerspage': {
            'Meta': {'object_name': 'PartnersPage'},
            'block1_description': ('django.db.models.fields.TextField', [], {'default': "u'344064, \\u0433. \\u0420\\u043e\\u0441\\u0442\\u043e\\u0432-\\u043d\\u0430-\\u0414\\u043e\\u043d\\u0443, \\u0412\\u0430\\u0432\\u0438\\u043b\\u043e\\u0432\\u0430 59\\u0432/101 , 2 \\u044d\\u0442\\u0430\\u0436, \\u043e\\u0444\\u0438\\u0441 223'", 'blank': 'True'}),
            'block1_description_cn': ('django.db.models.fields.TextField', [], {'default': "u'344064, \\u0433. \\u0420\\u043e\\u0441\\u0442\\u043e\\u0432-\\u043d\\u0430-\\u0414\\u043e\\u043d\\u0443, \\u0412\\u0430\\u0432\\u0438\\u043b\\u043e\\u0432\\u0430 59\\u0432/101 , 2 \\u044d\\u0442\\u0430\\u0436, \\u043e\\u0444\\u0438\\u0441 223'", 'null': 'True', 'blank': 'True'}),
            'block1_description_de': ('django.db.models.fields.TextField', [], {'default': "u'344064, \\u0433. \\u0420\\u043e\\u0441\\u0442\\u043e\\u0432-\\u043d\\u0430-\\u0414\\u043e\\u043d\\u0443, \\u0412\\u0430\\u0432\\u0438\\u043b\\u043e\\u0432\\u0430 59\\u0432/101 , 2 \\u044d\\u0442\\u0430\\u0436, \\u043e\\u0444\\u0438\\u0441 223'", 'null': 'True', 'blank': 'True'}),
            'block1_description_en': ('django.db.models.fields.TextField', [], {'default': "u'344064, \\u0433. \\u0420\\u043e\\u0441\\u0442\\u043e\\u0432-\\u043d\\u0430-\\u0414\\u043e\\u043d\\u0443, \\u0412\\u0430\\u0432\\u0438\\u043b\\u043e\\u0432\\u0430 59\\u0432/101 , 2 \\u044d\\u0442\\u0430\\u0436, \\u043e\\u0444\\u0438\\u0441 223'", 'null': 'True', 'blank': 'True'}),
            'block1_description_es': ('django.db.models.fields.TextField', [], {'default': "u'344064, \\u0433. \\u0420\\u043e\\u0441\\u0442\\u043e\\u0432-\\u043d\\u0430-\\u0414\\u043e\\u043d\\u0443, \\u0412\\u0430\\u0432\\u0438\\u043b\\u043e\\u0432\\u0430 59\\u0432/101 , 2 \\u044d\\u0442\\u0430\\u0436, \\u043e\\u0444\\u0438\\u0441 223'", 'null': 'True', 'blank': 'True'}),
            'block1_description_fr': ('django.db.models.fields.TextField', [], {'default': "u'344064, \\u0433. \\u0420\\u043e\\u0441\\u0442\\u043e\\u0432-\\u043d\\u0430-\\u0414\\u043e\\u043d\\u0443, \\u0412\\u0430\\u0432\\u0438\\u043b\\u043e\\u0432\\u0430 59\\u0432/101 , 2 \\u044d\\u0442\\u0430\\u0436, \\u043e\\u0444\\u0438\\u0441 223'", 'null': 'True', 'blank': 'True'}),
            'block1_description_ru': ('django.db.models.fields.TextField', [], {'default': "u'344064, \\u0433. \\u0420\\u043e\\u0441\\u0442\\u043e\\u0432-\\u043d\\u0430-\\u0414\\u043e\\u043d\\u0443, \\u0412\\u0430\\u0432\\u0438\\u043b\\u043e\\u0432\\u0430 59\\u0432/101 , 2 \\u044d\\u0442\\u0430\\u0436, \\u043e\\u0444\\u0438\\u0441 223'", 'null': 'True', 'blank': 'True'}),
            'block1_img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'block1_img_cn': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block1_img_de': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block1_img_en': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block1_img_es': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block1_img_fr': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block1_img_ru': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block1_title': ('django.db.models.fields.CharField', [], {'default': "u'\\u0412\\u044b \\u043c\\u043e\\u0436\\u0435\\u0442\\u0435 \\u0437\\u0430\\u043f\\u043e\\u043b\\u043d\\u0438\\u0442\\u044c, \\u043f\\u043e\\u0434\\u043f\\u0438\\u0441\\u0430\\u0442\\u044c \\u0438 \\u043e\\u0442\\u043f\\u0440\\u0430\\u0432\\u0438\\u0442\\u044c \\u043d\\u0430\\u043c \\u0430\\u0433\\u0435\\u043d\\u0442\\u0441\\u043a\\u0438\\u0439 \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440 \\u043d\\u0430 \\u0430\\u0434\\u0440\\u0435\\u0441:'", 'max_length': '255', 'blank': 'True'}),
            'block1_title_cn': ('django.db.models.fields.CharField', [], {'default': "u'\\u0412\\u044b \\u043c\\u043e\\u0436\\u0435\\u0442\\u0435 \\u0437\\u0430\\u043f\\u043e\\u043b\\u043d\\u0438\\u0442\\u044c, \\u043f\\u043e\\u0434\\u043f\\u0438\\u0441\\u0430\\u0442\\u044c \\u0438 \\u043e\\u0442\\u043f\\u0440\\u0430\\u0432\\u0438\\u0442\\u044c \\u043d\\u0430\\u043c \\u0430\\u0433\\u0435\\u043d\\u0442\\u0441\\u043a\\u0438\\u0439 \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440 \\u043d\\u0430 \\u0430\\u0434\\u0440\\u0435\\u0441:'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block1_title_de': ('django.db.models.fields.CharField', [], {'default': "u'\\u0412\\u044b \\u043c\\u043e\\u0436\\u0435\\u0442\\u0435 \\u0437\\u0430\\u043f\\u043e\\u043b\\u043d\\u0438\\u0442\\u044c, \\u043f\\u043e\\u0434\\u043f\\u0438\\u0441\\u0430\\u0442\\u044c \\u0438 \\u043e\\u0442\\u043f\\u0440\\u0430\\u0432\\u0438\\u0442\\u044c \\u043d\\u0430\\u043c \\u0430\\u0433\\u0435\\u043d\\u0442\\u0441\\u043a\\u0438\\u0439 \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440 \\u043d\\u0430 \\u0430\\u0434\\u0440\\u0435\\u0441:'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block1_title_en': ('django.db.models.fields.CharField', [], {'default': "u'\\u0412\\u044b \\u043c\\u043e\\u0436\\u0435\\u0442\\u0435 \\u0437\\u0430\\u043f\\u043e\\u043b\\u043d\\u0438\\u0442\\u044c, \\u043f\\u043e\\u0434\\u043f\\u0438\\u0441\\u0430\\u0442\\u044c \\u0438 \\u043e\\u0442\\u043f\\u0440\\u0430\\u0432\\u0438\\u0442\\u044c \\u043d\\u0430\\u043c \\u0430\\u0433\\u0435\\u043d\\u0442\\u0441\\u043a\\u0438\\u0439 \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440 \\u043d\\u0430 \\u0430\\u0434\\u0440\\u0435\\u0441:'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block1_title_es': ('django.db.models.fields.CharField', [], {'default': "u'\\u0412\\u044b \\u043c\\u043e\\u0436\\u0435\\u0442\\u0435 \\u0437\\u0430\\u043f\\u043e\\u043b\\u043d\\u0438\\u0442\\u044c, \\u043f\\u043e\\u0434\\u043f\\u0438\\u0441\\u0430\\u0442\\u044c \\u0438 \\u043e\\u0442\\u043f\\u0440\\u0430\\u0432\\u0438\\u0442\\u044c \\u043d\\u0430\\u043c \\u0430\\u0433\\u0435\\u043d\\u0442\\u0441\\u043a\\u0438\\u0439 \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440 \\u043d\\u0430 \\u0430\\u0434\\u0440\\u0435\\u0441:'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block1_title_fr': ('django.db.models.fields.CharField', [], {'default': "u'\\u0412\\u044b \\u043c\\u043e\\u0436\\u0435\\u0442\\u0435 \\u0437\\u0430\\u043f\\u043e\\u043b\\u043d\\u0438\\u0442\\u044c, \\u043f\\u043e\\u0434\\u043f\\u0438\\u0441\\u0430\\u0442\\u044c \\u0438 \\u043e\\u0442\\u043f\\u0440\\u0430\\u0432\\u0438\\u0442\\u044c \\u043d\\u0430\\u043c \\u0430\\u0433\\u0435\\u043d\\u0442\\u0441\\u043a\\u0438\\u0439 \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440 \\u043d\\u0430 \\u0430\\u0434\\u0440\\u0435\\u0441:'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block1_title_ru': ('django.db.models.fields.CharField', [], {'default': "u'\\u0412\\u044b \\u043c\\u043e\\u0436\\u0435\\u0442\\u0435 \\u0437\\u0430\\u043f\\u043e\\u043b\\u043d\\u0438\\u0442\\u044c, \\u043f\\u043e\\u0434\\u043f\\u0438\\u0441\\u0430\\u0442\\u044c \\u0438 \\u043e\\u0442\\u043f\\u0440\\u0430\\u0432\\u0438\\u0442\\u044c \\u043d\\u0430\\u043c \\u0430\\u0433\\u0435\\u043d\\u0442\\u0441\\u043a\\u0438\\u0439 \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440 \\u043d\\u0430 \\u0430\\u0434\\u0440\\u0435\\u0441:'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block2_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'blank': 'True'}),
            'block2_file_cn': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block2_file_de': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block2_file_en': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block2_file_es': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block2_file_fr': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block2_file_ru': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block2_file_title': ('django.db.models.fields.CharField', [], {'default': "u'dogovor_dlya_agenta.doc'", 'max_length': '255', 'blank': 'True'}),
            'block2_file_title_cn': ('django.db.models.fields.CharField', [], {'default': "u'dogovor_dlya_agenta.doc'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block2_file_title_de': ('django.db.models.fields.CharField', [], {'default': "u'dogovor_dlya_agenta.doc'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block2_file_title_en': ('django.db.models.fields.CharField', [], {'default': "u'dogovor_dlya_agenta.doc'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block2_file_title_es': ('django.db.models.fields.CharField', [], {'default': "u'dogovor_dlya_agenta.doc'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block2_file_title_fr': ('django.db.models.fields.CharField', [], {'default': "u'dogovor_dlya_agenta.doc'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block2_file_title_ru': ('django.db.models.fields.CharField', [], {'default': "u'dogovor_dlya_agenta.doc'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block2_img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'block2_img_cn': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block2_img_de': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block2_img_en': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block2_img_es': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block2_img_fr': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block2_img_ru': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block2_title': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0437\\u043d\\u0430\\u043a\\u043e\\u043c\\u0438\\u0442\\u044c\\u0441\\u044f \\u0441 \\u0430\\u0433\\u0435\\u043d\\u0442\\u0441\\u043a\\u0438\\u043c \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440\\u043e\\u043c \\u043c\\u043e\\u0436\\u043d\\u043e \\u0437\\u0434\\u0435\\u0441\\u044c:'", 'max_length': '255', 'blank': 'True'}),
            'block2_title_cn': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0437\\u043d\\u0430\\u043a\\u043e\\u043c\\u0438\\u0442\\u044c\\u0441\\u044f \\u0441 \\u0430\\u0433\\u0435\\u043d\\u0442\\u0441\\u043a\\u0438\\u043c \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440\\u043e\\u043c \\u043c\\u043e\\u0436\\u043d\\u043e \\u0437\\u0434\\u0435\\u0441\\u044c:'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block2_title_de': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0437\\u043d\\u0430\\u043a\\u043e\\u043c\\u0438\\u0442\\u044c\\u0441\\u044f \\u0441 \\u0430\\u0433\\u0435\\u043d\\u0442\\u0441\\u043a\\u0438\\u043c \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440\\u043e\\u043c \\u043c\\u043e\\u0436\\u043d\\u043e \\u0437\\u0434\\u0435\\u0441\\u044c:'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block2_title_en': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0437\\u043d\\u0430\\u043a\\u043e\\u043c\\u0438\\u0442\\u044c\\u0441\\u044f \\u0441 \\u0430\\u0433\\u0435\\u043d\\u0442\\u0441\\u043a\\u0438\\u043c \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440\\u043e\\u043c \\u043c\\u043e\\u0436\\u043d\\u043e \\u0437\\u0434\\u0435\\u0441\\u044c:'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block2_title_es': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0437\\u043d\\u0430\\u043a\\u043e\\u043c\\u0438\\u0442\\u044c\\u0441\\u044f \\u0441 \\u0430\\u0433\\u0435\\u043d\\u0442\\u0441\\u043a\\u0438\\u043c \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440\\u043e\\u043c \\u043c\\u043e\\u0436\\u043d\\u043e \\u0437\\u0434\\u0435\\u0441\\u044c:'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block2_title_fr': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0437\\u043d\\u0430\\u043a\\u043e\\u043c\\u0438\\u0442\\u044c\\u0441\\u044f \\u0441 \\u0430\\u0433\\u0435\\u043d\\u0442\\u0441\\u043a\\u0438\\u043c \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440\\u043e\\u043c \\u043c\\u043e\\u0436\\u043d\\u043e \\u0437\\u0434\\u0435\\u0441\\u044c:'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block2_title_ru': ('django.db.models.fields.CharField', [], {'default': "u'\\u041e\\u0437\\u043d\\u0430\\u043a\\u043e\\u043c\\u0438\\u0442\\u044c\\u0441\\u044f \\u0441 \\u0430\\u0433\\u0435\\u043d\\u0442\\u0441\\u043a\\u0438\\u043c \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440\\u043e\\u043c \\u043c\\u043e\\u0436\\u043d\\u043e \\u0437\\u0434\\u0435\\u0441\\u044c:'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block3_description': ('ckeditor.fields.RichTextField', [], {'default': 'u\'<a href="tel:8-800-775-21-01">8 (800) 775-21-01</a><br><a href="mailto:info@sagavoyages.ru">info@sagavoyages.ru</a>\'', 'blank': 'True'}),
            'block3_description_cn': ('ckeditor.fields.RichTextField', [], {'default': 'u\'<a href="tel:8-800-775-21-01">8 (800) 775-21-01</a><br><a href="mailto:info@sagavoyages.ru">info@sagavoyages.ru</a>\'', 'null': 'True', 'blank': 'True'}),
            'block3_description_de': ('ckeditor.fields.RichTextField', [], {'default': 'u\'<a href="tel:8-800-775-21-01">8 (800) 775-21-01</a><br><a href="mailto:info@sagavoyages.ru">info@sagavoyages.ru</a>\'', 'null': 'True', 'blank': 'True'}),
            'block3_description_en': ('ckeditor.fields.RichTextField', [], {'default': 'u\'<a href="tel:8-800-775-21-01">8 (800) 775-21-01</a><br><a href="mailto:info@sagavoyages.ru">info@sagavoyages.ru</a>\'', 'null': 'True', 'blank': 'True'}),
            'block3_description_es': ('ckeditor.fields.RichTextField', [], {'default': 'u\'<a href="tel:8-800-775-21-01">8 (800) 775-21-01</a><br><a href="mailto:info@sagavoyages.ru">info@sagavoyages.ru</a>\'', 'null': 'True', 'blank': 'True'}),
            'block3_description_fr': ('ckeditor.fields.RichTextField', [], {'default': 'u\'<a href="tel:8-800-775-21-01">8 (800) 775-21-01</a><br><a href="mailto:info@sagavoyages.ru">info@sagavoyages.ru</a>\'', 'null': 'True', 'blank': 'True'}),
            'block3_description_ru': ('ckeditor.fields.RichTextField', [], {'default': 'u\'<a href="tel:8-800-775-21-01">8 (800) 775-21-01</a><br><a href="mailto:info@sagavoyages.ru">info@sagavoyages.ru</a>\'', 'null': 'True', 'blank': 'True'}),
            'block3_img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'block3_img_cn': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block3_img_de': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block3_img_en': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block3_img_es': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block3_img_fr': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block3_img_ru': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'block3_title': ('django.db.models.fields.CharField', [], {'default': "u'\\u0420\\u0435\\u043f\\u0443\\u0442\\u0430\\u0446\\u0438\\u044f \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438'", 'max_length': '255', 'blank': 'True'}),
            'block3_title_cn': ('django.db.models.fields.CharField', [], {'default': "u'\\u0420\\u0435\\u043f\\u0443\\u0442\\u0430\\u0446\\u0438\\u044f \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block3_title_de': ('django.db.models.fields.CharField', [], {'default': "u'\\u0420\\u0435\\u043f\\u0443\\u0442\\u0430\\u0446\\u0438\\u044f \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block3_title_en': ('django.db.models.fields.CharField', [], {'default': "u'\\u0420\\u0435\\u043f\\u0443\\u0442\\u0430\\u0446\\u0438\\u044f \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block3_title_es': ('django.db.models.fields.CharField', [], {'default': "u'\\u0420\\u0435\\u043f\\u0443\\u0442\\u0430\\u0446\\u0438\\u044f \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block3_title_fr': ('django.db.models.fields.CharField', [], {'default': "u'\\u0420\\u0435\\u043f\\u0443\\u0442\\u0430\\u0446\\u0438\\u044f \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'block3_title_ru': ('django.db.models.fields.CharField', [], {'default': "u'\\u0420\\u0435\\u043f\\u0443\\u0442\\u0430\\u0446\\u0438\\u044f \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'doc2_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'blank': 'True'}),
            'doc2_file_cn': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'doc2_file_de': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'doc2_file_en': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'doc2_file_es': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'doc2_file_fr': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'doc2_file_ru': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'header_before_plashkas': ('django.db.models.fields.CharField', [], {'default': "u'\\u0414\\u043b\\u044f \\u043e\\u0442\\u0435\\u043b\\u0435\\u0439 \\u0420\\u043e\\u0441\\u0441\\u0438\\u0438 \\u043f\\u0440\\u0435\\u0434\\u043b\\u0430\\u0433\\u0430\\u0435\\u043c \\u043d\\u0430\\u0448\\u0438 \\u0443\\u0441\\u043b\\u0443\\u0433\\u0438:'", 'max_length': '255', 'blank': 'True'}),
            'header_before_plashkas_cn': ('django.db.models.fields.CharField', [], {'default': "u'\\u0414\\u043b\\u044f \\u043e\\u0442\\u0435\\u043b\\u0435\\u0439 \\u0420\\u043e\\u0441\\u0441\\u0438\\u0438 \\u043f\\u0440\\u0435\\u0434\\u043b\\u0430\\u0433\\u0430\\u0435\\u043c \\u043d\\u0430\\u0448\\u0438 \\u0443\\u0441\\u043b\\u0443\\u0433\\u0438:'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'header_before_plashkas_de': ('django.db.models.fields.CharField', [], {'default': "u'\\u0414\\u043b\\u044f \\u043e\\u0442\\u0435\\u043b\\u0435\\u0439 \\u0420\\u043e\\u0441\\u0441\\u0438\\u0438 \\u043f\\u0440\\u0435\\u0434\\u043b\\u0430\\u0433\\u0430\\u0435\\u043c \\u043d\\u0430\\u0448\\u0438 \\u0443\\u0441\\u043b\\u0443\\u0433\\u0438:'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'header_before_plashkas_en': ('django.db.models.fields.CharField', [], {'default': "u'\\u0414\\u043b\\u044f \\u043e\\u0442\\u0435\\u043b\\u0435\\u0439 \\u0420\\u043e\\u0441\\u0441\\u0438\\u0438 \\u043f\\u0440\\u0435\\u0434\\u043b\\u0430\\u0433\\u0430\\u0435\\u043c \\u043d\\u0430\\u0448\\u0438 \\u0443\\u0441\\u043b\\u0443\\u0433\\u0438:'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'header_before_plashkas_es': ('django.db.models.fields.CharField', [], {'default': "u'\\u0414\\u043b\\u044f \\u043e\\u0442\\u0435\\u043b\\u0435\\u0439 \\u0420\\u043e\\u0441\\u0441\\u0438\\u0438 \\u043f\\u0440\\u0435\\u0434\\u043b\\u0430\\u0433\\u0430\\u0435\\u043c \\u043d\\u0430\\u0448\\u0438 \\u0443\\u0441\\u043b\\u0443\\u0433\\u0438:'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'header_before_plashkas_fr': ('django.db.models.fields.CharField', [], {'default': "u'\\u0414\\u043b\\u044f \\u043e\\u0442\\u0435\\u043b\\u0435\\u0439 \\u0420\\u043e\\u0441\\u0441\\u0438\\u0438 \\u043f\\u0440\\u0435\\u0434\\u043b\\u0430\\u0433\\u0430\\u0435\\u043c \\u043d\\u0430\\u0448\\u0438 \\u0443\\u0441\\u043b\\u0443\\u0433\\u0438:'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'header_before_plashkas_ru': ('django.db.models.fields.CharField', [], {'default': "u'\\u0414\\u043b\\u044f \\u043e\\u0442\\u0435\\u043b\\u0435\\u0439 \\u0420\\u043e\\u0441\\u0441\\u0438\\u0438 \\u043f\\u0440\\u0435\\u0434\\u043b\\u0430\\u0433\\u0430\\u0435\\u043c \\u043d\\u0430\\u0448\\u0438 \\u0443\\u0441\\u043b\\u0443\\u0433\\u0438:'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text1': ('ckeditor.fields.RichTextField', [], {'blank': 'True'}),
            'text1_cn': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text1_de': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text1_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text1_es': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text1_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text1_ru': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text2': ('ckeditor.fields.RichTextField', [], {'blank': 'True'}),
            'text2_cn': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text2_de': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text2_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text2_es': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text2_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'text2_ru': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'})
        },
        u'partners.plaha': {
            'Meta': {'ordering': "['weight']", 'object_name': 'Plaha'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'img_cn': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_de': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_en': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_es': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_fr': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_ru': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_cn': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_de': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_en': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_es': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_fr': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'is_published_ru': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['partners.PartnersPage']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_cn': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_de': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_es': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_fr': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.SmallIntegerField', [], {'default': '10000'})
        }
    }

    complete_apps = ['partners']