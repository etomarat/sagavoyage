from django.contrib import admin

from modeltranslation.admin import TranslationTabularInline
from kernel.admin import  SingletonTranslationAdmin
from .models import PartnersPage, Plaha

class PlahaInline(TranslationTabularInline):
    model = Plaha
    extra = 0

class PartnersPageAdmin(SingletonTranslationAdmin):
    inlines = [PlahaInline, ]

admin.site.register(PartnersPage, PartnersPageAdmin)