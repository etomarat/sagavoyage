from django.shortcuts import render
from .models import PartnersPage

def partners(request):
    template_name='partners.html'

    return render(request, template_name, {'object': PartnersPage.objects.get()})