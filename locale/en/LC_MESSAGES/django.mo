��    &      L  5   |      P  �  Q  `   �  
   4     ?     X     f     �     �  B   �  4   �  ;   (     d     �     �  	   �     �  ?   �          "     5  ,   L     y     �     �     �     �      �       "   $  +   G     s     �     �     �  !   �     �  !   �  @    Y  ]  F   �      �      !     !     !     !     0!     7!     ?!     N!     V!     ]!     f!     s!     |!     �!     �!     �!  	   �!  	   �!  	   �!     �!  	   �!     �!  	   �!     �!     �!     "     "     "     %"     2"     9"     ?"  	   F"     P"     $   &          %                                #   "   !                                        
                                                      	                            
              <p class="title">Выберите рассылки, на которые<br>Вы хотели бы подписаться</p>
              <fieldset class="type">
                <input type="checkbox" id="ch1_1" name="agent"><label for="ch1_1">Я турагент</label>
                <input type="checkbox" id="ch1_2" name="turist"><label for="ch1_2">Я турист</label>
                <input type="checkbox" id="ch1_3" name="hotelmaster"><label for="ch1_3">Я отельер</label>
              </fieldset>
              <fieldset class="type">
                <input type="checkbox" id="ch2_0"><label for="ch2_0" class="main">вся рассылка</label>
                <input type="checkbox" id="ch2_1" name="scince"><label for="ch2_1">Научные</label>
                <input type="checkbox" id="ch2_2" name="etno"><label for="ch2_2">Этно-туры</label>
                <input type="checkbox" id="ch2_3" name="eco"><label for="ch2_3">Эко-туры</label>
                <input type="checkbox" id="ch2_4" name="extreme"><label for="ch2_4">Экстремальные</label>
                <input type="checkbox" id="ch2_5" name="action"><label for="ch2_5">Активные</label>
                <input type="checkbox" id="ch2_6" name="piligrim"><label for="ch2_6">Паломнические</label>
                <input type="checkbox" id="ch2_7" name="business"><label for="ch2_7">Деловые</label>
                <input type="checkbox" id="ch2_8" name="cruises"><label for="ch2_8">Круизы</label>
                <input type="checkbox" id="ch2_9" name="culture"><label for="ch2_9">Культурные</label>
                <input type="checkbox" id="ch2_10" name="city"><label for="ch2_10">Сити-туры</label>
                <input type="checkbox" id="ch2_11" name="excursions"><label for="ch2_11">Экскурсии</label>
                <input type="checkbox" id="ch2_12" name="health"><label for="ch2_12">Курортно-оздоровительные</label>
              </fieldset>
              <fieldset class="type">
                <input type="checkbox" id="ch3_0"><label for="ch3_0" class="main">вся россия</label>
                <input type="checkbox" id="ch3_1" name="central"><label for="ch3_1">Центральная Россия</label>
                <input type="checkbox" id="ch3_2" name="volgas"><label for="ch3_2">Поволжье</label>
                <input type="checkbox" id="ch3_3" name="north"><label for="ch3_3">Север</label>
                <input type="checkbox" id="ch3_4" name="south"><label for="ch3_4">Юг и Кавказ</label>
                <input type="checkbox" id="ch3_5" name="ural"><label for="ch3_5">Урал</label>
                <input type="checkbox" id="ch3_6" name="siberia"><label for="ch3_6">Сибирь</label>
                <input type="checkbox" id="ch3_7" name="far_east"><label for="ch3_7">Дальний Восток</label>
                <input type="checkbox" id="ch3_8" name="crimea"><label for="ch3_8">Крым</label>
              </fieldset>
              <div class="button_action">Подтвердить подписку</div> 344064, Россия, г.Ростов-на-Дону,<br>ул.Вавилова 59в/101, оф.223 Адрес Блог о России Ваш e-mail Вернуться назад Виды туризма Время года Выберите время года для путешествия Выберите направление отдыха Выберите регион для путешествия География туров Контакты Наши услуги О нас Обратная связь Организация путешествий по России Отправить Партнерам Подписаться Подписаться на рассылку Подробнее Поиск по сайту Поиск... Показать все Показать еще Показать на карте Популярные туры Продолжительность Продолжительность тура Проживание Текст сообщения Телефоны Туры Федеральный округ Читать далее Электронная почта Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-06-14 14:34+0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
              <p class="title">Select the newsletter which <br> you would like to subscribe</p>
              <fieldset class="type">
                <input type="checkbox" id="ch1_1" name="agent"><label for="ch1_1">I am a travel agent</label>
                <input type="checkbox" id="ch1_2" name="turist"><label for="ch1_2">I am a tourist</label>
                <input type="checkbox" id="ch1_3" name="hotelmaster"><label for="ch1_3">I am a hotelier</label>
              </fieldset>
              <fieldset class="type">
                <input type="checkbox" id="ch2_0"><label for="ch2_0" class="main">all</label>
                <input type="checkbox" id="ch2_1" name="scince"><label for="ch2_1">Scientific</label>
                <input type="checkbox" id="ch2_2" name="etno"><label for="ch2_2">Ethno Tours</label>
                <input type="checkbox" id="ch2_3" name="eco"><label for="ch2_3">Eco-tours</label>
                <input type="checkbox" id="ch2_4" name="extreme"><label for="ch2_4">Extreme</label>
                <input type="checkbox" id="ch2_5" name="action"><label for="ch2_5">Active</label>
                <input type="checkbox" id="ch2_6" name="piligrim"><label for="ch2_6">Pilgrimage</label>
                <input type="checkbox" id="ch2_7" name="business"><label for="ch2_7">Business</label>
                <input type="checkbox" id="ch2_8" name="cruises"><label for="ch2_8">Cruises</label>
                <input type="checkbox" id="ch2_9" name="culture"><label for="ch2_9">Cultural</label>
                <input type="checkbox" id="ch2_10" name="city"><label for="ch2_10">City tours</label>
                <input type="checkbox" id="ch2_11" name="excursions"><label for="ch2_11">Excursions</label>
                <input type="checkbox" id="ch2_12" name="health"><label for="ch2_12">Resort and Wellness</label>
              </fieldset>
              <fieldset class="type">
                <input type="checkbox" id="ch3_0"><label for="ch3_0" class="main">All Russia</label>
                <input type="checkbox" id="ch3_1" name="central"><label for="ch3_1">Central Russia</label>
                <input type="checkbox" id="ch3_2" name="volgas"><label for="ch3_2">Volga region</label>
                <input type="checkbox" id="ch3_3" name="north"><label for="ch3_3">North</label>
                <input type="checkbox" id="ch3_4" name="south"><label for="ch3_4">South And Caucasus</label>
                <input type="checkbox" id="ch3_5" name="ural"><label for="ch3_5">Ural</label>
                <input type="checkbox" id="ch3_6" name="siberia"><label for="ch3_6">Siberia</label>
                <input type="checkbox" id="ch3_7" name="far_east"><label for="ch3_7">Far East</label>
                <input type="checkbox" id="ch3_8" name="crimea"><label for="ch3_8">Crimea</label>
              </fieldset>
              <div class="button_action">Confirm</div> 344064, Russia, Rostov-on-Don,<br>Vavilov Street 59B / 101, office 223 Address Blog Your e-mail Go back Types of tourism Season Seasons Types of tours Regions Region Contacts Our Services About us Feedback Travels to Russia Send For partners Subscribe Subscribe Read more Search Search... Show all Show more Show on map Popular tours Duration Duration Accommodation Message text Phones Tours Region Read more Email 