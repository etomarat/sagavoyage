# coding: utf-8

from django.contrib import admin

from kernel.admin import PublishedSortableTranslationAdmin

from kernel.admin import  SingletonTranslationAdmin

from .models import *

class FormsAdmin(admin.ModelAdmin):
  readonly_fields = ('date',)


admin.site.register(Promo, PublishedSortableTranslationAdmin)
admin.site.register(Subscribe, FormsAdmin)
admin.site.register(FeedBack, FormsAdmin)

admin.site.register(SubscribeTranslation, SingletonTranslationAdmin)

# EOF
