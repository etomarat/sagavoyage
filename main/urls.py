# coding: utf-8

from django.conf.urls import patterns, url
from django.views.generic import TemplateView

from .views import *


urlpatterns = patterns(
    '',
    url(r'^$', Index.as_view(), name='index'),
    url(r'^subscribe/$', subscribe, name='subscribe'),
    url(r'^feedback/$', feedback, name='feedback'),
    
    #url(r'^about/$', InWorkView.as_view(), name='about'),
    #url(r'^blog/$', InWorkView.as_view(), name='blog'),
    #url(r'^for_partners/$', InWorkView.as_view(), name='for_partners'),
    #url(r'^service/$', InWorkView.as_view(), name='service'),
    #url(r'^(?P<pk>\d+)$', TourDetail.as_view(), name='detail'),
    )

# EOF