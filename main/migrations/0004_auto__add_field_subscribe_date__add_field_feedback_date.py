# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Subscribe.date'
        db.add_column(u'main_subscribe', 'date',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, default=datetime.datetime(2015, 5, 17, 0, 0), blank=True),
                      keep_default=False)

        # Adding field 'FeedBack.date'
        db.add_column(u'main_feedback', 'date',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, default=datetime.datetime(2015, 5, 17, 0, 0), blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Subscribe.date'
        db.delete_column(u'main_subscribe', 'date')

        # Deleting field 'FeedBack.date'
        db.delete_column(u'main_feedback', 'date')


    models = {
        u'main.feedback': {
            'Meta': {'object_name': 'FeedBack'},
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        },
        u'main.promo': {
            'Meta': {'object_name': 'Promo'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'link_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'link_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'text_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'text_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'})
        },
        u'main.subscribe': {
            'Meta': {'object_name': 'Subscribe'},
            'action': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'agent': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'business': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'central': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'city': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'crimea': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cruises': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'culture': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'eco': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'etno': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'excursions': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'extreme': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'far_east': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'health': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hotelmaster': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'north': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'piligrim': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'scince': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'siberia': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'south': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'turist': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ural': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'volgas': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['main']