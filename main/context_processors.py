from .models import SubscribeTranslation

def subscribe_form(request):
    try:
        s = SubscribeTranslation.objects.get()
    except SubscribeTranslation.DoesNotExist:
        s = None
    return {
        'subscribe_form': s
    }
