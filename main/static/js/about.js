$(document).ready(function () {

	$(function(){
      	$("#slides").slidesjs({
	        width: 1280,
	        height: 600,
    	});
    });

	//Подключаем fancybox

    $(".fancybox").fancybox();

    //Показать все отзывы

    $('.feedbacks .button_action').on("click", function() {
    	$(".feedback_more").slideToggle(500, function(){
    		$('.feedbacks .button_action').hide();
    	});
	});

});