$(document).ready(function () {

	var time=500;

	//Подключение плагина multiselect

	$("#geo_tours").multiselect({
	  show: ['blind', time],
	  hide: ['blind', time],
	  height: 204,
	  create: function(event, ui){
        var label = $(this).attr('data-label');
		$(this).multiselect('option', 'noneSelectedText', label+' (0)')
		$(this).multiselect('option', 'selectedText', label+' (#)')
	  }
	});

	$("#season_tours").multiselect({
	   show: ['blind', time],
	   hide: ['blind', time],
	   height: 204,
	  create: function(event, ui){
        var label = $(this).attr('data-label');
		$(this).multiselect('option', 'noneSelectedText', label+' (0)')
		$(this).multiselect('option', 'selectedText', label+' (#)')
	  }
	});

	$("#types_tours").multiselect({
	   show: ['blind', time],
	   hide: ['blind', time],
	   height: 204,
	   create: function(event, ui){
        var label = $(this).attr('data-label');
		$(this).multiselect('option', 'noneSelectedText', label+' (0)')
		$(this).multiselect('option', 'selectedText', label+' (#)')
	  }
	});

	$("#duration_tours").multiselect({
	   show: ['blind', time],
	   hide: ['blind', time],
	   height: 204,
	   create: function(event, ui){
        var label = $(this).attr('data-label');
		$(this).multiselect('option', 'noneSelectedText', label+' (0)')
		$(this).multiselect('option', 'selectedText', label+' (#)')
	  }
	});

	//Обработка кликов по инпутам внутри мультиселектов

	$(".multiselect").on("change", function(event, ui) {
        $(".pack_of_tours .waiter").addClass("on");
        $(".tours .button_wrap").removeClass("no_action");

        // Фильтрация туров

        data = {
            csrfmiddlewaretoken: $('input[name="csrfmiddlewaretoken"]').val(),
            geography: $('#geo_tours').val(),
            season: $('#season_tours').val(),
            type: $('#types_tours').val(),
            t_duration: $('#duration_tours').val(),
        }

        $.ajax({
            type: "POST",
            url: ".",
            data: data,
            success: function(content){
                $('.pack_of_tours').html(content)
        		$(".pack_of_tours .waiter").removeClass("on");
            }
        })
	});

	//Обработка кнопки "Показать еще"

	$('.tours .button_action').on("click", function() {
    	$(".pack_of_tours .waiter").addClass("on");
		$.ajax({
            type: "POST",
            url: "contact.php",
            data: obj = {key1: 4},
            success: function(msg){
                if(msg == 'OK')
                {
                   $(".pack_of_tours .waiter").removeClass("on");
                   // Здесь нужно сделать проверку, закончились ли туры по данному выбору. Если да, то делаем
                   // $(".tours .button_wrap").addClass("no_action");
                }
                else{
                    alert("Error!");
                    $(".pack_of_tours .waiter").removeClass("on");
                }
            }
        })
	});


});