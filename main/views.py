# coding: utf-8

from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView

from django.http import HttpResponse
from django.core.mail import send_mail, EmailMessage

from django.shortcuts import render_to_response
from django.template import RequestContext

from tour.models import *

from .models import *


class Index(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        context.update(
            promo_list=Promo.objects.published(),
            popular_tours=Tour.objects.published(is_popular=True)[:5],
            tour_seasons=TourSeason.get_exists_tours_qset(),
            tour_types=TourType.get_exists_tours_qset().filter(on_main=True),
            tour_regions=TourRegion.objects.published(),
            tour_list=Tour.objects.published()[:3],
            )
        return context


def subscribe(request):
    s = Subscribe()
    for i in request.POST:
        try:
            setattr(s, i, request.POST.get(i))
        except:
            pass
    s.save()
    return HttpResponse("ok")


def feedback(request):
    s = FeedBack()
    for i in request.POST:
        try:
            setattr(s, i, request.POST.get(i))
        except:
            pass
    s.save()
    return HttpResponse("ok")


class InWorkView(TemplateView):
    template_name = "in_work.html"

# EOF
