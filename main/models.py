# coding: utf-8

from django.db import models
from django.utils.translation import ugettext_lazy as _
from solo.models import SingletonModel

from kernel.db import PublishModel, TitleMixin, SortableMixin


class Promo(PublishModel, TitleMixin, SortableMixin):
    text = models.TextField(verbose_name=_(u'Текст'), blank=True)
    link = models.CharField(verbose_name=_(u'Ссылка'), max_length=0xff, blank=True)
    img = models.ImageField(
        verbose_name=_(u'Изображение'),
        help_text=u'1280x600px',
        upload_to='promo',
        blank=True,
        )

    class Meta:
        verbose_name = _(u'Промо на главной')
        verbose_name_plural = _(u'Промо на главной')
        ordering = ['weight']

    class TMeta:
        exclude = ['img']

class SubscribeTranslation(SingletonModel):
    heading = models.CharField(verbose_name=_(u'Заголовок'), default=u'Выберите рассылки, на которые<br>Вы хотели бы подписаться', blank=True, max_length=0xff)

    agent = models.CharField(verbose_name=_(u'Я турагент'), default=u'Я турагент', blank=True, max_length=0xff)
    turist = models.CharField(verbose_name=_(u'Я турист'), default=u'Я турист', blank=True, max_length=0xff)
    hotelmaster = models.CharField(verbose_name=_(u'Я отельер'), default=u'Я отельер', blank=True, max_length=0xff)

    allsubscription = models.CharField(verbose_name=_(u'вся рассылка'), default=u'вся рассылка', blank=True, max_length=0xff)
    scince = models.CharField(verbose_name=_(u'Научные туры'), default=u'Научные туры', blank=True, max_length=0xff)
    etno = models.CharField(verbose_name=_(u'Этно-туры'), default=u'Этно-туры', blank=True, max_length=0xff)
    eco = models.CharField(verbose_name=_(u'Эко-туры'), default=u'Эко-туры', blank=True, max_length=0xff)
    extreme = models.CharField(verbose_name=_(u'Экстремальные туры'), default=u'Экстремальные туры', blank=True, max_length=0xff)
    action = models.CharField(verbose_name=_(u'Активные туры'), default=u'Активные туры', blank=True, max_length=0xff)
    piligrim = models.CharField(verbose_name=_(u'Паломнические туры'), default=u'Паломнические туры', blank=True, max_length=0xff)
    business = models.CharField(verbose_name=_(u'Деловые туры'), default=u'Деловые туры', blank=True, max_length=0xff)
    cruises = models.CharField(verbose_name=_(u'Круизы'), default=u'Круизы', blank=True, max_length=0xff)
    culture = models.CharField(verbose_name=_(u'Культурные'), default=u'Культурные', blank=True, max_length=0xff)
    city = models.CharField(verbose_name=_(u'Сити-туры'), default=u'Сити-туры', blank=True, max_length=0xff)
    excursions = models.CharField(verbose_name=_(u'Экскурсии'), default=u'Экскурсии', blank=True, max_length=0xff)
    health = models.CharField(verbose_name=_(u'Курортно-оздоровительные'), default=u'Курортно-оздоровительные', blank=True, max_length=0xff)

    allrussia = models.CharField(verbose_name=_(u'вся Россия'), default=u'вся Россия', blank=True, max_length=0xff)
    central = models.CharField(verbose_name=_(u'Центральная Россия'), default=u'Центральная Россия', blank=True, max_length=0xff)
    volgas = models.CharField(verbose_name=_(u'Поволжье'), default=u'Поволжье', blank=True, max_length=0xff)
    north = models.CharField(verbose_name=_(u'Север'), default=u'Север', blank=True, max_length=0xff)
    south = models.CharField(verbose_name=_(u'Юг и Кавказ'), default=u'Юг и Кавказ', blank=True, max_length=0xff)
    ural = models.CharField(verbose_name=_(u'Урал'), default=u'Урал', blank=True, max_length=0xff)
    siberia = models.CharField(verbose_name=_(u'Сибирь'), default=u'Сибирь', blank=True, max_length=0xff)
    far_east = models.CharField(verbose_name=_(u'Дальний Восток'), default=u'Дальний Восток', blank=True, max_length=0xff)
    crimea = models.CharField(verbose_name=_(u'Крым'), default=u'Крым', blank=True, max_length=0xff)

    accept = models.CharField(verbose_name=_(u'Подтвердить подписку'), default=u'Подтвердить подписку', blank=True, max_length=0xff)

    def __unicode__(self):
        return u'Подписка на рассылку (ПЕРЕВОД)'

    class Meta:
        verbose_name=_(u'Подписка на рассылку (ПЕРЕВОД)')

    class TMeta:
        fields = [
              'heading',
              'agent',
              'turist',
              'hotelmaster',
              'allsubscription',
              'scince',
              'etno',
              'eco',
              'extreme',
              'action',
              'piligrim',
              'business',
              'cruises',
              'culture',
              'city',
              'excursions',
              'health',
              'allrussia',
              'central',
              'volgas',
              'north',
              'south',
              'ural',
              'siberia',
              'far_east',
              'crimea',
              'accept'
          ]


class Subscribe(models.Model):
    date = models.DateTimeField(verbose_name=_(u'Дата создания'), auto_now_add=True)
    email = models.CharField(verbose_name=_(u'E-mail'), max_length=0xff, blank=True)
    lang = models.CharField(verbose_name=_(u'Язык'), max_length=0xff, blank=True)

    agent = models.BooleanField(verbose_name=_(u'Я турагент'), default=False, blank=True)
    turist = models.BooleanField(verbose_name=_(u'Я турист'), default=False, blank=True)
    hotelmaster = models.BooleanField(verbose_name=_(u'Я отельер'), default=False, blank=True)

    scince = models.BooleanField(verbose_name=_(u'Научные туры'), default=False, blank=True)
    etno = models.BooleanField(verbose_name=_(u'Этно-туры'), default=False, blank=True)
    eco = models.BooleanField(verbose_name=_(u'Эко-туры'), default=False, blank=True)
    extreme = models.BooleanField(verbose_name=_(u'Экстремальные туры'), default=False, blank=True)
    action = models.BooleanField(verbose_name=_(u'Активные туры'), default=False, blank=True)
    piligrim = models.BooleanField(verbose_name=_(u'Паломнические туры'), default=False, blank=True)
    business = models.BooleanField(verbose_name=_(u'Деловые туры'), default=False, blank=True)
    cruises = models.BooleanField(verbose_name=_(u'Круизы'), default=False, blank=True)
    culture = models.BooleanField(verbose_name=_(u'Культурные'), default=False, blank=True)
    city = models.BooleanField(verbose_name=_(u'Сити-туры'), default=False, blank=True)
    excursions = models.BooleanField(verbose_name=_(u'Экскурсии'), default=False, blank=True)
    health = models.BooleanField(verbose_name=_(u'Курортно-оздоровительные'), default=False, blank=True)

    central = models.BooleanField(verbose_name=_(u'Центральная Россия'), default=False, blank=True)
    volgas = models.BooleanField(verbose_name=_(u'Поволжье'), default=False, blank=True)
    north = models.BooleanField(verbose_name=_(u'Север'), default=False, blank=True)
    south = models.BooleanField(verbose_name=_(u'Юг и Кавказ'), default=False, blank=True)
    ural = models.BooleanField(verbose_name=_(u'Урал'), default=False, blank=True)
    siberia = models.BooleanField(verbose_name=_(u'Сибирь'), default=False, blank=True)
    far_east = models.BooleanField(verbose_name=_(u'Дальний Восток'), default=False, blank=True)
    crimea = models.BooleanField(verbose_name=_(u'Крым'), default=False, blank=True)

    class Meta:
        verbose_name = _(u'Подписка на рассылку')
        verbose_name_plural = _(u'Подписки на рассылку')

    def __unicode__(self):
        return u'%s' % (self.email)

class FeedBack(models.Model):
    date = models.DateTimeField(verbose_name=_(u'Дата создания'), auto_now_add=True)
    email = models.CharField(verbose_name=_(u'E-mail'), max_length=0xff, blank=True)
    lang = models.CharField(verbose_name=_(u'Язык'), max_length=0xff, blank=True)
    message = models.TextField(verbose_name=_(u'Текст'), blank=True)

    class Meta:
        verbose_name = _(u'Обратная связь')
        verbose_name_plural = _(u'Обратная связь')

    def __unicode__(self):
        return u'%s' % (self.email)

# EOF
