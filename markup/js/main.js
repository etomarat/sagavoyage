$(document).ready(function () {

	var time = 500;

	//Анимашка в хэдере

    $('.x-telephone').on("click", function() {
		$('.telephone_li').toggleClass("on");
		if ($('.search_li').hasClass("on")){
			$('.search_li').toggleClass("on");
			$('.topmenu').removeClass("on");
			$('.topmenu').addClass("on2");
		}
		else{
			$('.topmenu').toggleClass("on2");
		}	
	});

	$('.x-search').on("click", function() {
		$('.search_li').toggleClass("on");
		if ($('.telephone_li').hasClass("on")){
			$('.telephone_li').toggleClass("on");
			$('.topmenu').removeClass("on2");
			$('.topmenu').addClass("on");
		}
		else{
			$('.topmenu').toggleClass("on");
		}	
	});

	//Плавный скроллинг по якорю

	$('#go_contacts').on("click", function()
	{
		$('html, body').animate({
			scrollTop: $('#contacts').offset().top-44
		}, 1500);
	});

	//Обработка кнопки "подписаться", проверка валидности e-mail и вызов всплывающего окна

	$('.subscribe .button_action').on("click", function() {
        if($('#email').val() != '') {
            var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
            if(pattern.test($('#email').val())){
            	$('#email').css({'border' : '1px solid white'});
                $('.subscribe .darker').fadeIn(time);
            } else {
                $('#email')
                	.css("outline","0px solid #ff0000")
			        .css("border-color","#ff0000")
			        .animate({"outline-width":"3px"},250)
			        .animate({"outline-width":"0px"},250)
			        .animate({"outline-width":"3px"},250)
			        .animate({"outline-width":"0px"},250, function(){alert('Введен неверный e-mail');});         
            }
        } else {
            $('#email')
	            .css("outline","0px solid #ff0000")
		        .css("border-color","#ff0000")
		        .animate({"outline-width":"3px"},250)
		        .animate({"outline-width":"0px"},250)
		        .animate({"outline-width":"3px"},250)
		        .animate({"outline-width":"0px"},250, function(){alert('Поле email не должно быть пустым');});    
        }	
	});

	//Обработка кнопки "подтвердить подписку"

	$('.subscribe_selection .button_action').on("click", function() {
        $.ajax({
            type: "POST",
            url: "contact.php",
            data: obj = {key1: 5},
            success: function(msg){
                if(msg == 'OK')
                {
                   $('.subscribe .darker').fadeOut(time, function() {alert('Спасибо за подписку!');});	
                }
                else{
                    alert("Error");
                }
            }
        })				
	});

	//Закрытие поп-апа при клике по темной области

	$('.darker2').on("click", function() 
	{
		$(this).parent().fadeOut(time);
    });

    //Инпут, которые чекает/анчекает сразу все инпуты

    $('label.main').on("click", function() {
    	if ( $(this).parent().children('input:first-child').is(':checked') ){
    		$(this).parent().children('input:nth-child(n+2)').prop("checked", false);
    	}
    	else{
    		$(this).parent().children('input:nth-child(n+2)').prop("checked", true);
    	} 	
	});

	//Обработка формы обратной связи, проверка валидации e-mail

	$('#feedback .button_action').on("click", function() 
	{
		var x1 = 1,
	        x2 = 1;
	    var data = $("#feedback").serialize();
	    if($("#feedback input")[0].value=="")
	    {
	        x1 = 0;
	        $("#feedback input")
	        	.css("outline","0px solid #ff0000")
		        .css("border-color","#ff0000")
		        .animate({"outline-width":"3px"},250)
		        .animate({"outline-width":"0px"},250)
		        .animate({"outline-width":"3px"},250)
		        .animate({"outline-width":"0px"},250, function(){alert('Поле email не должно быть пустым');});   
	    }
	    else{
            var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
            if(pattern.test($('#feedback input').val())){
            	$('#feedback input').css({'border' : '1px solid white'});
            } 
            else {
                $('#feedback input')
                	.css("outline","0px solid #ff0000")
			        .css("border-color","#ff0000")
			        .animate({"outline-width":"3px"},250)
			        .animate({"outline-width":"0px"},250)
			        .animate({"outline-width":"3px"},250)
			        .animate({"outline-width":"0px"},250, function(){alert('Введен неверный e-mail');});         
            }
	    }
	    if($("#feedback textarea")[0].value=="")
	    {
	        x2 = 0;
	        $("#feedback textarea")
	        	.css("outline","0px solid #ff0000")
		        .css("border-color","#ff0000")
		        .animate({"outline-width":"3px"},250)
		        .animate({"outline-width":"0px"},250)
		        .animate({"outline-width":"3px"},250)
		        .animate({"outline-width":"0px"},250, function(){alert('Введите пожалуйста текст вашего сообщения!');});   
	    }
	    if(x1*x2)
	    {
	        $.ajax({
	            type: "POST",
	            url: "contact.php",
	            data: obj = {key1: 4},
	            success: function(msg){
	                if(msg == 'OK')
	                {
	                   alert("Спасибо за ваше сообщение!")
	                }
	                else{
	                    alert("Error!");
	                }
	            }
	        })
	    }
    });

	


});