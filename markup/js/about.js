$(document).ready(function () {

	//Скрываем меню по скроллу
	
	$(window).scroll(function(){
	    if ($(window).scrollTop() >= 100) {
	       $('.header').addClass('hide_header');
	    }
	  if ($(window).scrollTop() < 100) {
	       $('.header').removeClass('hide_header');
	    }
	});

	//Подключение слайдера

	$(function(){
      	$("#slides").slidesjs({
	        width: 1280,
	        height: 600,
    	});
    });

	//Подключаем fancybox

    $(".fancybox").fancybox();

    //Показать все отзывы

    $('.feedbacks .button_action').on("click", function() {
    	$(".feedback_more").slideToggle(500, function(){
    		$('.feedbacks .button_action').hide();
    	});
	});

});