$(document).ready(function () {

	var time=500;

	//Подключение плагина multiselect 

	$("#geo_tours").multiselect({
	   selectedText : "География туров (#)",
	   noneSelectedText : "География туров (0)",
	   show: ['blind', time],
	   hide: ['blind', time],
	   height: 204,
	});

	$("#season_tours").multiselect({
	   selectedText : "Время года (#)",
	   noneSelectedText : "Время года (0)",
	   show: ['blind', time],
	   hide: ['blind', time],
	   height: 204,
	});

	$("#types_tours").multiselect({
	   selectedText : "Виды туризма (#)",
	   noneSelectedText : "Виды туризма (0)",
	   show: ['blind', time],
	   hide: ['blind', time],
	   height: 204,
	});

	$("#duration_tours").multiselect({
	   selectedText : "Продолжительность (#)",
	   noneSelectedText : "Продолжительность (0)",
	   show: ['blind', time],
	   hide: ['blind', time],
	   height: 204,
	});

	//Обработка кликов по инпутам внутри мультиселектов

	$(".multiselect").on("multiselectclick", function(event, ui) {
		$(".pack_of_tours .waiter").addClass("on");
		$(".tours .button_wrap").removeClass("no_action");
		$.ajax({
            type: "POST",
            url: "contact.php",
            data: obj = {key1: 4},
            success: function(msg){
                if(msg == 'OK')
                {
                   $(".pack_of_tours .waiter").removeClass("on");
                }
                else{
                    alert("Error!");
                    $(".pack_of_tours .waiter").removeClass("on");
                }
            }
        })
	});

	//Обработка кнопки "Показать еще"

	$('.tours .button_action').on("click", function() {
    	$(".pack_of_tours .waiter").addClass("on");
		$.ajax({
            type: "POST",
            url: "contact.php",
            data: obj = {key1: 4},
            success: function(msg){
                if(msg == 'OK')
                {
                   $(".pack_of_tours .waiter").removeClass("on");
                   // Здесь нужно сделать проверку, закончились ли туры по данному выбору. Если да, то делаем
                   // $(".tours .button_wrap").addClass("no_action");
                }
                else{
                    alert("Error!");
                    $(".pack_of_tours .waiter").removeClass("on");
                }
            }
        })	
	});

	//Скрываем меню по скроллу
	
	$(window).scroll(function(){
	    if ($(window).scrollTop() >= 100) {
	       $('.header').addClass('hide_header');
	    }
	  if ($(window).scrollTop() < 100) {
	       $('.header').removeClass('hide_header');
	    }
	});


});